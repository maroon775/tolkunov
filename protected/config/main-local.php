<?php

return [
    'preload' => [
        'debug',
    ],
    'modules' => [
        /*'gii' => [ // you can access gii by following url http://site.com/gii/default/login
            'class' => 'system.gii.GiiModule',
            'password' => 'giiYupe',
            'generatorPaths' => [
                'application.modules.yupe.extensions.yupe.gii',
            ],
            'ipFilters' => [],
        ],*/
    ],
    'components' => [
//        'assetManager' => [
////            'linkAssets' => true,
//            //'forceCopy' => true
//        ],
        'debug' => [
            'class' => 'vendor.zhuravljov.yii2-debug.Yii2Debug',
            'internalUrls' => true,
        ],
        'cache' => [
            'class' => 'CDummyCache',
        ],
        'urlManager' => [
            'rules' => [
                '/gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
            ],
        ],
//        'clientScript' => [
//            'scriptMap' => [
//                'jquery.js' => 'https://yastatic.net/jquery/1.11.3/jquery.min.js',
//            ],
//        ],
        'bootstrap' => [
            'coreCss' => false,
            'bootstrapCss' => false,
            'fontAwesomeCss' => false,
            'enableJS' => false,
            'enableNotifierJS' => false,
            'enableBootboxJS' => false,
        ],
    ],
];
