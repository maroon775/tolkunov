<?php

class m170317_141119_add_seo_short_title_fields extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{vacancy_vacancy}}', 'short_title', 'string');
        $this->addColumn('{{vacancy_vacancy}}', 'slug', 'varchar(150) NOT NULL');
        $this->addColumn('{{vacancy_vacancy}}', 'seo_title', 'string');
        $this->addColumn('{{vacancy_vacancy}}', 'seo_keywords', 'string');
        $this->addColumn('{{vacancy_vacancy}}', 'seo_description', 'string');

        Yii::app()->db->createCommand('UPDATE {{vacancy_vacancy}} SET slug = id')->execute();

        //ix
        $this->createIndex("ix_{{vacancy_vacancy}}_slug", '{{vacancy_vacancy}}', "slug", true);
    }

    public function safeDown()
    {
        $this->dropIndex("ix_{{vacancy_vacancy}}_slug", '{{vacancy_vacancy}}');

        $this->dropColumn('{{vacancy_vacancy}}', 'short_title');
        $this->dropColumn('{{vacancy_vacancy}}', 'slug');
        $this->dropColumn('{{vacancy_vacancy}}', 'seo_title');
        $this->dropColumn('{{vacancy_vacancy}}', 'seo_keywords');
        $this->dropColumn('{{vacancy_vacancy}}', 'seo_description');
    }
}
