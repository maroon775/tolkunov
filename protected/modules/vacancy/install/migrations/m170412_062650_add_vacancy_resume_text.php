<?php

class m170412_062650_add_vacancy_resume_text extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{vacancy_resume}}', 'text', 'text');
    }

    public function safeDown()
    {
        $this->dropColumn('{{vacancy_resume}}', 'text');
    }
}
