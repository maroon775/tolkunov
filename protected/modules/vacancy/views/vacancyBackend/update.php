<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('VacancyModule.vacancy', 'Вакансии') => ['/vacancy/vacancyBackend/index'],
    $model->title => ['/vacancy/vacancyBackend/view', 'id' => $model->id],
    Yii::t('VacancyModule.vacancy', 'Редактирование'),
];

$this->pageTitle = Yii::t('VacancyModule.vacancy', 'Вакансии - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('VacancyModule.vacancy', 'Управление вакансиями'), 'url' => ['/vacancy/vacancyBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('VacancyModule.vacancy', 'Добавить вакансию'), 'url' => ['/vacancy/vacancyBackend/create']],
    ['label' => Yii::t('VacancyModule.vacancy', 'Вакансия') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('VacancyModule.vacancy', 'Редактирование вакансии'), 'url' => [
        '/vacancy/vacancyBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('VacancyModule.vacancy', 'Просмотреть вакансию'), 'url' => [
        '/vacancy/vacancyBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('VacancyModule.vacancy', 'Удалить вакансию'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/vacancy/vacancyBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('VacancyModule.vacancy', 'Вы уверены, что хотите удалить вакансию?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('VacancyModule.vacancy', 'Редактирование') . ' ' . Yii::t('VacancyModule.vacancy', 'вакансии'); ?>        <br/>
        <small>&laquo;<?php echo $model->title; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', ['model' => $model]); ?>