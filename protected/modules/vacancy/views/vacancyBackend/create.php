<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('VacancyModule.vacancy', 'Вакансии') => ['/vacancy/vacancyBackend/index'],
    Yii::t('VacancyModule.vacancy', 'Добавление'),
];

$this->pageTitle = Yii::t('VacancyModule.vacancy', 'Вакансии - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('VacancyModule.vacancy', 'Управление вакансиями'), 'url' => ['/vacancy/vacancyBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('VacancyModule.vacancy', 'Добавить вакансию'), 'url' => ['/vacancy/vacancyBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('VacancyModule.vacancy', 'Вакансии'); ?>
        <small><?php echo Yii::t('VacancyModule.vacancy', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', ['model' => $model]); ?>