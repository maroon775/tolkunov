<?php
/**
 * Отображение для view:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model VacancyResume
 * @var $this VacancyResumeBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('VacancyModule.vacancy', 'Резюме') => ['/vacancy/vacancyResumeBackend/index'],
    $model->name,
];

$this->pageTitle = Yii::t('VacancyModule.vacancy', 'Резюме - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('VacancyModule.vacancy', 'Управление резюме'), 'url' => ['/vacancy/vacancyResumeBackend/index']],
    ['label' => Yii::t('VacancyModule.vacancy', 'Резюме') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    [
        'icon' => 'fa fa-fw fa-eye',
        'label' => Yii::t('VacancyModule.vacancy', 'Просмотреть резюме'),
        'url' => [
            '/vacancy/vacancyResumeBackend/view',
            'id' => $model->id,
        ],
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
        'label' => Yii::t('VacancyModule.vacancy', 'Удалить резюме'),
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/vacancy/vacancyResumeBackend/delete', 'id' => $model->id],
            'confirm' => Yii::t('VacancyModule.vacancy', 'Вы уверены, что хотите удалить резюме?'),
            'csrf' => true,
        ],
    ],
];
?>
    <div class="page-header">
        <h1>
            <?= Yii::t('VacancyModule.vacancy', 'Просмотр') . ' ' . Yii::t('VacancyModule.vacancy', 'резюме'); ?> <br/>
            <small>&laquo;<?= $model->name; ?>&raquo;</small>
        </h1>
    </div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        'id',
        'create_time',
        'update_time',
        [
            'name' => 'vacancy_id',
            'value' => $model->vacancy->title,
        ],
        'name',
        'phone',
        'email',
        [
            'name' => 'text',
            'type' => 'raw',
            'value' => nl2br($model->text),
        ],
        [
            'name' => 'status',
            'value' => $model->getStatus(),
        ],
    ],
]); ?>

    <h3>Файлы</h3>

<?php foreach ($model->files as $file): ?>
    <?= CHtml::link($file->file, $file->getFileUrl()); ?><br>
<?php endforeach; ?>