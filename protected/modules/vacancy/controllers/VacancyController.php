<?php

class VacancyController extends yupe\components\controllers\FrontController
{
    public function behaviors()
    {
        return [
            'seo' => ['class' => 'vendor.chemezov.yii-seo.behaviors.SeoBehavior'],
        ];
    }

    public function filters()
    {
        return [
//            ['vendor.chemezov.yii-seo.filters.SeoFilter + view'], // apply the filter to the view-action
//            array(
//                'COutputCache + view',
//                'duration'    => 100,
//                'varyByParam' => array('id'),
//            ),
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider(Vacancy::model()->published(), [
            'pagination' => false,
//            'pagination' => [
//                'pageSize' => 16,
//            ],
        ]);

        $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionCreate()
    {
        $model = new VacancyResume();
        $fileModels = [];

        if (\Yii::app()->request->isPostRequest) {
            if (isset($_POST['VacancyResume'])) {
                $validate = true;

                $model->attributes = $_POST['VacancyResume'];

                $files = CUploadedFile::getInstancesByName('VacancyResume[files]');

                if (isset($files) && count($files) > 0) {
                    // go through each uploaded image
                    foreach ($files as $file) {
                        $fileModel = new VacancyResumeFile();
                        $fileModel->resume_id = 1;
                        $fileModel->fileUpload->addFileInstance($file);

                        if (!$fileModel->validate()) {
                            $validate = false;
                        }

                        $fileModels[] = $fileModel;
                    }
                }

                if ($model->validate() && $validate) {
                    if (!$model->save()) {
                        Yii::app()->user->setFlash(
                            yupe\widgets\YFlashMessages::ERROR_MESSAGE,
                            Yii::t('VacancyModule.vacancy', 'Не удалось сохранить резюме!')
                        );
                    } else {
                        foreach ($fileModels as $fileModel) {
                            $fileModel->resume_id = $model->id;

                            if (!$fileModel->save()) {
                                Yii::app()->user->setFlash(
                                    yupe\widgets\YFlashMessages::ERROR_MESSAGE,
                                    Yii::t('VacancyModule.vacancy', 'Не удалось сохранить файл из резюме!')
                                );
                            }
                        }

                    }

                    $filesString = '';

                    foreach ($fileModels as $fileModel) {
                        $filesString .= CHtml::link($fileModel->file, $fileModel->getFileUrl()) . '<br>';
                    }

                    /* Send Email */
                    $mail = Yii::app()->mailMessage;

                    $result = $mail->raiseMailEvent('NEW_RESUME', [
                        '{user_email}' => $model->email,
                        '{user_name}' => $model->name,
                        '{user_phone}' => $model->phone,
                        '{user_text}' => nl2br($model->text),
                        '{files}' => $filesString,
                        '{site_name}' => Yii::app()->getModule('yupe')->siteName,
                        '{admin_email}' => Yii::app()->getModule('yupe')->email,
                    ]);

                    /* End send email */

                    Yii::app()->user->setFlash(
                        yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                        Yii::t('VacancyModule.vacancy', 'Запись добавлена!')
                    );
                }

//                if ($model->save()) {
//                    Yii::app()->user->setFlash(
//                        yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
//                        Yii::t('VacancyModule.vacancy', 'Запись добавлена!')
//                    );
//                }


            }

//            Yii::app()->clientScript->registerScript('success', 'var needReload = true; setTimeout($.fancybox.close, 1000);', CClientScript::POS_END);
        }

        $this->renderPartial('_form', ['model' => $model, 'fileModels' => $fileModels]);
    }

    public function actionView($slug)
    {
        $model = $this->loadModel($slug);

        $this->render('view', ['model' => $model]);
    }

    /**
     * @param string $slug
     * @return Vacancy
     * @throws CHttpException
     */
    protected function loadModel($slug)
    {
        $model = Vacancy::model()->published()->findByAttributes(['slug' => $slug]);

        if (!$model) {
            throw new CHttpException(404);
        }

        return $model;
    }
}
