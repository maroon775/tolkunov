<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model Offer
 *   @var $this OfferBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OfferModule.offer', 'Акции') => ['/offer/offerBackend/index'],
    $model->title => ['/offer/offerBackend/view', 'id' => $model->id],
    Yii::t('OfferModule.offer', 'Редактирование'),
];

$this->pageTitle = Yii::t('OfferModule.offer', 'Акции - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OfferModule.offer', 'Управление акциями'), 'url' => ['/offer/offerBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OfferModule.offer', 'Добавить акцию'), 'url' => ['/offer/offerBackend/create']],
    ['label' => Yii::t('OfferModule.offer', 'Акция') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('OfferModule.offer', 'Редактирование акции'), 'url' => [
        '/offer/offerBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('OfferModule.offer', 'Просмотреть акцию'), 'url' => [
        '/offer/offerBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('OfferModule.offer', 'Удалить акцию'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/offer/offerBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('OfferModule.offer', 'Вы уверены, что хотите удалить акцию?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OfferModule.offer', 'Редактирование') . ' ' . Yii::t('OfferModule.offer', 'акции'); ?>        <br/>
        <small>&laquo;<?=  $model->title; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>