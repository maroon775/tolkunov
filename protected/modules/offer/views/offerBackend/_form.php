<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Offer
 * @var $form yupe\widgets\ActiveForm
 * @var $this OfferBackendController
 **/
?>
<ul class="nav nav-tabs">
    <li class="active"><a href="#common" data-toggle="tab"><?= Yii::t("OfferModule.offer", "Общие"); ?></a></li>
    <li><a href="#images" data-toggle="tab"><?= Yii::t("OfferModule.offer", "Бонусы"); ?></a></li>
    <li><a href="#seo" data-toggle="tab"><?= Yii::t("OfferModule.offer", "SEO"); ?></a></li>
</ul>


<?php $form = $this->beginWidget(
    'yupe\widgets\ActiveForm', [
        'id' => 'offer-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well', 'enctype' => 'multipart/form-data'],
    ]
);
?>

<div class="alert alert-info">
    <?= Yii::t('OfferModule.offer', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?= Yii::t('OfferModule.offer', 'обязательны.'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class="tab-content">
    <div class="tab-pane active" id="common">
        <div class="row">
            <div class="col-sm-6">
                <?php echo $form->textFieldGroup($model, 'title', [
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'class' => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('title'),
                            'data-content' => $model->getAttributeDescription('title'),
                        ],
                    ],
                ]); ?>
            </div>
            <div class="col-sm-6">
                <?php echo $form->slugFieldGroup(
                    $model,
                    'slug',
                    [
                        'sourceAttribute' => 'title',
                        'widgetOptions' => [
                            'htmlOptions' => [
                                'class' => 'popover-help',
                                'data-original-title' => $model->getAttributeLabel('slug'),
                                'data-content' => $model->getAttributeDescription('slug'),
                                'placeholder' => 'Для автоматической генерации оставьте поле пустым',
                            ],
                        ],
                    ]
                ); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?php echo $form->dropDownListGroup(
                    $model,
                    'status',
                    [
                        'widgetOptions' => [
                            'data' => $model->getStatusList(),
                            'htmlOptions' => [
                                'empty' => Yii::t('OfferModule.offer', '--выберите--'),
                            ],
                        ],
                    ]
                ); ?>
            </div>
            <div class="col-sm-6">
                <?php echo $form->datePickerGroup($model, 'date', [
                    'widgetOptions' => [
                        'options' => [
                            'format' => 'yyyy-mm-dd',
//                        'showToday'  => true,
                            'autoclose' => true,
//                        'minuteStep' => 1,
//                        'todayBtn'   => true,
                        ],
                        'htmlOptions' => [],
                    ],
                    'prepend' => '<i class="fa fa-calendar"></i>',
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'coupons'); ?>
            </div>
        </div>

        <div class='row'>
            <div class="col-sm-7">
                <div class="preview-image-wrapper<?= !$model->getIsNewRecord() && $model->image ? '' : ' hidden' ?>">
                    <?=
                    CHtml::image(
                        !$model->getIsNewRecord() && $model->image ? $model->getImageUrl(200, 200, true) : '#',
                        $model->title,
                        [
                            'class' => 'preview-image img-thumbnail',
                            'style' => !$model->getIsNewRecord() && $model->image ? '' : 'display:none',
                        ]
                    ); ?>
                </div>

                <?php if (!$model->getIsNewRecord() && $model->image): ?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="delete-file"> <?= Yii::t(
                                'YupeModule.yupe',
                                'Delete the file'
                            ) ?>
                        </label>
                    </div>
                <?php endif; ?>

                <?= $form->fileFieldGroup(
                    $model,
                    'image',
                    [
                        'widgetOptions' => [
                            'htmlOptions' => [
                                'onchange' => 'readURL(this);',
                            ],
                        ],
                    ]
                ); ?>
            </div>
        </div>
        <div class='row'>
            <div class="col-sm-7">
                <div class="preview-image-wrapper<?= !$model->getIsNewRecord() && $model->image_splash ? '' : ' hidden' ?>">
                    <?=
                    CHtml::image(
                        !$model->getIsNewRecord() && $model->image_splash ? $model->imageSplashUpload->getImageUrl(200, 200, true) : '#',
                        $model->title,
                        [
                            'class' => 'img-thumbnail',
                            'style' => !$model->getIsNewRecord() && $model->image_splash ? '' : 'display:none',
                        ]
                    ); ?>
                </div>

                <?php if (!$model->getIsNewRecord() && $model->image_splash): ?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="delete-file-2"> <?= Yii::t(
                                'YupeModule.yupe',
                                'Delete the file'
                            ) ?>
                        </label>
                    </div>
                <?php endif; ?>

                <?= $form->fileFieldGroup(
                    $model,
                    'image_splash'
                ); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 <?= $model->hasErrors('description') ? 'has-error' : ''; ?>">
                <?= $form->labelEx($model, 'description'); ?>
                <?php $this->widget(
                    $this->module->getVisualEditor(),
                    [
                        'model' => $model,
                        'attribute' => 'description',
                    ]
                ); ?>
                <?= $form->error($model, 'description'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 <?= $model->hasErrors('text') ? 'has-error' : ''; ?>">
                <?= $form->labelEx($model, 'text'); ?>
                <?php $this->widget(
                    $this->module->getVisualEditor(),
                    [
                        'model' => $model,
                        'attribute' => 'text',
                    ]
                ); ?>
                <?= $form->error($model, 'text'); ?>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="images">
        <div class="row form-group">
            <div class="col-xs-2">
                <?= Yii::t("OfferModule.offer", "Бонусы"); ?>
            </div>
            <div class="col-xs-2">
                <button id="button-add-image" type="button" class="btn btn-default"><i class="fa fa-fw fa-plus"></i></button>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?php $imageModel = new OfferItem(); ?>
                <div id="product-images">
                    <div class="image-template hidden form-group">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for=""><?= Yii::t("OfferModule.offer", "Тариф"); ?></label>
                                <?= CHtml::dropDownList('', null, CHtml::listData(Tariff::model()->findAll(), 'id', 'title'), [
                                    'empty' => Yii::t('OfferModule.offer', '--choose--'),
                                    'class' => 'form-control image-tariff',
                                ]) ?>
                            </div>
                            <div class="col-xs-6">
                                <label for=""><?= Yii::t("OfferModule.offer", "Описание бонуса"); ?></label>
                                <input type="text" class="image-description form-control"/>
                            </div>
                            <div class="col-xs-2">
                                <label for=""><?= Yii::t("OfferModule.offer", "Бонус"); ?></label>
                                <input type="text" class="image-bonus form-control"/>
                            </div>
                            <div class="col-xs-1" style="padding-top: 24px">
                                <button class="button-delete-image btn btn-default" type="button"><i class="fa fa-fw fa-trash-o"></i></button>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if (!$model->getIsNewRecord() && $model->items): ?>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="col-xs-3"><?= Yii::t("OfferModule.offer", "Тариф"); ?></th>
                            <th class="col-xs-6"><?= Yii::t("OfferModule.offer", "Описание бонуса"); ?></th>
                            <th class="col-xs-2"><?= Yii::t("OfferModule.offer", "Бонус"); ?></th>
                            <th class="col-xs-1"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($model->items as $item): ?>
                            <tr>
                                <td>
                                    <?= CHtml::dropDownList(
                                        'OfferItem[' . $item->id . '][tariff_id]',
                                        $item->tariff_id,
                                        CHtml::listData(Tariff::model()->findAll(), 'id', 'title'),
                                        [
                                            'empty' => Yii::t('OfferModule.offer', '--choose--'),
                                            'class' => 'form-control',
                                        ]
                                    ) ?>
                                </td>
                                <td>
                                    <?= CHtml::textField('OfferItem[' . $item->id . '][description]', $item->description, ['class' => 'form-control']) ?>
                                </td>
                                <td>
                                    <?= CHtml::textField('OfferItem[' . $item->id . '][bonus]', $item->bonus, ['class' => 'form-control']) ?>
                                </td>
                                <td class="text-center">
                                    <a data-id="<?= $item->id; ?>" href="<?= Yii::app()->createUrl(
                                        '/offer/offerBackend/deleteItem',
                                        ['id' => $item->id]
                                    ); ?>" class="btn btn-default product-delete-image"><i
                                            class="fa fa-fw fa-trash-o"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="tab-pane" id="seo">
        <div class="row">
            <div class="col-sm-12">
                <?= $form->textFieldGroup($model, 'seo_title'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->textFieldGroup($model, 'seo_keywords'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->textAreaGroup($model, 'seo_description', ['widgetOptions' => ['htmlOptions' => ['rows' => 8]]]); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => Yii::t('OfferModule.offer', 'Сохранить акцию и продолжить'),
    ]
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label' => Yii::t('OfferModule.offer', 'Сохранить акцию и закрыть'),
    ]
); ?>

<?php $this->endWidget(); ?>
<script type="text/javascript">
    $(function () {

        $('#button-add-image').on('click', function () {
            var newImage = $("#product-images .image-template").clone().removeClass('image-template').removeClass('hidden');
            var key = $.now();

            newImage.appendTo("#product-images");
            newImage.find(".image-tariff").attr('name', 'OfferItem[new_' + key + '][tariff_id]');
            newImage.find(".image-description").attr('name', 'OfferItem[new_' + key + '][description]');
            newImage.find(".image-bonus").attr('name', 'OfferItem[new_' + key + '][bonus]');

            return false;
        });

        $('#product-images').on('click', '.button-delete-image', function () {
            $(this).closest('.row').remove();
        });

        $('.product-delete-image').on('click', function (event) {
            event.preventDefault();
            var blockForDelete = $(this).closest('tr');
            $.ajax({
                type: "POST",
                data: {
                    'id': $(this).data('id'),
                    '<?= Yii::app()->getRequest()->csrfTokenName;?>': '<?= Yii::app()->getRequest()->csrfToken;?>'
                },
                url: '<?= Yii::app()->createUrl('/offer/offerBackend/deleteItem');?>',
                success: function () {
                    blockForDelete.remove();
                }
            });
        });

        function activateFirstTabWithErrors() {
            var tab = $('.has-error').parents('.tab-pane').first();
            if (tab.length) {
                var id = tab.attr('id');
                $('a[href="#' + id + '"]').tab('show');
            }
        }

        activateFirstTabWithErrors();
    });
</script>
