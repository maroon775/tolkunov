<?php

/**
 * OfferController контроллер для offer на публичной части сайта
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2017 amyLabs && Yupe! team
 * @package yupe.modules.offer.controllers
 * @since 0.1
 *
 */
class OfferController extends \yupe\components\controllers\FrontController
{
    public function behaviors()
    {
        return [
            'seo' => ['class' => 'vendor.chemezov.yii-seo.behaviors.SeoBehavior'],
        ];
    }

    public function actionPrice()
    {
        $this->render('price');
    }

    public function actionIndex()
    {
        $criteria = new CDbCriteria;

        $dataProvider = new CActiveDataProvider(Offer::model()->published(), [
            'pagination' => [
                'class' => 'Pagination',
                'pageSize' => 5 * 4,
            ],
        ]);

        $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionCategory($slug)
    {
        $tariff = $this->loadTariff($slug);

        $dataProvider = new CActiveDataProvider(Offer::model()->published()->tariff($tariff->id), [
            'pagination' => [
                'class' => 'Pagination',
                'pageSize' => 5 * 4,
            ],
        ]);

        $this->render('index', ['dataProvider' => $dataProvider, 'current' => $tariff]);
    }

    public function actionView($slug)
    {
        $model = $this->loadModel($slug);

        preg_match_all('/{gallery_(\d+)}/', $model->text, $matches);

        if (count($matches[0]) > 0) {
            foreach ($matches[1] as $key => $gallery_id) {
                $model->text = str_replace($matches[0][$key], $this->renderEntity('Gallery', $gallery_id),
                    $model->text);
            }
        }

        $this->render('view', ['model' => $model]);
    }

    /**
     * @param string $slug
     * @return Tariff
     * @throws CHttpException
     */
    protected function loadTariff($slug)
    {
        $model = Tariff::model()->published()->findByAttributes(['slug' => $slug]);

        if (!$model) {
            throw new CHttpException(404);
        }

        return $model;
    }

    /**
     * @param string $slug
     * @return Offer
     * @throws CHttpException
     */
    protected function loadModel($slug)
    {
        $model = Offer::model()->published()->findByAttributes(['slug' => $slug]);

        if (!$model) {
            throw new CHttpException(404);
        }

        return $model;
    }

    public function renderEntity($modelClass, $id)
    {
        Yii::import('application.modules.' . strtolower($modelClass) . '.models.' . $modelClass, true);

        $model = CActiveRecord::model($modelClass)->findByPk($id);

        if (!$model) {
            return false;
        }

        return $this->renderPartial('_' . strtolower($modelClass), ['model' => $model], true);
    }

    public function actionCreate()
    {
        $model = new OfferFeedback();
        $fileModels = [];

        if (\Yii::app()->request->isPostRequest) {
            if (isset($_POST['OfferFeedback'])) {
                $validate = true;

                $model->attributes = $_POST['OfferFeedback'];

                $files = CUploadedFile::getInstancesByName('OfferFeedback[files]');

                if (isset($files) && count($files) > 0) {
                    // go through each uploaded image
                    foreach ($files as $file) {
                        $fileModel = new OfferFeedbackFile();
                        $fileModel->feedback_id = 1;
                        $fileModel->fileUpload->addFileInstance($file);

                        if (!$fileModel->validate()) {
                            $validate = false;
                        }

                        $fileModels[] = $fileModel;
                    }
                }

                if ($model->validate() && $validate) {
                    if (!$model->save()) {
                        Yii::app()->user->setFlash(
                            yupe\widgets\YFlashMessages::ERROR_MESSAGE,
                            Yii::t('OfferModule.offer', 'Не удалось сохранить модель!')
                        );
                    } else {
                        foreach ($fileModels as $fileModel) {
                            $fileModel->feedback_id = $model->id;

                            if (!$fileModel->save()) {
                                Yii::app()->user->setFlash(
                                    yupe\widgets\YFlashMessages::ERROR_MESSAGE,
                                    Yii::t('OfferModule.offer', 'Не удалось сохранить файл!')
                                );
                            }
                        }
                    }

                    $filesString = '';

                    foreach ($fileModels as $fileModel) {
                        $filesString .= CHtml::link($fileModel->file, $fileModel->getFileUrl()) . '<br>';
                    }

                    /* Send Email */
                    $mail = Yii::app()->mailMessage;

                    $result = $mail->raiseMailEvent('NEW_FEEDBACK', [
                        '{user_email}' => $model->email,
                        '{user_name}' => $model->name,
                        '{user_site}' => $model->site,
                        '{user_text}' => nl2br($model->text),
                        '{files}' => $filesString,
                        '{site_name}' => Yii::app()->getModule('yupe')->siteName,
                        '{admin_email}' => Yii::app()->getModule('yupe')->email,
                    ]);

                    /* End send email */

                    Yii::app()->user->setFlash(
                        yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                        Yii::t('OfferModule.offer', 'Запись добавлена!')
                    );
                }

//                if ($model->save()) {
//                    Yii::app()->user->setFlash(
//                        yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
//                        Yii::t('VacancyModule.vacancy', 'Запись добавлена!')
//                    );
//                }


            }

//            Yii::app()->clientScript->registerScript('success', 'var needReload = true; setTimeout($.fancybox.close, 1000);', CClientScript::POS_END);
        }

        $this->renderPartial('_form', ['model' => $model, 'fileModels' => $fileModels]);
    }
}
