<?php

/**
 * Класс OfferBackendController:
 *
 * @category Yupe\yupe\components\controllers\BackController
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
class OfferBackendController extends \yupe\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Offer.OfferBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Offer.OfferBackend.View']],
            ['allow', 'actions' => ['create', 'deleteItem'], 'roles' => ['Offer.OfferBackend.Create']],
            ['allow', 'actions' => ['update', 'inline', 'deleteItem'], 'roles' => ['Offer.OfferBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Offer.OfferBackend.Delete']],
            ['deny'],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'yupe\components\actions\YInLineEditAction',
                'model' => 'Offer',
                'validAttributes' => ['title', 'slug', 'status', 'coupons'],
            ],
        ];
    }

    /**
     * Отображает акцию по указанному идентификатору
     *
     * @param integer $id Идинтификатор акцию для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Создает новую модель акции.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new Offer;

        if (Yii::app()->getRequest()->getPost('Offer') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Offer'));

            if ($model->save()) {

                $this->updateItems($model);

                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('OfferModule.offer', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование акции.
     *
     * @param integer $id Идинтификатор акцию для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('Offer') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Offer'));

            if ($model->save()) {

                $this->updateItems($model);

                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('OfferModule.offer', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель акции из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор акции, который нужно удалить
     *
     * @return void
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('OfferModule.offer', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t('OfferModule.offer', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
        }
    }

    /**
     * Управление акциями.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new Offer('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('Offer') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('Offer'));
        }
        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param $id integer идентификатор нужной модели
     *
     * @return Offer
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Offer::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('OfferModule.offer', 'Запрошенная страница не найдена.'));
        }

        return $model;
    }

    /**
     * @param Offer $model
     */
    protected function updateItems(Offer $model)
    {
        if (Yii::app()->getRequest()->getPost('OfferItem')) {
            foreach (Yii::app()->getRequest()->getPost('OfferItem') as $key => $val) {
                $productImage = OfferItem::model()->findByPk($key);
                if (null === $productImage) {
                    $productImage = new OfferItem();
                    $productImage->offer_id = $model->id;
                }
                $productImage->setAttributes($_POST['OfferItem'][$key]);
                if (false === $productImage->save()) {
                    Yii::app()->getUser()->setFlash(\yupe\widgets\YFlashMessages::WARNING_MESSAGE,
                        Yii::t('OfferModule.offer', 'Произошла ошибка при сохранении бонусов...'));

                    Yii::app()->getUser()->setFlash(\yupe\widgets\YFlashMessages::ERROR_MESSAGE, (new CActiveForm())->errorSummary($productImage));
                }
            }
        }
    }

    /**
     * @throws CHttpException
     */
    public function actionDeleteItem()
    {
        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getIsAjaxRequest()) {

            $id = (int)Yii::app()->getRequest()->getPost('id');

            $model = OfferItem::model()->findByPk($id);

            if (null !== $model && $model->delete()) {
                Yii::app()->ajax->success();
            }
        }

        throw new CHttpException(404);
    }
}
