<?php

class m170412_053314_add_offer_image2_and_coupons extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{offer_offer}}', 'image_splash', 'string');
        $this->addColumn('{{offer_offer}}', 'coupons', 'string');
    }

    public function safeDown()
    {
        $this->dropColumn('{{offer_offer}}', 'image_splash');
        $this->dropColumn('{{offer_offer}}', 'coupons');
    }
}
