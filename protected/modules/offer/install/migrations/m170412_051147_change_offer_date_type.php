<?php

class m170412_051147_change_offer_date_type extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->alterColumn('{{offer_offer}}', 'date', 'DATE DEFAULT NULL');
    }

    public function safeDown()
    {
        $this->alterColumn('{{offer_offer}}', 'date', 'DATE NOT NULL');
    }
}
