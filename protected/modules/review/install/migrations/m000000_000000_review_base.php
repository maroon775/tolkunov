<?php

/**
 * Review install migration
 * Класс миграций для модуля Review:
 *
 * @category YupeMigration
 * @package  yupe.modules.review.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000000_review_base extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{review_social_network}}',
            [
                'id' => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'title' => 'string NOT NULL',
                'image' => 'string',
            ],
            $this->getOptions()
        );

        $this->createTable(
            '{{review_forum}}',
            [
                'id' => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'title' => 'string NOT NULL',
                'image' => 'string',
            ],
            $this->getOptions()
        );

        $this->createTable(
            '{{review_review}}',
            [
                'id' => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'type' => 'integer NOT NULL',
                'client_id' => 'integer NOT NULL',
                'date' => 'DATE NOT NULL',
                'social_network_id' => 'integer DEFAULT NULL',
                'forum_id' => 'integer DEFAULT NULL',
                'status' => 'tinyint(1) NOT NULL DEFAULT 0',
                'text' => 'text',
                'video_link' => 'string',
                'image' => 'string',
                'link' => 'string',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{review_review}}_date", '{{review_review}}', "date", false);
        $this->createIndex("ix_{{review_review}}_type", '{{review_review}}', "type", false);
        $this->createIndex("ix_{{review_review}}_status", '{{review_review}}', "status", false);

        //fk
        $this->addForeignKey('fk_{{review_review}}_client_id', '{{review_review}}', 'client_id', '{{client_client}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_{{review_review}}_social_network_id', '{{review_review}}', 'social_network_id', '{{review_social_network}}', 'id', 'SET NULL');
        $this->addForeignKey('fk_{{review_review}}_forum_id', '{{review_review}}', 'forum_id', '{{review_forum}}', 'id', 'SET NULL');
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{review_review}}');
        $this->dropTableWithForeignKeys('{{review_forum}}');
        $this->dropTableWithForeignKeys('{{review_social_network}}');
    }
}
