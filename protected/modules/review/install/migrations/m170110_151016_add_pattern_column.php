<?php

class m170110_151016_add_pattern_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{review_review}}', 'pattern', 'string');
    }

    public function safeDown()
    {
        $this->dropColumn('{{review_review}}', 'pattern');
    }
}
