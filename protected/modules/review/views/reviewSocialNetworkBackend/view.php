<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model ReviewSocialNetwork
 *   @var $this ReviewSocialNetworkBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('ReviewModule.review', 'Социальные сети') => ['/review/reviewSocialNetworkBackend/index'],
    $model->title,
];

$this->pageTitle = Yii::t('ReviewModule.review', 'Социальные сети - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ReviewModule.review', 'Управление социальными сетями'), 'url' => ['/review/reviewSocialNetworkBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ReviewModule.review', 'Добавить социальную сеть'), 'url' => ['/review/reviewSocialNetworkBackend/create']],
    ['label' => Yii::t('ReviewModule.review', 'Социальная сеть') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('ReviewModule.review', 'Редактирование социальной сети'), 'url' => [
        '/review/reviewSocialNetworkBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('ReviewModule.review', 'Просмотреть социальную сеть'), 'url' => [
        '/review/reviewSocialNetworkBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('ReviewModule.review', 'Удалить социальную сеть'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/review/reviewSocialNetworkBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('ReviewModule.review', 'Вы уверены, что хотите удалить социальную сеть?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('ReviewModule.review', 'Просмотр') . ' ' . Yii::t('ReviewModule.review', 'социальной сети'); ?>        <br/>
        <small>&laquo;<?=  $model->title; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'create_time',
        'update_time',
        'title',
        'image',
    ],
]); ?>
