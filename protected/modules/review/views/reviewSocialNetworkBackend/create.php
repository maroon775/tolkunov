<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model ReviewSocialNetwork
 *   @var $this ReviewSocialNetworkBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('ReviewModule.review', 'Социальные сети') => ['/review/reviewSocialNetworkBackend/index'],
    Yii::t('ReviewModule.review', 'Добавление'),
];

$this->pageTitle = Yii::t('ReviewModule.review', 'Социальные сети - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ReviewModule.review', 'Управление социальными сетями'), 'url' => ['/review/reviewSocialNetworkBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ReviewModule.review', 'Добавить социальную сеть'), 'url' => ['/review/reviewSocialNetworkBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('ReviewModule.review', 'Социальные сети'); ?>
        <small><?=  Yii::t('ReviewModule.review', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>