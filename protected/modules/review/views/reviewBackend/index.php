<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Review
 * @var $this ReviewBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('ReviewModule.review', 'Отзывы') => ['/review/reviewBackend/index'],
    Yii::t('ReviewModule.review', 'Управление'),
];

$this->pageTitle = Yii::t('ReviewModule.review', 'Отзывы - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ReviewModule.review', 'Управление отзывами'), 'url' => ['/review/reviewBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ReviewModule.review', 'Добавить отзыв'), 'url' => ['/review/reviewBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('ReviewModule.review', 'Отзывы'); ?>
        <small><?= Yii::t('ReviewModule.review', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?= Yii::t('ReviewModule.review', 'Поиск отзывов'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('review-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<br/>

<p> <?= Yii::t('ReviewModule.review', 'В данном разделе представлены средства управления отзывами'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id' => 'review-grid',
        'type' => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'id',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->id, ["update", "id" => $data->id]);
                },
                'htmlOptions' => ['width' => '100px'],
            ],
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'type',
                'url' => $this->createUrl('inline'),
                'source' => $model->getTypeList(),
                'editable' => [
                    'emptytext' => '---',
                ],
            ],
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'client_id',
                'url' => $this->createUrl('inline'),
                'source' => CHtml::listData(Client::model()->findAll(), 'id', 'title'),
                'editable' => [
                    'emptytext' => '---',
                ],
            ],
            'date',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    $model::STATUS_PUBLISHED => ['class' => 'label-success'],
                    $model::STATUS_MODERATION => ['class' => 'label-warning'],
                    $model::STATUS_DRAFT => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'social_network_id',
                'url' => $this->createUrl('inline'),
                'source' => CHtml::listData(ReviewSocialNetwork::model()->findAll(), 'id', 'title'),
                'editable' => [
                    'emptytext' => '---',
                ],
            ],
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'forum_id',
                'url' => $this->createUrl('inline'),
                'source' => CHtml::listData(ReviewForum::model()->findAll(), 'id', 'title'),
                'editable' => [
                    'emptytext' => '---',
                ],
            ],
//            'create_time',
//            'update_time',
//            'text',
//            'video_link',
//            'image',
//            'link',
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
