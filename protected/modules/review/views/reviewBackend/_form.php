<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Review
 * @var $form yupe\widgets\ActiveForm
 * @var $this ReviewBackendController
 **/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm', [
        'id' => 'review-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well', 'enctype' => 'multipart/form-data'],
    ]
);
?>

    <div class="alert alert-info">
        <?= Yii::t('ReviewModule.review', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?= Yii::t('ReviewModule.review', 'обязательны.'); ?>
    </div>

<?= $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->dropDownListGroup(
                $model,
                'type',
                [
                    'widgetOptions' => [
                        'data' => $model->getTypeList(),
                        'htmlOptions' => [
                            'class' => 'popover-help',
                            'empty' => Yii::t('ReviewModule.review', '--выберите--'),
                            'data-original-title' => $model->getAttributeLabel('type'),
                            'data-content' => $model->getAttributeDescription('type'),
                            'data-container' => 'body',
                        ],
                    ],
                ]
            ); ?>
        </div>
        <div class="col-sm-6">
            <?= $form->select2Group($model, 'client_id', [
                'widgetOptions' => [
                    'data' => CHtml::listData(Client::model()->findAll(), 'id', 'title'),
                    'options' => [
                        'placeholder' => Yii::t('ReviewModule.review', 'Выберите клиента'),
                        'width' => '100%',
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->dropDownListGroup(
                $model,
                'status',
                [
                    'widgetOptions' => [
                        'data' => $model->getStatusList(),
                        'htmlOptions' => [
                            'class' => 'popover-help',
                            'empty' => Yii::t('ReviewModule.review', '--выберите--'),
                            'data-original-title' => $model->getAttributeLabel('status'),
                            'data-content' => $model->getAttributeDescription('status'),
                            'data-container' => 'body',
                        ],
                    ],
                ]
            ); ?>
        </div>
        <div class="col-sm-6">
            <?php echo $form->datePickerGroup($model, 'date', [
                'widgetOptions' => [
                    'options' => [
                        'format' => 'yyyy-mm-dd',
//                        'showToday'  => true,
                        'autoclose' => true,
//                        'minuteStep' => 1,
//                        'todayBtn'   => true,
                    ],
                    'htmlOptions' => [],
                ],
                'prepend' => '<i class="fa fa-calendar"></i>',
            ]); ?>
        </div>
    </div>
    <div class="row" data-type-3="yes">
        <div class="col-sm-6">
            <?= $form->select2Group($model, 'social_network_id', [
                'widgetOptions' => [
                    'data' => CHtml::listData(ReviewSocialNetwork::model()->findAll(), 'id', 'title'),
                    'options' => [
                        'placeholder' => Yii::t('ReviewModule.review', 'Выберите социальную сеть'),
                        'width' => '100%',
                        'allowClear' => true,
                    ],
                ],
                'hint' => 'Для отзывов с социальных сетей',
            ]); ?>
        </div>
    </div>
    <div class="row" data-type-5="yes">
        <div class="col-sm-6">
            <?= $form->select2Group($model, 'forum_id', [
                'widgetOptions' => [
                    'data' => CHtml::listData(ReviewForum::model()->findAll(), 'id', 'title'),
                    'options' => [
                        'placeholder' => Yii::t('ReviewModule.review', 'Выберите форум'),
                        'width' => '100%',
                        'allowClear' => true,
                    ],
                ],
                'hint' => 'Для отзывов с форума',
            ]); ?>
        </div>
    </div>
    <div class="row" data-type-3="yes" data-type-4="yes" data-type-5="yes">
        <div class="col-sm-12 <?= $model->hasErrors('text') ? 'has-error' : ''; ?>">
            <?= $form->labelEx($model, 'text'); ?>
            <?php $this->widget(
                $this->module->getVisualEditor(),
                [
                    'model' => $model,
                    'attribute' => 'text',
                ]
            ); ?>
            <?= $form->error($model, 'text'); ?>
        </div>
    </div>
    <div class="row" data-type-1="yes">
        <div class="col-sm-12">
            <?= $form->textFieldGroup($model, 'video_link', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('video_link'),
                        'data-content' => $model->getAttributeDescription('video_link'),
                    ],
                ],
                'hint' => 'Для видеоотзывов. Пример: <code>http://www.youtube.com/watch?v=opj24KnzrWo</code>',
            ]); ?>
        </div>
    </div>
    <div class="row" data-type-2="yes">
        <div class="col-sm-7">
            <div class="preview-image-wrapper<?= !$model->getIsNewRecord() && $model->image ? '' : ' hidden' ?>">
                <?=
                CHtml::image(
                    !$model->getIsNewRecord() && $model->image ? $model->getImageUrl(200, 200, true) : '#',
                    '',
                    [
                        'class' => 'preview-image img-thumbnail',
                        'style' => !$model->getIsNewRecord() && $model->image ? '' : 'display:none',
                    ]
                ); ?>
            </div>

            <?php if (!$model->getIsNewRecord() && $model->image): ?>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="delete-file"> <?= Yii::t(
                            'YupeModule.yupe',
                            'Delete the file'
                        ) ?>
                    </label>
                </div>
            <?php endif; ?>

            <?= $form->fileFieldGroup(
                $model,
                'image',
                [
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'onchange' => 'readURL(this);',
                        ],
                    ],
                    'hint' => 'Для рекомендательных писем',
                ]
            ); ?>
        </div>
    </div>
    <div class="row" data-type-2="yes">
        <div class="col-sm-7">
            <div class="preview-image-wrapper<?= !$model->getIsNewRecord() && $model->pattern ? '' : ' hidden' ?>">
                <?=
                CHtml::image(
                    !$model->getIsNewRecord() && $model->pattern ? $model->patternUpload->getImageUrl(200, 200, true) : '#',
                    '#',
                    [
                        'class' => 'img-thumbnail',
                        'style' => !$model->getIsNewRecord() && $model->pattern ? '' : 'display:none',
                    ]
                ); ?>
            </div>

            <?php if (!$model->getIsNewRecord() && $model->pattern): ?>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="delete-file-2"> <?= Yii::t(
                            'YupeModule.yupe',
                            'Delete the file'
                        ) ?>
                    </label>
                </div>
            <?php endif; ?>

            <?= $form->fileFieldGroup(
                $model,
                'pattern'
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->textFieldGroup($model, 'link', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('link'),
                        'data-content' => $model->getAttributeDescription('link'),
                    ],
                ],
            ]); ?>
        </div>
    </div>

    <script>
        function showFields() {
            for (var i = 1; i <= 5; i++) {
                $('[data-type-' + i + '="yes"]').addClass('hidden');
            }

            var type = $('#Review_type').val();

            $('[data-type-' + type + '="yes"]').removeClass('hidden');

            /* Hack for Redactor */
            window.scrollBy(0, 1);
        }

        $('#Review_type').on('change', showFields);

        $(showFields);
    </script>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => Yii::t('ReviewModule.review', 'Сохранить отзыв и продолжить'),
    ]
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label' => Yii::t('ReviewModule.review', 'Сохранить отзыв и закрыть'),
    ]
); ?>

<?php $this->endWidget(); ?>