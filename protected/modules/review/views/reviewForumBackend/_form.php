<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model ReviewForum
 * @var $form yupe\widgets\ActiveForm
 * @var $this ReviewForumBackendController
 **/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm', [
        'id' => 'review-forum-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well', 'enctype' => 'multipart/form-data'],
    ]
);
?>

    <div class="alert alert-info">
        <?= Yii::t('ReviewModule.review', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?= Yii::t('ReviewModule.review', 'обязательны.'); ?>
    </div>

<?= $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-12">
            <?= $form->textFieldGroup($model, 'title', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('title'),
                        'data-content' => $model->getAttributeDescription('title'),
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <div class='row'>
        <div class="col-sm-7">
            <div class="preview-image-wrapper<?= !$model->getIsNewRecord() && $model->image ? '' : ' hidden' ?>">
                <?=
                CHtml::image(
                    !$model->getIsNewRecord() && $model->image ? $model->getImageUrl(25, 25, true) : '#',
                    $model->title,
                    [
                        'class' => 'preview-image img-thumbnail',
                        'style' => !$model->getIsNewRecord() && $model->image ? '' : 'display:none',
                    ]
                ); ?>
            </div>

            <?php if (!$model->getIsNewRecord() && $model->image): ?>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="delete-file"> <?= Yii::t(
                            'YupeModule.yupe',
                            'Delete the file'
                        ) ?>
                    </label>
                </div>
            <?php endif; ?>

            <?= $form->fileFieldGroup(
                $model,
                'image',
                [
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'onchange' => 'readURL(this);',
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => Yii::t('ReviewModule.review', 'Сохранить форум и продолжить'),
    ]
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label' => Yii::t('ReviewModule.review', 'Сохранить форум и закрыть'),
    ]
); ?>

<?php $this->endWidget(); ?>