<?php

/**
 * Класс ReviewForumBackendController:
 *
 * @category Yupe\yupe\components\controllers\BackController
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
class ReviewForumBackendController extends \yupe\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Review.ReviewForumBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Review.ReviewForumBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Review.ReviewForumBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Review.ReviewForumBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Review.ReviewForumBackend.Delete']],
            ['deny'],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'yupe\components\actions\YInLineEditAction',
                'model' => 'ReviewForum',
                'validAttributes' => ['title'],
            ],
        ];
    }

    /**
     * Отображает форум по указанному идентификатору
     *
     * @param integer $id Идинтификатор форум для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Создает новую модель форума.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new ReviewForum;

        if (Yii::app()->getRequest()->getPost('ReviewForum') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('ReviewForum'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ReviewModule.review', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование форума.
     *
     * @param integer $id Идинтификатор форум для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('ReviewForum') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('ReviewForum'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ReviewModule.review', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель форума из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор форума, который нужно удалить
     *
     * @return void
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('ReviewModule.review', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t('ReviewModule.review', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
        }
    }

    /**
     * Управление форумами.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new ReviewForum('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('ReviewForum') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('ReviewForum'));
        }
        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param $id integer идентификатор нужной модели
     *
     * @return ReviewForum    * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = ReviewForum::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('ReviewModule.review', 'Запрошенная страница не найдена.'));
        }

        return $model;
    }
}
