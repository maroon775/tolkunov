<?php

/**
 * This is the model class for table "{{tariff_tariff}}".
 *
 * The followings are the available columns in table '{{tariff_tariff}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property integer $type
 * @property string $title
 * @property string $slug
 * @property integer $color
 * @property string $price
 * @property integer $status
 * @property integer $sort
 * @property integer $is_recommended
 * @property integer $is_wide
 *
 * The followings are the available model relations:
 * @property TariffProperty[] $properties
 * @property integer $propertiesCount
 *
 * @method Tariff published()
 */
class Tariff extends yupe\models\YModel
{
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const STATUS_MODERATION = 2;

    const TYPE_HTML5 = 1;
    const TYPE_GIF = 2;
    const TYPE_JPG = 3;

    const COLOR_GREEN = 1;
    const COLOR_RED = 2;
    const COLOR_BLUE = 3;
    const COLOR_PURPLE = 4;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{tariff_tariff}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['title, price', 'filter', 'filter' => 'trim'],
            ['title, price', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],
            ['type, title, slug, color, price', 'required'],
            ['type, color, status, sort', 'numerical', 'integerOnly' => true],
            ['is_recommended, is_wide', 'boolean'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['type', 'in', 'range' => array_keys($this->getTypeList())],
            ['color', 'in', 'range' => array_keys($this->getColorList())],
            ['title, price', 'length', 'max' => 255],
            ['slug', 'length', 'max' => 150],
            ['slug', 'unique'],
            [
                'slug',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('TariffModule.tariff', 'Bad characters in {attribute} field'),
            ],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, type, title, slug, color, price, status, sort, is_recommended, is_wide', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => $this->tableAlias . '.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
        ];
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function type($id)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->tableAlias . '.type = :type',
            'params' => [':type' => $id],
        ]);

        return $this;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('tariff');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
            'sortable' => [
                'class' => 'yupe\components\behaviors\SortableBehavior',
                'attributeName' => 'sort',
            ],
            'seo' => [
                'class' => 'vendor.chemezov.yii-seo.behaviors.SeoActiveRecordBehavior',
                'route' => '/offer/offer/category',
                'params' => [
                    'slug' => function (Tariff $data) {
                        return $data->slug;
                    },
                ],
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'properties' => [self::HAS_MANY, 'TariffProperty', 'tariff_id', 'order' => 'properties.sort'],
            'propertiesCount' => [self::STAT, 'TariffProperty', 'tariff_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата обновления',
            'type' => 'Тип',
            'title' => 'Заголовок',
            'slug' => 'URL',
            'color' => 'Цвет',
            'price' => 'Цена',
            'status' => 'Статус',
            'sort' => 'Сортировка',
            'is_recommended' => 'Рекомендуем',
            'is_wide' => 'Широкий блок',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.type', $this->type);
        $criteria->compare($this->tableAlias . '.title', $this->title, true);
        $criteria->compare($this->tableAlias . '.slug', $this->slug, true);
        $criteria->compare($this->tableAlias . '.color', $this->color);
        $criteria->compare($this->tableAlias . '.price', $this->price, true);
        $criteria->compare($this->tableAlias . '.status', $this->status);
        $criteria->compare($this->tableAlias . '.sort', $this->sort);
        $criteria->compare($this->tableAlias . '.is_recommended', $this->is_recommended);
        $criteria->compare($this->tableAlias . '.is_wide', $this->is_wide);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'sort' => [
                'defaultOrder' => $this->tableAlias . '.sort',
            ],
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Tariff the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('TariffModule.tariff', 'Draft'),
            self::STATUS_MODERATION => Yii::t('TariffModule.tariff', 'On moderation'),
            self::STATUS_PUBLISHED => Yii::t('TariffModule.tariff', 'Published'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('TariffModule.tariff', '*unknown*');
    }

    /**
     * @return array
     */
    public function getTypeList()
    {
        return [
            self::TYPE_HTML5 => Yii::t('TariffModule.tariff', 'HTML5'),
            self::TYPE_GIF => Yii::t('TariffModule.tariff', 'GIF'),
            self::TYPE_JPG => Yii::t('TariffModule.tariff', 'JPG'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getType()
    {
        $data = $this->getTypeList();

        return isset($data[$this->type]) ? $data[$this->type] : Yii::t('TariffModule.tariff', '*unknown*');
    }

    /**
     * @return array
     */
    public function getColorList()
    {
        return [
            self::COLOR_GREEN => Yii::t('TariffModule.tariff', 'Зелёный'),
            self::COLOR_RED => Yii::t('TariffModule.tariff', 'Красный'),
            self::COLOR_BLUE => Yii::t('TariffModule.tariff', 'Голубой'),
            self::COLOR_PURPLE => Yii::t('TariffModule.tariff', 'Фиолетовый'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getColor()
    {
        $data = $this->getColorList();

        return isset($data[$this->color]) ? $data[$this->color] : Yii::t('TariffModule.tariff', '*unknown*');
    }

    /**
     * @return array
     */
    public function getColorStringList()
    {
        return [
            self::COLOR_GREEN => 'green',
            self::COLOR_RED => 'red',
            self::COLOR_BLUE => 'blue',
            self::COLOR_PURPLE => 'purple',
        ];
    }

    /**
     * @return mixed|string
     */
    public function getColorString()
    {
        $data = $this->getColorStringList();

        return isset($data[$this->color]) ? $data[$this->color] : null;
    }

    /**
     * @return array
     */
    public function getYesNoList()
    {
        return [
            Yii::t('TariffModule.tariff', 'No'),
            Yii::t('TariffModule.tariff', 'Yes'),
        ];
    }

    public static function getMinPriceForType($type)
    {
        return Yii::app()->db->createCommand('SELECT MIN(price) FROM {{tariff_tariff}} WHERE type = :type')->queryScalar([':type' => $type]);
    }
}
