<?php

/**
 * This is the model class for table "{{tariff_property}}".
 *
 * The followings are the available columns in table '{{tariff_property}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property integer $tariff_id
 * @property string $title
 * @property string $description
 * @property integer $sort
 *
 * The followings are the available model relations:
 * @property Tariff $tariff
 */
class TariffProperty extends yupe\models\YModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{tariff_property}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['title, description', 'filter', 'filter' => 'trim'],
            ['title, description', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],
            ['tariff_id, title', 'required'],
            ['tariff_id, sort', 'numerical', 'integerOnly' => true],
            ['tariff_id', 'exist', 'className' => 'Tariff', 'attributeName' => 'id'],
            ['title', 'length', 'max' => 255],
            ['description', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, tariff_id, title, description, sort', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('tariff');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
            'sortable' => [
                'class' => 'yupe\components\behaviors\SortableBehavior',
                'attributeName' => 'sort',
            ],
//            'seo' => [
//                'class' => 'vendor.chemezov.yii-seo.behaviors.SeoActiveRecordBehavior',
//                'route' => '/portfolio/portfolio/view',
//                'params' => [
//                    'slug' => function (Portfolio $data) {
//                        return $data->slug;
//                    },
//                ],
//            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'tariff' => [self::BELONGS_TO, 'Tariff', 'tariff_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата обновления',
            'tariff_id' => 'Тариф',
            'title' => 'Текст',
            'description' => 'Описание',
            'sort' => 'Сортировка',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.tariff_id', $this->tariff_id);
        $criteria->compare($this->tableAlias . '.title', $this->title, true);
        $criteria->compare($this->tableAlias . '.description', $this->description, true);
        $criteria->compare($this->tableAlias . '.sort', $this->sort);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'sort' => [
                'defaultOrder' => $this->tableAlias . '.sort',
            ],
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TariffProperty the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getTitle()
    {
        $title = str_replace('₽', '<span class="b-prices__rub">₽</span>', $this->title);
        $title = str_replace('[', '<b>', $title);
        $title = str_replace(']', '</b>', $title);

        return $title;
    }
}
