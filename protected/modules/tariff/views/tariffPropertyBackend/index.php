<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model TariffProperty
 * @var $this TariffPropertyBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('TariffModule.tariff', 'Особенности тарифа') => ['/tariff/tariffPropertyBackend/index'],
    Yii::t('TariffModule.tariff', 'Управление'),
];

$this->pageTitle = Yii::t('TariffModule.tariff', 'Особенности тарифа - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('TariffModule.tariff', 'Управление особенностями тарифа'), 'url' => ['/tariff/tariffPropertyBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('TariffModule.tariff', 'Добавить особенность тарифа'), 'url' => ['/tariff/tariffPropertyBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('TariffModule.tariff', 'Особенности тарифа'); ?>
        <small><?= Yii::t('TariffModule.tariff', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?= Yii::t('TariffModule.tariff', 'Поиск особенностей тарифа'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('tariff-property-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<br/>

<p> <?= Yii::t('TariffModule.tariff', 'В данном разделе представлены средства управления особенностями тарифа'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id' => 'tariff-property-grid',
        'type' => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'sort',
        'sortableAction' => '/tariff/tariffPropertyBackend/sortable',
        'columns' => [
            'id',
            'create_time',
            'update_time',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'tariff_id',
                'url' => $this->createUrl('inline'),
                'source' => CHtml::listData(Tariff::model()->findAll(), 'id', 'title'),
                'editable' => [
                    'emptytext' => '---',
                ],
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'title',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'description',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'description', ['class' => 'form-control']),
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
