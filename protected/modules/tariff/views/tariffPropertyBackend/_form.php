<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model TariffProperty
 * @var $form yupe\widgets\ActiveForm
 * @var $this TariffPropertyBackendController
 **/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm', [
        'id' => 'tariff-property-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well'],
    ]
);
?>

    <div class="alert alert-info">
        <?= Yii::t('TariffModule.tariff', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?= Yii::t('TariffModule.tariff', 'обязательны.'); ?>
    </div>

<?= $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-12">
            <?= $form->select2Group($model, 'tariff_id', [
                'widgetOptions' => [
                    'data' => CHtml::listData(Tariff::model()->findAll(), 'id', 'title'),
                    'options' => [
                        'placeholder' => Yii::t('TariffModule.tariff', 'Выберите тариф'),
                        'width' => '100%',
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->textFieldGroup($model, 'title', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('title'),
                        'data-content' => $model->getAttributeDescription('title'),
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->textAreaGroup($model, 'description', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'rows' => 6,
                        'cols' => 50,
                        'data-original-title' => $model->getAttributeLabel('description'),
                        'data-content' => $model->getAttributeDescription('description'),
                    ],
                ],
            ]); ?>
        </div>
    </div>


<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => Yii::t('TariffModule.tariff', 'Сохранить особенность тарифа и продолжить'),
    ]
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label' => Yii::t('TariffModule.tariff', 'Сохранить особенность тарифа и закрыть'),
    ]
); ?>

<?php $this->endWidget(); ?>