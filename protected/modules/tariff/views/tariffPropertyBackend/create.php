<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model TariffProperty
 *   @var $this TariffPropertyBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('TariffModule.tariff', 'Особенности тарифа') => ['/tariff/tariffPropertyBackend/index'],
    Yii::t('TariffModule.tariff', 'Добавление'),
];

$this->pageTitle = Yii::t('TariffModule.tariff', 'Особенности тарифа - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('TariffModule.tariff', 'Управление особенностями тарифа'), 'url' => ['/tariff/tariffPropertyBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('TariffModule.tariff', 'Добавить особенность тарифа'), 'url' => ['/tariff/tariffPropertyBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('TariffModule.tariff', 'Особенности тарифа'); ?>
        <small><?=  Yii::t('TariffModule.tariff', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>