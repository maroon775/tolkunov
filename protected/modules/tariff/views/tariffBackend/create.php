<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model Tariff
 *   @var $this TariffBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('TariffModule.tariff', 'Тарифы') => ['/tariff/tariffBackend/index'],
    Yii::t('TariffModule.tariff', 'Добавление'),
];

$this->pageTitle = Yii::t('TariffModule.tariff', 'Тарифы - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('TariffModule.tariff', 'Управление тарифами'), 'url' => ['/tariff/tariffBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('TariffModule.tariff', 'Добавить тариф'), 'url' => ['/tariff/tariffBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('TariffModule.tariff', 'Тарифы'); ?>
        <small><?=  Yii::t('TariffModule.tariff', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>