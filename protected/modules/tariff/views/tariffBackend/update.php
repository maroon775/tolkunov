<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model Tariff
 *   @var $this TariffBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('TariffModule.tariff', 'Тарифы') => ['/tariff/tariffBackend/index'],
    $model->title => ['/tariff/tariffBackend/view', 'id' => $model->id],
    Yii::t('TariffModule.tariff', 'Редактирование'),
];

$this->pageTitle = Yii::t('TariffModule.tariff', 'Тарифы - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('TariffModule.tariff', 'Управление тарифами'), 'url' => ['/tariff/tariffBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('TariffModule.tariff', 'Добавить тариф'), 'url' => ['/tariff/tariffBackend/create']],
    ['label' => Yii::t('TariffModule.tariff', 'Тариф') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('TariffModule.tariff', 'Редактирование тарифа'), 'url' => [
        '/tariff/tariffBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('TariffModule.tariff', 'Просмотреть тариф'), 'url' => [
        '/tariff/tariffBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('TariffModule.tariff', 'Удалить тариф'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/tariff/tariffBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('TariffModule.tariff', 'Вы уверены, что хотите удалить тариф?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('TariffModule.tariff', 'Редактирование') . ' ' . Yii::t('TariffModule.tariff', 'тарифа'); ?>        <br/>
        <small>&laquo;<?=  $model->title; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>