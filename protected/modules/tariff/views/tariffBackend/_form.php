<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Tariff
 * @var $form yupe\widgets\ActiveForm
 * @var $this TariffBackendController
 **/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm', [
        'id' => 'tariff-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well'],
    ]
);
?>

    <div class="alert alert-info">
        <?= Yii::t('TariffModule.tariff', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?= Yii::t('TariffModule.tariff', 'обязательны.'); ?>
    </div>

<?= $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->textFieldGroup($model, 'title'); ?>
        </div>
        <div class="col-sm-6">
            <?php echo $form->slugFieldGroup(
                $model,
                'slug',
                [
                    'sourceAttribute' => 'title',
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'placeholder' => 'Для автоматической генерации оставьте поле пустым',
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'status',
                [
                    'widgetOptions' => [
                        'data' => $model->getStatusList(),
                    ],
                ]
            ); ?>
        </div>
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'type',
                [
                    'widgetOptions' => [
                        'data' => $model->getTypeList(),
                    ],
                ]
            ); ?>

        </div>
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'color',
                [
                    'widgetOptions' => [
                        'data' => $model->getColorList(),
                    ],
                ]
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->textFieldGroup($model, 'price', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('price'),
                        'data-content' => $model->getAttributeDescription('price'),
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->checkboxGroup($model, 'is_recommended'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->checkboxGroup($model, 'is_wide'); ?>
        </div>
    </div>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => Yii::t('TariffModule.tariff', 'Сохранить тариф и продолжить'),
    ]
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label' => Yii::t('TariffModule.tariff', 'Сохранить тариф и закрыть'),
    ]
); ?>

<?php $this->endWidget(); ?>