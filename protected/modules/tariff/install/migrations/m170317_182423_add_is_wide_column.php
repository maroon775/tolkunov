<?php

class m170317_182423_add_is_wide_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{tariff_tariff}}', 'is_wide', 'boolean NOT NULL DEFAULT FALSE');
    }

    public function safeDown()
    {
        $this->dropColumn('{{tariff_tariff}}', 'is_wide');
    }
}
