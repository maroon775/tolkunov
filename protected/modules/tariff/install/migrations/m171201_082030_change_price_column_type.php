<?php

class m171201_082030_change_price_column_type extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->alterColumn('{{tariff_tariff}}', 'price', 'integer');
    }

    public function safeDown()
    {
        $this->alterColumn('{{tariff_tariff}}', 'price', 'string');
    }
}
