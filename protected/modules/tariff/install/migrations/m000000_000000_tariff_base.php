<?php

/**
 * Tariff install migration
 * Класс миграций для модуля Tariff:
 *
 * @category YupeMigration
 * @package  yupe.modules.tariff.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000000_tariff_base extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{tariff_tariff}}',
            [
                'id' => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'type' => 'tinyint(1) NOT NULL',
                'title' => 'string NOT NULL',
                'slug' => 'varchar(150) NOT NULL',
                'color' => 'tinyint(1) NOT NULL',
                'price' => 'string',
                'status' => 'tinyint(1) NOT NULL DEFAULT 0',
                'sort' => 'integer NOT NULL DEFAULT 1',
                'is_recommended' => 'boolean NOT NULL DEFAULT FALSE',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{tariff_tariff}}_create_time", '{{tariff_tariff}}', "create_time", false);
        $this->createIndex("ix_{{tariff_tariff}}_update_time", '{{tariff_tariff}}', "update_time", false);
        $this->createIndex("ix_{{tariff_tariff}}_status", '{{tariff_tariff}}', "status", false);
        $this->createIndex("ix_{{tariff_tariff}}_sort", '{{tariff_tariff}}', "sort", false);
        $this->createIndex("ix_{{tariff_tariff}}_type", '{{tariff_tariff}}', "type", false);
        $this->createIndex("ix_{{tariff_tariff}}_slug", '{{tariff_tariff}}', "slug", true);

        $this->createTable(
            '{{tariff_property}}',
            [
                'id' => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'tariff_id' => 'integer NOT NULL',
                'title' => 'string NOT NULL',
                'description' => 'text',
                'sort' => 'integer NOT NULL DEFAULT 1',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{tariff_property}}_create_time", '{{tariff_property}}', "create_time", false);
        $this->createIndex("ix_{{tariff_property}}_update_time", '{{tariff_property}}', "update_time", false);
        $this->createIndex("ix_{{tariff_property}}_tariff_id", '{{tariff_property}}', "tariff_id", false);
        $this->createIndex("ix_{{tariff_property}}_sort", '{{tariff_property}}', "sort", false);

        //fk
        $this->addForeignKey('fk_{{tariff_property}}_tariff_id', '{{tariff_property}}', 'tariff_id', '{{tariff_tariff}}', 'id', 'CASCADE');
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{tariff_property}}');
        $this->dropTableWithForeignKeys('{{tariff_tariff}}');
    }
}
