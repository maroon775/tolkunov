<?php

/**
 * PortfolioController контроллер для portfolio на публичной части сайта
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2016 amyLabs && Yupe! team
 * @package yupe.modules.portfolio.controllers
 * @since 0.1
 *
 */
class PortfolioController extends \yupe\components\controllers\FrontController
{
    public function behaviors()
    {
        return [
            'seo' => ['class' => 'vendor.chemezov.yii-seo.behaviors.SeoBehavior'],
        ];
    }

    public function actionIndex()
    {
        $model = new Portfolio('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Portfolio'])) {
            $model->attributes = $_GET['Portfolio'];
        }
        $model->status = Portfolio::STATUS_PUBLISHED;

        //send model object for search
        $dataProvider = $model->search();
        $dataProvider->pagination = [
            'class' => 'Pagination',
            'pageSize' => 8 * 6,
        ];

        $baseCriteria = new CDbCriteria();
        $baseCriteria->compare('t.status', Portfolio::STATUS_PUBLISHED);

        $this->render('index', [
                'dataProvider' => $dataProvider,
                'model' => $model,
                'baseCriteria' => $baseCriteria,
            ]
        );
    }

    public function actionFavorite()
    {
        $model = new Portfolio('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Portfolio'])) {
            $model->attributes = $_GET['Portfolio'];
        }
        $model->status = Portfolio::STATUS_PUBLISHED;
        $model->is_loved = true;

        //send model object for search
        $dataProvider = $model->search();
        $dataProvider->pagination = [
            'class' => 'Pagination',
            'pageSize' => 8 * 6,
        ];

        $baseCriteria = new CDbCriteria();
        $baseCriteria->compare('t.status', Portfolio::STATUS_PUBLISHED);
        $baseCriteria->compare('t.is_loved', true);

        $this->render('favorite', [
                'dataProvider' => $dataProvider,
                'model' => $model,
                'baseCriteria' => $baseCriteria,
            ]
        );
    }

    public function actionThemes()
    {
        $model = PortfolioTheme::model()->find(['order' => 't.sort']);

        if ($model) {
            $this->redirect($model->url);
        } else {
            throw new CHttpException(404, 'Не найдено ни одной темы для портфолио');
        }
    }

    public function actionTheme($slug)
    {
        $theme = $this->loadTheme($slug);
        $themes = PortfolioTheme::model()->search()->getData();

        $model = new Portfolio('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Portfolio'])) {
            $model->attributes = $_GET['Portfolio'];
        }
        $model->status = Portfolio::STATUS_PUBLISHED;
        $model->theme_id = $theme->id;

        //send model object for search
        $dataProvider = $model->search();
        $dataProvider->pagination = [
            'class' => 'Pagination',
            'pageSize' => 8 * 6,
        ];

        $baseCriteria = new CDbCriteria();
        $baseCriteria->compare('t.status', Portfolio::STATUS_PUBLISHED);
        $baseCriteria->compare('t.theme_id', $theme->id);

        $this->render('theme', [
                'dataProvider' => $dataProvider,
                'model' => $model,
                'baseCriteria' => $baseCriteria,
                'currentTheme' => $theme,
                'themes' => $themes,
            ]
        );
    }

    public function actionStyles()
    {
        $model = PortfolioStyle::model()->find(['order' => 't.sort']);

        if ($model) {
            $this->redirect($model->url);
        } else {
            throw new CHttpException(404, 'Не найдено ни одного стиля для портфолио');
        }
    }

    public function actionStyle($slug)
    {
        $style = $this->loadStyle($slug);
        $styles = PortfolioStyle::model()->search()->getData();

        $model = new Portfolio('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Portfolio'])) {
            $model->attributes = $_GET['Portfolio'];
        }
        $model->status = Portfolio::STATUS_PUBLISHED;
        $model->style_id = $style->id;

        //send model object for search
        $dataProvider = $model->search();
        $dataProvider->pagination = [
            'class' => 'Pagination',
            'pageSize' => 8 * 6,
        ];

        $baseCriteria = new CDbCriteria();
        $baseCriteria->compare('t.status', Portfolio::STATUS_PUBLISHED);
        $baseCriteria->compare('t.style_id', $style->id);

        $this->render('style', [
                'dataProvider' => $dataProvider,
                'model' => $model,
                'baseCriteria' => $baseCriteria,
                'currentStyle' => $style,
                'styles' => $styles,
            ]
        );
    }

    public function actionSizes()
    {
        $sizes = $this->loadSizes();

        if (count($sizes)) {
            $size = reset($sizes);
            list($width, $height) = explode('x', $size);

            $this->redirect(['size', 'width' => $width, 'height' => $height]);
        } else {
            throw new CHttpException(404, 'Не найдено ни одного размера для портфолио');
        }
    }

    public function actionSize($width, $height)
    {        
        $sizes = $this->loadSizes();

        $criteria = new CDbCriteria();
        $criteria->select = 't.*';
        $criteria->join = 'INNER JOIN {{portfolio_item}} as i ON i.portfolio_id = t.id';
        $criteria->group = 't.id';
        $criteria->compare('i.width', $width);
        $criteria->compare('i.height', $height);

        $model = new Portfolio('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Portfolio'])) {
            $model->attributes = $_GET['Portfolio'];
        }
        $model->status = Portfolio::STATUS_PUBLISHED;

        //send model object for search
        $dataProvider = $model->search();
        $dataProvider->pagination = [
            'class' => 'Pagination',
            'pageSize' => 8 * 6,
        ];

        $dataProvider->getCriteria()->mergeWith($criteria);

        $baseCriteria = new CDbCriteria();
        $baseCriteria->compare('t.status', Portfolio::STATUS_PUBLISHED);
        $baseCriteria->mergeWith($criteria);

        $this->render('size', [
                'dataProvider' => $dataProvider,
                'model' => $model,
                'baseCriteria' => $baseCriteria,
                'width' => $width,
                'height' => $height,
                'sizes' => $sizes,
            ]
        );
    }

    public function actionView($slug)
    {
        $model = $this->loadModel($slug);

        preg_match_all('/{gallery_(\d+)}/', $model->text, $matches);

        if (count($matches[0]) > 0) {
            foreach ($matches[1] as $key => $gallery_id) {
                $model->text = str_replace($matches[0][$key], $this->renderEntity('Gallery', $gallery_id),
                    $model->text);
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('view', ['model' => $model]);
        } else {
            $this->render('view', ['model' => $model]);
        }
    }

    /**
     * @param string $slug
     * @return Portfolio
     * @throws CHttpException
     */
    protected function loadModel($slug)
    {
        $model = Portfolio::model()->published()->findByAttributes(['slug' => $slug]);

        if (!$model) {
            throw new CHttpException(404);
        }

        return $model;
    }

    /**
     * @param string $slug
     * @return PortfolioTheme
     * @throws CHttpException
     */
    protected function loadTheme($slug)
    {
        $model = PortfolioTheme::model()->findByAttributes(['slug' => $slug]);

        if (!$model) {
            throw new CHttpException(404);
        }

        return $model;
    }

    /**
     * @param string $slug
     * @return PortfolioStyle
     * @throws CHttpException
     */
    protected function loadStyle($slug)
    {
        $model = PortfolioStyle::model()->findByAttributes(['slug' => $slug]);

        if (!$model) {
            throw new CHttpException(404);
        }

        return $model;
    }

    protected function loadSizes()
    {
        $command = Yii::app()->db->createCommand();

        $command
            ->select(['CONCAT_WS("x",items.width,items.height) as size_string'])
            ->from('{{portfolio_item}} as items')
            ->group('size_string')
            ->order('COUNT(*) DESC');

        return $command->queryColumn();
    }

    public function renderEntity($modelClass, $id)
    {
        Yii::import('application.modules.' . strtolower($modelClass) . '.models.' . $modelClass, true);

        $model = CActiveRecord::model($modelClass)->findByPk($id);

        if (!$model) {
            return false;
        }

        return $this->renderPartial('_' . strtolower($modelClass), ['model' => $model], true);
    }
}
