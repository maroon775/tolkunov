<?php

/**
 * Класс PortfolioBackendController:
 *
 * @category Yupe\yupe\components\controllers\BackController
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
class PortfolioBackendController extends \yupe\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Portfolio.PortfolioBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Portfolio.PortfolioBackend.View']],
            ['allow', 'actions' => ['create', 'deleteItem'], 'roles' => ['Portfolio.PortfolioBackend.Create']],
            ['allow', 'actions' => ['update', 'inline', 'inlineItem', 'sortableItem', 'deleteItem'], 'roles' => ['Portfolio.PortfolioBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Portfolio.PortfolioBackend.Delete']],
            ['deny'],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'yupe\components\actions\YInLineEditAction',
                'model' => 'Portfolio',
                'validAttributes' => ['title', 'name', 'slug', 'status', 'type', 'client_id', 'theme_id', 'style_id', 'is_loved'],
            ],
            'inlineItem' => [
                'class' => 'yupe\components\actions\YInLineEditAction',
                'model' => 'PortfolioItem',
                'validAttributes' => ['language', 'width', 'height'],
            ],
            'sortableItem' => [
                'class' => 'yupe\components\actions\SortAction',
                'model' => 'PortfolioItem',
                'attribute' => 'order',
            ],
        ];
    }

    /**
     * Отображает портфолио по указанному идентификатору
     *
     * @param integer $id Идинтификатор портфолио для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Создает новую модель портфолио.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new Portfolio;

        if (Yii::app()->getRequest()->getPost('Portfolio') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Portfolio'));

            if ($model->save()) {

                $this->updatePortfolioItems($model);

                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('PortfolioModule.portfolio', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование портфолио.
     *
     * @param integer $id Идинтификатор портфолио для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('Portfolio') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Portfolio'));

            if ($model->save()) {

                $this->updatePortfolioItems($model);

                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('PortfolioModule.portfolio', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель портфолио из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор портфолио, который нужно удалить
     *
     * @return void
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('PortfolioModule.portfolio', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t('PortfolioModule.portfolio', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
        }
    }

    /**
     * Управление портфолио.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new Portfolio('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('Portfolio') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('Portfolio'));
        }
        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param $id integer идентификатор нужной модели
     *
     * @return Portfolio
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Portfolio::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('PortfolioModule.portfolio', 'Запрошенная страница не найдена.'));
        }

        return $model;
    }

    /**
     * @param Portfolio $portfolio
     */
    protected function updatePortfolioItems(Portfolio $portfolio)
    {
        if (Yii::app()->getRequest()->getPost('PortfolioItem')) {
            foreach (Yii::app()->getRequest()->getPost('PortfolioItem') as $key => $val) {
                if (in_array($key, ['width', 'height', 'language'])) {
                    continue;
                }

                $productImage = PortfolioItem::model()->findByPk($key);
                if (null === $productImage) {
                    $productImage = new PortfolioItem();
                    $productImage->portfolio_id = $portfolio->id;
                    if ($portfolio->type == Portfolio::TYPE_HTML5) {
                        $productImage->fileUpload->addFileInstanceName('PortfolioItem[' . $key . '][image]');
                    } else {
                        $productImage->imageUpload->addFileInstanceName('PortfolioItem[' . $key . '][image]');
                    }
                }
                $productImage->setAttributes($_POST['PortfolioItem'][$key]);
                if (false === $productImage->save()) {
                    Yii::app()->getUser()->setFlash(\yupe\widgets\YFlashMessages::WARNING_MESSAGE,
                        Yii::t('PortfolioModule.portfolio', 'Произошла ошибка при загрузке ресайзов...'));

                    Yii::app()->getUser()->setFlash(\yupe\widgets\YFlashMessages::ERROR_MESSAGE, (new CActiveForm())->errorSummary($productImage));
                }
            }
        }
    }

    public function actionDeleteItem($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $model = PortfolioItem::model()->findByPk($id);

            $model->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('PortfolioModule.portfolio', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t('PortfolioModule.portfolio', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
        }
    }
}
