<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Portfolio
 * @var $this PortfolioBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('PortfolioModule.portfolio', 'Портфолио') => ['/portfolio/portfolioBackend/index'],
    Yii::t('PortfolioModule.portfolio', 'Управление'),
];

$this->pageTitle = Yii::t('PortfolioModule.portfolio', 'Портфолио - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('PortfolioModule.portfolio', 'Управление портфолио'), 'url' => ['/portfolio/portfolioBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('PortfolioModule.portfolio', 'Добавить портфолио'), 'url' => ['/portfolio/portfolioBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('PortfolioModule.portfolio', 'Портфолио'); ?>
        <small><?= Yii::t('PortfolioModule.portfolio', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?= Yii::t('PortfolioModule.portfolio', 'Поиск портфолио'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('portfolio-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<br/>

<p> <?= Yii::t('PortfolioModule.portfolio', 'В данном разделе представлены средства управления портфолио'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id' => 'portfolio-grid',
        'type' => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'id',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->id, ["update", "id" => $data->id]);
                },
                'htmlOptions' => ['width' => '80px'],
            ],
            [
                'type' => 'raw',
                'name' => 'image',
                'filter' => false,
                'value' => function (Portfolio $data) {
                    return CHtml::image($data->getImageUrl(80, 80), CHtml::encode($data->title), ["width" => 80, "height" => 80, "class" => "img-thumbnail"]);
                },
            ],
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'type',
                'url' => $this->createUrl('inline'),
                'source' => $model->getTypeList(),
                'editable' => [
                    'emptytext' => '---',
                ],
                'options' => [
                    $model::TYPE_HTML5 => ['class' => 'label-primary'],
                    $model::TYPE_GIF => ['class' => 'label-info'],
                    $model::TYPE_JPG => ['class' => 'label-success'],
                ],
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'title',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
            ],
//            'create_time',
//            'update_time',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'client_id',
                'url' => $this->createUrl('inline'),
                'source' => CHtml::listData(Client::model()->findAll(), 'id', 'title'),
                'editable' => [
                    'emptytext' => '---',
                ],
            ],
            'date',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    $model::STATUS_PUBLISHED => ['class' => 'label-success'],
                    $model::STATUS_MODERATION => ['class' => 'label-warning'],
                    $model::STATUS_DRAFT => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'theme_id',
                'url' => $this->createUrl('inline'),
                'source' => CHtml::listData(PortfolioTheme::model()->findAll(), 'id', 'title'),
                'editable' => [
                    'emptytext' => '---',
                ],
            ],
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'style_id',
                'url' => $this->createUrl('inline'),
                'source' => CHtml::listData(PortfolioStyle::model()->findAll(), 'id', 'title'),
                'editable' => [
                    'emptytext' => '---',
                ],
            ],
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'is_loved',
                'url' => $this->createUrl('inline'),
                'source' => $model->getYesNoList(),
                'options' => [
                    ['class' => 'label-default'],
                    ['class' => 'label-success'],
                ],
            ],
//            'slug',
//            'text',
//            'seo_title',
//            'seo_keywords',
//            'seo_description',
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'frontViewButtonUrl' => function ($data) {
                    return $data->url;
                },
                'buttons' => [
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->status == Portfolio::STATUS_PUBLISHED;
                        },
                    ],
                ],
            ],
        ],
    ]
); ?>
