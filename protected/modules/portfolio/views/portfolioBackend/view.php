<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model Portfolio
 *   @var $this PortfolioBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('PortfolioModule.portfolio', 'Портфолио') => ['/portfolio/portfolioBackend/index'],
    $model->title,
];

$this->pageTitle = Yii::t('PortfolioModule.portfolio', 'Портфолио - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('PortfolioModule.portfolio', 'Управление портфолио'), 'url' => ['/portfolio/portfolioBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('PortfolioModule.portfolio', 'Добавить портфолио'), 'url' => ['/portfolio/portfolioBackend/create']],
    ['label' => Yii::t('PortfolioModule.portfolio', 'Портфолио') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('PortfolioModule.portfolio', 'Редактирование портфолио'), 'url' => [
        '/portfolio/portfolioBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('PortfolioModule.portfolio', 'Просмотреть портфолио'), 'url' => [
        '/portfolio/portfolioBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('PortfolioModule.portfolio', 'Удалить портфолио'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/portfolio/portfolioBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('PortfolioModule.portfolio', 'Вы уверены, что хотите удалить портфолио?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('PortfolioModule.portfolio', 'Просмотр') . ' ' . Yii::t('PortfolioModule.portfolio', 'портфолио'); ?>        <br/>
        <small>&laquo;<?=  $model->title; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'create_time',
        'update_time',
        'type',
        'date',
        'client_id',
        'title',
        'image',
        'theme_id',
        'style_id',
        'is_loved',
        'status',
        'slug',
        'text',
        'seo_title',
        'seo_keywords',
        'seo_description',
    ],
]); ?>
