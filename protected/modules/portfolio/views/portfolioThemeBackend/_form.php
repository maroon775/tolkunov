<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model PortfolioTheme
 * @var $form yupe\widgets\ActiveForm
 * @var $this PortfolioThemeBackendController
 **/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm', [
        'id' => 'portfolio-theme-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well'],
    ]
);
?>

    <div class="alert alert-info">
        <?= Yii::t('PortfolioModule.portfolio', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?= Yii::t('PortfolioModule.portfolio', 'обязательны.'); ?>
    </div>

<?= $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->textFieldGroup($model, 'title'); ?>
        </div>
        <div class="col-sm-6">
            <?php echo $form->slugFieldGroup(
                $model,
                'slug',
                [
                    'sourceAttribute' => 'title',
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'class' => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('slug'),
                            'data-content' => $model->getAttributeDescription('slug'),
                            'placeholder' => 'Для автоматической генерации оставьте поле пустым',
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
            <div class="panel-group" id="extended-options">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <a data-toggle="collapse" data-parent="#extended-options" href="#collapseTwo">
                                <?php echo Yii::t('PortfolioModule.portfolio', 'Data for SEO'); ?>
                            </a>
                        </div>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?= $form->textFieldGroup($model, 'seo_title'); ?>
                            <?= $form->textFieldGroup($model, 'seo_keywords'); ?>
                            <?= $form->textAreaGroup($model, 'seo_description', ['widgetOptions' => ['htmlOptions' => ['rows' => 8]]]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => Yii::t('PortfolioModule.portfolio', 'Сохранить сферу деятельности и продолжить'),
    ]
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label' => Yii::t('PortfolioModule.portfolio', 'Сохранить сферу деятельности и закрыть'),
    ]
); ?>

<?php $this->endWidget(); ?>