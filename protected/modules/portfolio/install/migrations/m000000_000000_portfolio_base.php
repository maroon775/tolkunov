<?php

/**
 * Portfolio install migration
 * Класс миграций для модуля Portfolio:
 *
 * @category YupeMigration
 * @package  yupe.modules.portfolio.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000000_portfolio_base extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        /* Portfolio Theme */
        $this->createTable(
            '{{portfolio_theme}}',
            [
                'id' => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'title' => 'string NOT NULL',
                'slug' => 'varchar(150) NOT NULL',
                'sort' => 'integer NOT NULL DEFAULT 1',
                'seo_title' => 'string',
                'seo_keywords' => 'string',
                'seo_description' => 'string',
            ],
            $this->getOptions()
        );

        // ix
        $this->createIndex("ix_{{portfolio_theme}}_slug", '{{portfolio_theme}}', "slug", true);
        $this->createIndex("ix_{{portfolio_theme}}_sort", '{{portfolio_theme}}', "sort", false);

        /* Portfolio Style */
        $this->createTable(
            '{{portfolio_style}}',
            [
                'id' => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'title' => 'string NOT NULL',
                'title_h1' => 'string',
                'slug' => 'varchar(150) NOT NULL',
                'sort' => 'integer NOT NULL DEFAULT 1',
                'seo_title' => 'string',
                'seo_keywords' => 'string',
                'seo_description' => 'string',
            ],
            $this->getOptions()
        );

        // ix
        $this->createIndex("ix_{{portfolio_style}}_slug", '{{portfolio_style}}', "slug", true);
        $this->createIndex("ix_{{portfolio_style}}_sort", '{{portfolio_style}}', "sort", false);

        /* Portfolio */
        $this->createTable(
            '{{portfolio_portfolio}}',
            [
                'id' => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'type' => 'tinyint(1) NOT NULL',
                'date' => 'DATE NOT NULL',
                'client_id' => 'integer NOT NULL',
                'title' => 'string NOT NULL',
                'image' => 'string',
                'theme_id' => 'integer DEFAULT NULL',
                'style_id' => 'integer DEFAULT NULL',
                'is_loved' => 'boolean NOT NULL DEFAULT FALSE',
                'status' => 'tinyint(1) NOT NULL DEFAULT 0',
                'slug' => 'varchar(150) NOT NULL',
                'text' => 'text',
                'seo_title' => 'string',
                'seo_keywords' => 'string',
                'seo_description' => 'string',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{portfolio_portfolio}}_status", '{{portfolio_portfolio}}', "status", false);
        $this->createIndex("ix_{{portfolio_portfolio}}_date", '{{portfolio_portfolio}}', "date", false);
        $this->createIndex("ix_{{portfolio_portfolio}}_type", '{{portfolio_portfolio}}', "type", false);
        $this->createIndex("ix_{{portfolio_portfolio}}_slug", '{{portfolio_portfolio}}', "slug", true);
        $this->createIndex("ix_{{portfolio_portfolio}}_is_loved", '{{portfolio_portfolio}}', "is_loved", false);

        //fk
        $this->addForeignKey('fk_{{portfolio_portfolio}}_client_id', '{{portfolio_portfolio}}', 'client_id', '{{client_client}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_{{portfolio_portfolio}}_theme_id', '{{portfolio_portfolio}}', 'theme_id', '{{portfolio_theme}}', 'id', 'SET NULL');
        $this->addForeignKey('fk_{{portfolio_portfolio}}_style_id', '{{portfolio_portfolio}}', 'style_id', '{{portfolio_style}}', 'id', 'SET NULL');

        /* Portfolio Item */
        $this->createTable(
            '{{portfolio_item}}',
            [
                'id' => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'portfolio_id' => 'integer NOT NULL',
                'file' => 'string',
                'image' => 'string',
                'width' => 'integer NOT NULL',
                'height' => 'integer NOT NULL',
                'language' => 'tinyint(3) NOT NULL DEFAULT 1',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{portfolio_item}}_width", '{{portfolio_item}}', 'width', false);
        $this->createIndex("ix_{{portfolio_item}}_height", '{{portfolio_item}}', 'height', false);

        //fk
        $this->addForeignKey('fk_{{portfolio_item}}_portfolio_id', '{{portfolio_item}}', 'portfolio_id', '{{portfolio_portfolio}}', 'id', 'CASCADE');
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{portfolio_item}}');
        $this->dropTableWithForeignKeys('{{portfolio_portfolio}}');
        $this->dropTableWithForeignKeys('{{portfolio_style}}');
        $this->dropTableWithForeignKeys('{{portfolio_theme}}');
    }
}
