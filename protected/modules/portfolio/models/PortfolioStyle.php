<?php

/**
 * This is the model class for table "{{portfolio_style}}".
 *
 * The followings are the available columns in table '{{portfolio_style}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $title
 * @property string $title_h1
 * @property string $slug
 * @property integer $sort
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 *
 * The followings are the available model relations:
 * @property Portfolio[] $portfolios
 */
class PortfolioStyle extends yupe\models\YModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{portfolio_style}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['title, title_h1, slug, seo_title, seo_description, seo_keywords', 'filter', 'filter' => 'trim'],
            ['title, title_h1, slug, seo_title, seo_description, seo_keywords', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],
            ['title, slug', 'required'],
            ['sort', 'numerical', 'integerOnly' => true],
            ['title, title_h1, seo_title, seo_keywords, seo_description', 'length', 'max' => 255],
            ['slug', 'length', 'max' => 150],
            ['slug', 'unique'],
            [
                'slug',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('PortfolioModule.portfolio', 'Bad characters in {attribute} field'),
            ],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, title, title_h1, slug, sort, seo_title, seo_keywords, seo_description', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('portfolio');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
            'seo' => [
                'class' => 'vendor.chemezov.yii-seo.behaviors.SeoActiveRecordBehavior',
                'route' => '/portfolio/portfolio/style',
                'params' => [
                    'slug' => function (PortfolioStyle $data) {
                        return $data->slug;
                    },
                ],
            ],
            'sortable' => [
                'class' => 'yupe\components\behaviors\SortableBehavior',
                'attributeName' => 'sort',
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'portfolios' => [self::HAS_MANY, 'Portfolio', 'style_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата обновления',
            'title' => 'Название',
            'title_h1' => 'Заголовок перед списком',
            'slug' => 'URL',
            'sort' => 'Сортировка',
            'seo_title' => 'SEO Title',
            'seo_keywords' => 'SEO Keywords',
            'seo_description' => 'SEO Description',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.title', $this->title, true);
        $criteria->compare($this->tableAlias . '.title_h1', $this->title_h1, true);
        $criteria->compare($this->tableAlias . '.slug', $this->slug, true);
        $criteria->compare($this->tableAlias . '.sort', $this->sort);
        $criteria->compare($this->tableAlias . '.seo_title', $this->seo_title, true);
        $criteria->compare($this->tableAlias . '.seo_keywords', $this->seo_keywords, true);
        $criteria->compare($this->tableAlias . '.seo_description', $this->seo_description, true);

        return new CActiveDataProvider(
            get_class($this), [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => $this->tableAlias . '.sort'],
            ]
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PortfolioStyle the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
