<?php

/**
 * This is the model class for table "{{portfolio_portfolio}}".
 *
 * The followings are the available columns in table '{{portfolio_portfolio}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property integer $type
 * @property string $date
 * @property integer $client_id
 * @property string $title
 * @property string $image
 * @property string $pattern
 * @property integer $theme_id
 * @property integer $style_id
 * @property integer $is_loved
 * @property integer $status
 * @property string $slug
 * @property string $text
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 *
 * The followings are the available model relations:
 * @property PortfolioItem[] $items
 * @property PortfolioStyle $style
 * @property Client $client
 * @property PortfolioTheme $theme
 *
 * @method Portfolio published()
 */
class Portfolio extends yupe\models\YModel
{
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;
    /**
     *
     */
    const STATUS_MODERATION = 2;

    const TYPE_HTML5 = 1;
    const TYPE_GIF = 2;
    const TYPE_JPG = 3;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{portfolio_portfolio}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['title, text, seo_title, seo_description, seo_keywords', 'filter', 'filter' => 'trim'],
            ['title, seo_title, seo_description, seo_keywords', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],
            ['type, date, client_id, title, slug', 'required'],
            ['type, client_id, theme_id, style_id, status', 'numerical', 'integerOnly' => true],
            ['is_loved', 'boolean'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['type', 'in', 'range' => array_keys($this->getTypeList())],
            ['client_id', 'exist', 'className' => 'Client', 'attributeName' => 'id'],
            ['theme_id', 'exist', 'className' => 'PortfolioTheme', 'attributeName' => 'id'],
            ['style_id', 'exist', 'className' => 'PortfolioStyle', 'attributeName' => 'id'],
            ['title, seo_title, seo_keywords, seo_description', 'length', 'max' => 255],
            ['slug', 'length', 'max' => 150],
            ['slug', 'unique'],
            [
                'slug',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('PortfolioModule.portfolio', 'Bad characters in {attribute} field'),
            ],
            ['text', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, type, date, client_id, title, image, pattern, theme_id, style_id, is_loved, status, slug, text, seo_title, seo_keywords, seo_description', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => $this->tableAlias . '.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
        ];
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function type($id)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->tableAlias . '.type = :type',
            'params' => [':type' => $id],
        ]);

        return $this;
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function theme($id)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->tableAlias . '.theme_id = :theme_id',
            'params' => [':theme_id' => $id],
        ]);

        return $this;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('portfolio');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
            'seo' => [
                'class' => 'vendor.chemezov.yii-seo.behaviors.SeoActiveRecordBehavior',
                'route' => '/portfolio/portfolio/view',
                'params' => [
                    'slug' => function (Portfolio $data) {
                        return $data->slug;
                    },
                ],
            ],
            'imageUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
                'resizeOnUpload' => false,
//                'defaultImage' => $module->defaultImage,
            ],
            'patternUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'pattern',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
                'resizeOnUpload' => false,
                'deleteFileKey' => 'delete-file-2',
//                'defaultImage' => $module->defaultImage,
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'items' => [self::HAS_MANY, 'PortfolioItem', 'portfolio_id'],
            'style' => [self::BELONGS_TO, 'PortfolioStyle', 'style_id'],
            'client' => [self::BELONGS_TO, 'Client', 'client_id'],
            'theme' => [self::BELONGS_TO, 'PortfolioTheme', 'theme_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата обновления',
            'type' => 'Тип',
            'date' => 'Дата',
            'client_id' => 'Клиент',
            'title' => 'Заголовок',
            'slug' => 'URL',
            'image' => 'Изображение',
            'pattern' => 'Паттерн',
            'theme_id' => 'Сфера деятельности',
            'style_id' => 'Стиль',
            'is_loved' => 'Любимый проект',
            'status' => 'Статус',
            'text' => 'Текст',
            'seo_title' => 'SEO Title',
            'seo_keywords' => 'SEO Keywords',
            'seo_description' => 'SEO Description',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.type', $this->type);
        $criteria->compare($this->tableAlias . '.date', $this->date, true);
        $criteria->compare($this->tableAlias . '.client_id', $this->client_id);
        $criteria->compare($this->tableAlias . '.title', $this->title, true);
        $criteria->compare($this->tableAlias . '.image', $this->image, true);
        $criteria->compare($this->tableAlias . '.pattern', $this->pattern, true);
        $criteria->compare($this->tableAlias . '.theme_id', $this->theme_id);
        $criteria->compare($this->tableAlias . '.style_id', $this->style_id);
        $criteria->compare($this->tableAlias . '.is_loved', $this->is_loved);
        $criteria->compare($this->tableAlias . '.status', $this->status);
        $criteria->compare($this->tableAlias . '.slug', $this->slug, true);
        $criteria->compare($this->tableAlias . '.text', $this->text, true);
        $criteria->compare($this->tableAlias . '.seo_title', $this->seo_title, true);
        $criteria->compare($this->tableAlias . '.seo_keywords', $this->seo_keywords, true);
        $criteria->compare($this->tableAlias . '.seo_description', $this->seo_description, true);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'sort' => ['defaultOrder' => $this->tableAlias . '.date DESC'],
        ]);
    }

    /**
     * @param $id
     * @return CActiveDataProvider
     */
    public function searchNotFor($id)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.type', $this->type);
        $criteria->compare($this->tableAlias . '.date', $this->date, true);
        $criteria->compare($this->tableAlias . '.client_id', $this->client_id);
        $criteria->compare($this->tableAlias . '.title', $this->title, true);
        $criteria->compare($this->tableAlias . '.image', $this->image, true);
        $criteria->compare($this->tableAlias . '.pattern', $this->pattern, true);
        $criteria->compare($this->tableAlias . '.theme_id', $this->theme_id);
        $criteria->compare($this->tableAlias . '.style_id', $this->style_id);
        $criteria->compare($this->tableAlias . '.is_loved', $this->is_loved);
        $criteria->compare($this->tableAlias . '.status', $this->status);
        $criteria->compare($this->tableAlias . '.slug', $this->slug, true);
        $criteria->compare($this->tableAlias . '.text', $this->text, true);
        $criteria->compare($this->tableAlias . '.seo_title', $this->seo_title, true);
        $criteria->compare($this->tableAlias . '.seo_keywords', $this->seo_keywords, true);
        $criteria->compare($this->tableAlias . '.seo_description', $this->seo_description, true);
        $criteria->addNotInCondition($this->tableAlias . '.id', [$id]);

        $criteria->limit = 4;

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => false,
            'sort' => ['defaultOrder' => $this->tableAlias . '.date DESC'],
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Portfolio the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('PortfolioModule.portfolio', 'Draft'),
            self::STATUS_MODERATION => Yii::t('PortfolioModule.portfolio', 'On moderation'),
            self::STATUS_PUBLISHED => Yii::t('PortfolioModule.portfolio', 'Published'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('PortfolioModule.portfolio', '*unknown*');
    }

    /**
     * @return array
     */
    public function getTypeList()
    {
        return [
            self::TYPE_HTML5 => Yii::t('PortfolioModule.portfolio', 'HTML5'),
            self::TYPE_GIF => Yii::t('PortfolioModule.portfolio', 'GIF'),
            self::TYPE_JPG => Yii::t('PortfolioModule.portfolio', 'JPG'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getType()
    {
        $data = $this->getTypeList();

        return isset($data[$this->type]) ? $data[$this->type] : Yii::t('PortfolioModule.portfolio', '*unknown*');
    }

    public function getTypeStringList()
    {
        return [
            self::TYPE_HTML5 => Yii::t('PortfolioModule.portfolio', 'html5'),
            self::TYPE_GIF => Yii::t('PortfolioModule.portfolio', 'gif'),
            self::TYPE_JPG => Yii::t('PortfolioModule.portfolio', 'jpg'),
        ];
    }

    public function getTypeString()
    {
        $data = $this->getTypeStringList();

        return isset($data[$this->type]) ? $data[$this->type] : null;
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!$this->slug) {
            $slugParts = [
                $this->getTypeString(),
                'banner-dlya',
                $this->client->slug,
                yupe\helpers\YText::translit($this->title),
            ];

            $this->slug = implode('-', $slugParts);
        }

        return parent::beforeValidate();
    }

    /**
     * @return array
     */
    public function getYesNoList()
    {
        return [
            Yii::t('PortfolioModule.portfolio', 'No'),
            Yii::t('PortfolioModule.portfolio', 'Yes'),
        ];
    }

    public function getLanguages()
    {
        $languages = [];

        foreach ($this->items as $item) {
            $languages[$item->language] = $item->getLanguage();
        }

        ksort($languages, SORT_NUMERIC);

        $languagesAssoc = [];

        foreach ($languages as $key => $value) {
            $languagesAssoc[PortfolioItem::model()->getLanguageStringList()[$key]] = $value;
        }

        return $languagesAssoc;
    }

    public function getUrlHashForSize($width, $height)
    {
        foreach ($this->items as $item) {
            if ($item->width == $width && $item->height == $height) {
                return $item->getId();
            }
        }

        return null;
    }

    public function getTitle()
    {
        return $this->getType() . '-баннер для ' . $this->client->title . ' «' . $this->title . '»';
    }
}
