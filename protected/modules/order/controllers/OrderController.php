<?php

/**
 * OrderController контроллер для order на публичной части сайта
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2017 amyLabs && Yupe! team
 * @package yupe.modules.order.controllers
 * @since 0.1
 *
 */
class OrderController extends \yupe\components\controllers\FrontController
{
    public function behaviors()
    {
        return [
            'seo' => ['class' => 'vendor.chemezov.yii-seo.behaviors.SeoBehavior'],
        ];
    }

    public function filters()
    {
        return [
//            ['vendor.chemezov.yii-seo.filters.SeoFilter + view'], // apply the filter to the view-action
//            array(
//                'COutputCache + view',
//                'duration'    => 100,
//                'varyByParam' => array('id'),
//            ),
        ];
    }

    public function actionCreate()
    {
        $model = Order::getOrderByUser(Yii::app()->user->getUid(), false);

        if (!$model || ($model->scenario = 'step_1') && !$model->validate()) {
            /* Step 1 */
            $model = Order::getOrderByUser(Yii::app()->user->getUid(), true);
            $model->scenario = 'step_1';

            if (\Yii::app()->request->isPostRequest) {
                if (isset($_POST['Order'])) {
                    $model->attributes = $_POST['Order'];

                    if ($model->save()) {
                        $model->scenario = 'step_2';

//                        $this->redirect(['/order/order/createStep2']);
                    }
                }
            }

//            $this->redirect(['/order/order/createStep1']);
        } else {
            $model->scenario = 'step_2';

            $fileModels = [];

            if (\Yii::app()->request->isPostRequest) {
                if (isset($_POST['Order'])) {
                    $validate = true;

                    $model->attributes = $_POST['Order'];

                    $files = CUploadedFile::getInstancesByName('Order[files]');

                    if (isset($files) && count($files) > 0) {
                        // go through each uploaded image
                        foreach ($files as $file) {
                            $fileModel = new OrderFile();
                            $fileModel->order_id = 1;
                            $fileModel->fileUpload->addFileInstance($file);

                            if (!$fileModel->validate()) {
                                $validate = false;
                            }

                            $fileModels[] = $fileModel;
                        }
                    }

                    if ($model->validate() && $validate) {
                        $model->user_status = Order::USER_STATUS_CLOSED;

                        if (!$model->save()) {
                            Yii::app()->user->setFlash(
                                yupe\widgets\YFlashMessages::ERROR_MESSAGE,
                                Yii::t('OrderModule.order', 'Не удалось сохранить заказ!')
                            );
                        } else {
                            foreach ($fileModels as $fileModel) {
                                $fileModel->order_id = $model->id;

                                if (!$fileModel->save()) {
                                    Yii::app()->user->setFlash(
                                        yupe\widgets\YFlashMessages::ERROR_MESSAGE,
                                        Yii::t('OrderModule.order', 'Не удалось сохранить файл из заказа!')
                                    );
                                }
                            }
                        }

                        $filesString = '';

                        foreach ($fileModels as $fileModel) {
                            $filesString .= CHtml::link($fileModel->file, $fileModel->getFileUrl()) . '<br>';
                        }

                        /* Send Email */
                        $mail = Yii::app()->mailMessage;

                        $result = $mail->raiseMailEvent('NEW_ORDER', [
                            '{user_email}' => $model->email,
                            '{user_name}' => $model->name,
                            '{user_phone}' => $model->phone,
                            '{files}' => $filesString,
                            '{site_name}' => Yii::app()->getModule('yupe')->siteName,
                            '{admin_email}' => Yii::app()->getModule('yupe')->email,
                            '{order_id}' => $model->id,
                            '{order_text_1}' => $model->text_1,
                            '{order_text_2}' => $model->text_2,
                            '{order_text_3}' => $model->text_3,
                            '{order_text_4}' => $model->text_4,
                            '{order_text_5}' => $model->text_5,
                            '{order_text_6}' => $model->text_6,
                            '{order_tariff}' => $model->tariff,
                            '{order_portfolio}' => $model->portfolio ? $model->portfolio->getAbsoluteUrl() : '',
                        ]);

                        /* End send email */

//                    Yii::app()->user->setFlash(
//                        yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
//                        Yii::t('OrderModule.order', 'Запись добавлена!')
//                    );

                        $this->renderPartial('done', ['id' => $model->id]);

                        echo '<script>history.pushState(null, null, "' . $this->createUrl('done', ['id' => $model->id]) . '");</script>';
//                        echo '<script>location.href = "' . $this->createUrl('done', ['id' => $model->id]) . '";</script>';
                        Yii::app()->end();
//                    $this->redirect(['done', 'id' => $model->id]);
                    }
                }

//            Yii::app()->clientScript->registerScript('success', 'var needReload = true; setTimeout($.fancybox.close, 1000);', CClientScript::POS_END);
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial($model->scenario == 'step_1' ? '_form_1' : '_form_2', ['model' => $model, 'fileModels' => !empty($fileModels) ? $fileModels : []]);
        } else {
            $this->render('create', ['model' => $model, 'fileModels' => !empty($fileModels) ? $fileModels : []]);
        }
    }

    public function actionDone($id)
    {
        $this->render('done', ['id' => $id]);
    }

    public function actionTariff($tariff)
    {
        $model = Order::getOrderByUser(Yii::app()->user->getUid(), true);

        $model->tariff = $tariff;

        if ($model->save()) {
            $this->redirect(['/order/order/create']);
        } else {
            throw new CHttpException(500, 'Не удалось сохранить заказ!');
        }
    }

    public function actionPortfolio($id)
    {
        $model = Order::getOrderByUser(Yii::app()->user->getUid(), true);

        $model->portfolio_id = $id;

        if ($model->save()) {
            $this->redirect(['/order/order/create']);
        } else {
            throw new CHttpException(500, 'Не удалось сохранить заказ!');
        }
    }
}
