<?php
/**
 * Отображение для view:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Order
 * @var $this OrderBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderModule.order', 'Заказы') => ['/order/orderBackend/index'],
    $model->name,
];

$this->pageTitle = Yii::t('OrderModule.order', 'Заказы - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderModule.order', 'Управление заказами'), 'url' => ['/order/orderBackend/index']],
    ['label' => Yii::t('OrderModule.order', 'Заказ') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    [
        'icon' => 'fa fa-fw fa-eye',
        'label' => Yii::t('OrderModule.order', 'Просмотреть заказ'),
        'url' => [
            '/order/orderBackend/view',
            'id' => $model->id,
        ],
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
        'label' => Yii::t('OrderModule.order', 'Удалить заказ'),
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/order/orderBackend/delete', 'id' => $model->id],
            'confirm' => Yii::t('OrderModule.order', 'Вы уверены, что хотите удалить заказ?'),
            'csrf' => true,
        ],
    ],
];
?>
    <div class="page-header">
        <h1>
            <?= Yii::t('OrderModule.order', 'Просмотр') . ' ' . Yii::t('OrderModule.order', 'заказа'); ?> <br/>
            <small>&laquo;<?= $model->name; ?>&raquo;</small>
        </h1>
    </div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        'id',
        'create_time',
        'update_time',
        [
            'name' => 'status',
            'value' => $model->getStatus(),
        ],
//        'user',
        'name',
        'email',
        'phone',
        'text_1',
        'text_2',
        'text_3',
        'text_4',
        'text_5',
        'text_6',
        'tariff',
        [
            'name' => 'portfolio_id',
            'type' => 'raw',
            'value' => $model->portfolio ? CHtml::link($model->portfolio->getTitle(), $model->portfolio->getAbsoluteUrl()) : null,
        ],
    ],
]); ?>

    <h3>Файлы</h3>

<?php foreach ($model->files as $file): ?>
    <?= CHtml::link($file->file, $file->getFileUrl()); ?><br>
<?php endforeach; ?>