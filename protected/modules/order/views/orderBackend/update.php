<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model Order
 *   @var $this OrderBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderModule.order', 'Заказы') => ['/order/orderBackend/index'],
    $model->name => ['/order/orderBackend/view', 'id' => $model->id],
    Yii::t('OrderModule.order', 'Редактирование'),
];

$this->pageTitle = Yii::t('OrderModule.order', 'Заказы - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderModule.order', 'Управление заказами'), 'url' => ['/order/orderBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderModule.order', 'Добавить заказ'), 'url' => ['/order/orderBackend/create']],
    ['label' => Yii::t('OrderModule.order', 'Заказ') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('OrderModule.order', 'Редактирование заказа'), 'url' => [
        '/order/orderBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('OrderModule.order', 'Просмотреть заказ'), 'url' => [
        '/order/orderBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('OrderModule.order', 'Удалить заказ'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/order/orderBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('OrderModule.order', 'Вы уверены, что хотите удалить заказ?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderModule.order', 'Редактирование') . ' ' . Yii::t('OrderModule.order', 'заказа'); ?>        <br/>
        <small>&laquo;<?=  $model->name; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>