<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model Order
 *   @var $form yupe\widgets\ActiveForm
 *   @var $this OrderBackendController
 **/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm', [
        'id'                     => 'order-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'htmlOptions'            => ['class' => 'well'],
    ]
);
?>

<div class="alert alert-info">
    <?=  Yii::t('OrderModule.order', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?=  Yii::t('OrderModule.order', 'обязательны.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-12">
            <?=  $form->dateTimePickerGroup($model,'create_time', [
            'widgetOptions' => [
                'options' => [],
                'htmlOptions'=>[]
            ],
            'prepend'=>'<i class="fa fa-calendar"></i>'
        ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->dateTimePickerGroup($model,'update_time', [
            'widgetOptions' => [
                'options' => [],
                'htmlOptions'=>[]
            ],
            'prepend'=>'<i class="fa fa-calendar"></i>'
        ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->textFieldGroup($model, 'status', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('status'),
                        'data-content' => $model->getAttributeDescription('status')
                    ]
                ]
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->textFieldGroup($model, 'user', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('user'),
                        'data-content' => $model->getAttributeDescription('user')
                    ]
                ]
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->textFieldGroup($model, 'name', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('name'),
                        'data-content' => $model->getAttributeDescription('name')
                    ]
                ]
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->textFieldGroup($model, 'email', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('email'),
                        'data-content' => $model->getAttributeDescription('email')
                    ]
                ]
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->textFieldGroup($model, 'phone', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('phone'),
                        'data-content' => $model->getAttributeDescription('phone')
                    ]
                ]
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->textAreaGroup($model, 'text_1', [
            'widgetOptions' => [
                'htmlOptions' => [
                    'class' => 'popover-help',
                    'rows' => 6,
                    'cols' => 50,
                    'data-original-title' => $model->getAttributeLabel('text_1'),
                    'data-content' => $model->getAttributeDescription('text_1')
                ]
            ]]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->textAreaGroup($model, 'text_2', [
            'widgetOptions' => [
                'htmlOptions' => [
                    'class' => 'popover-help',
                    'rows' => 6,
                    'cols' => 50,
                    'data-original-title' => $model->getAttributeLabel('text_2'),
                    'data-content' => $model->getAttributeDescription('text_2')
                ]
            ]]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->textAreaGroup($model, 'text_3', [
            'widgetOptions' => [
                'htmlOptions' => [
                    'class' => 'popover-help',
                    'rows' => 6,
                    'cols' => 50,
                    'data-original-title' => $model->getAttributeLabel('text_3'),
                    'data-content' => $model->getAttributeDescription('text_3')
                ]
            ]]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->textAreaGroup($model, 'text_4', [
            'widgetOptions' => [
                'htmlOptions' => [
                    'class' => 'popover-help',
                    'rows' => 6,
                    'cols' => 50,
                    'data-original-title' => $model->getAttributeLabel('text_4'),
                    'data-content' => $model->getAttributeDescription('text_4')
                ]
            ]]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->textAreaGroup($model, 'text_5', [
            'widgetOptions' => [
                'htmlOptions' => [
                    'class' => 'popover-help',
                    'rows' => 6,
                    'cols' => 50,
                    'data-original-title' => $model->getAttributeLabel('text_5'),
                    'data-content' => $model->getAttributeDescription('text_5')
                ]
            ]]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->textAreaGroup($model, 'text_6', [
            'widgetOptions' => [
                'htmlOptions' => [
                    'class' => 'popover-help',
                    'rows' => 6,
                    'cols' => 50,
                    'data-original-title' => $model->getAttributeLabel('text_6'),
                    'data-content' => $model->getAttributeDescription('text_6')
                ]
            ]]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->textFieldGroup($model, 'tariff', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('tariff'),
                        'data-content' => $model->getAttributeDescription('tariff')
                    ]
                ]
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=  $form->textFieldGroup($model, 'portfolio_id', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('portfolio_id'),
                        'data-content' => $model->getAttributeDescription('portfolio_id')
                    ]
                ]
            ]); ?>
        </div>
    </div>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => Yii::t('OrderModule.order', 'Сохранить заказ и продолжить'),
    ]
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'htmlOptions'=> ['name' => 'submit-type', 'value' => 'index'],
        'label'      => Yii::t('OrderModule.order', 'Сохранить заказ и закрыть'),
    ]
); ?>

<?php $this->endWidget(); ?>