<?php

/**
 * Order install migration
 * Класс миграций для модуля Order:
 *
 * @category YupeMigration
 * @package  yupe.modules.order.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000000_order_base extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{order_order}}',
            [
                'id' => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'status' => 'tinyint(1) NOT NULL DEFAULT 0',
                'user_status' => 'tinyint(1) NOT NULL DEFAULT 1',
                'user' => 'string NOT NULL',
                'name' => 'string',
                'email' => 'string',
                'phone' => 'string',
                'text_1' => 'text',
                'text_2' => 'text',
                'text_3' => 'text',
                'text_4' => 'text',
                'text_5' => 'text',
                'text_6' => 'text',
                'tariff' => 'string',
                'portfolio_id' => 'integer DEFAULT NULL',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{order_order}}_create_time", '{{order_order}}', "create_time", false);
        $this->createIndex("ix_{{order_order}}_update_time", '{{order_order}}', "update_time", false);
        $this->createIndex("ix_{{order_order}}_status", '{{order_order}}', "status", false);
        $this->createIndex("ix_{{order_order}}_user", '{{order_order}}', "user", false);

        /* Vacancy Order Files */
        $this->createTable(
            '{{order_file}}',
            [
                'id' => 'pk',
                'order_id' => 'integer NOT NULL',
                'file' => 'string',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{order_file}}_order_id", '{{order_file}}', "order_id", false);

        //fk
        $this->addForeignKey('fk_{{order_file}}_order_id', '{{order_file}}', 'order_id', '{{order_order}}', 'id', 'CASCADE');
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{order_file}}');
        $this->dropTableWithForeignKeys('{{order_order}}');
    }
}
