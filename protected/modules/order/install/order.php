<?php
/**
 * Файл настроек для модуля order
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2017 amyLabs && Yupe! team
 * @package yupe.modules.order.install
 * @since 0.1
 *
 */
return [
    'module' => [
        'class' => 'application.modules.order.OrderModule',
    ],
    'import' => [
        'application.modules.order.models.*',
    ],
    'component' => [],
    'rules' => [
        '/order' => '/order/order/create',
//        '/order/step2' => '/order/order/createStep2',
        '/order/done/<id:\d+>' => '/order/order/done',
        '/order/tariff' => '/order/order/tariff',
        '/order/portfolio/<id:\d+>' => '/order/order/portfolio',
    ],
];
