<?php

/**
 * Client install migration
 * Класс миграций для модуля Client:
 *
 * @category YupeMigration
 * @package  yupe.modules.client.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000000_client_base extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{client_client}}',
            [
                'id' => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'status' => 'tinyint(1) NOT NULL',
                'title' => 'string NOT NULL',
                'slug' => 'varchar(150) NOT NULL',
                'text' => 'text',
                'image' => 'string',
                'image2' => 'string',
                'link' => 'string',
                'seo_title' => 'string',
                'seo_keywords' => 'string',
                'seo_description' => 'string',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{client_client}}_create_time", '{{client_client}}', "create_time", false);
        $this->createIndex("ix_{{client_client}}_update_time", '{{client_client}}', "update_time", false);
        $this->createIndex("ix_{{client_client}}_status", '{{client_client}}', "status", false);
        $this->createIndex("ix_{{client_client}}_slug", '{{client_client}}', "slug", true);
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{client_client}}');
    }
}
