<?php
/**
 * Файл настроек для модуля client
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2016 amyLabs && Yupe! team
 * @package yupe.modules.client.install
 * @since 0.1
 *
 */
return [
    'module' => [
        'class' => 'application.modules.client.ClientModule',
    ],
    'import' => [
        'application.modules.client.listeners.*',
        'application.modules.client.models.*',
    ],
    'component' => [
        'eventManager' => [
            'class' => 'yupe\components\EventManager',
            'events' => [
                'sitemap.before.generate' => [
                    ['\ClientSitemapGeneratorListener', 'onGenerate'],
                ],
            ],
        ],
    ],
    'rules' => [
        '/clients' => '/client/client/index',
        '/clients/<slug:[\w-]+>' => [
            '/client/client/view',
            'type' => 'db',
            'fields' => [
                'slug' => [
                    'table' => '{{client_client}}',
                    'field' => 'slug',
                ],
            ],
        ],
    ],
];