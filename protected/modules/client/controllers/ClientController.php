<?php

/**
 * ClientController контроллер для client на публичной части сайта
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2016 amyLabs && Yupe! team
 * @package yupe.modules.client.controllers
 * @since 0.1
 *
 */
class ClientController extends \yupe\components\controllers\FrontController
{
    public function behaviors()
    {
        return [
            'seo' => ['class' => 'vendor.chemezov.yii-seo.behaviors.SeoBehavior'],
        ];
    }

    public function actionIndex()
    {
        $criteria = new CDbCriteria;

        $portfolio_table = Portfolio::model()->tableName();
        $portfolio_count_sql = "(select count(*) from $portfolio_table pt where pt.client_id = t.id)";

        $criteria->select = [
            '*',
            $portfolio_count_sql . ' as searchPortfolioCount',
        ];

        $dataProvider = new CActiveDataProvider(Client::model()->published(), [
            'criteria' => $criteria,
            'sort' => [
                'defaultOrder' => 'searchPortfolioCount DESC',
                'attributes' => [
                    // order by
                    'searchPortfolioCount' => [
                        'asc' => 'searchPortfolioCount ASC',
                        'desc' => 'searchPortfolioCount DESC',
                    ],
                    '*',
                ],
            ],
            'pagination' => [
                'class' => 'Pagination',
                'pageSize' => 7 * 7,
            ],
        ]);

        $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionView($slug)
    {
        $model = $this->loadModel($slug);

        $criteria = new CDbCriteria();

        $criteria->compare('t.client_id', $model->id);

        $portfolioDataProvider = new CActiveDataProvider(Portfolio::model()->published(), [
            'criteria' => $criteria,
            'sort' => ['defaultOrder' => 't.date DESC'],
            'pagination' => false,
        ]);

        $reviewsDataProvider = new CActiveDataProvider(Review::model()->published(), [
            'criteria' => $criteria,
            'sort' => ['defaultOrder' => 't.date DESC'],
            'pagination' => false,
        ]);

        $this->render('view', ['model' => $model, 'portfolioDataProvider' => $portfolioDataProvider, 'reviewsDataProvider' => $reviewsDataProvider]);
    }

    /**
     * @param string $slug
     * @return Client
     * @throws CHttpException
     */
    protected function loadModel($slug)
    {
        $model = Client::model()->published()->findByAttributes(['slug' => $slug]);

        if (!$model) {
            throw new CHttpException(404);
        }

        return $model;
    }
}
