<?php

/**
 * Класс ClientBackendController:
 *
 * @category Yupe\yupe\components\controllers\BackController
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
class ClientBackendController extends \yupe\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Client.ClientBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Client.ClientBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Client.ClientBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Client.ClientBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Client.ClientBackend.Delete']],
            ['deny'],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'yupe\components\actions\YInLineEditAction',
                'model' => 'Client',
                'validAttributes' => ['title', 'slug', 'link', 'status'],
            ],
        ];
    }

    /**
     * Отображает клиента по указанному идентификатору
     *
     * @param integer $id Идинтификатор клиента для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Создает новую модель клиента.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new Client;

        if (Yii::app()->getRequest()->getPost('Client') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Client'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ClientModule.client', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование клиента.
     *
     * @param integer $id Идинтификатор клиента для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('Client') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Client'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ClientModule.client', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель клиента из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор клиента, который нужно удалить
     *
     * @return void
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('ClientModule.client', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t('ClientModule.client', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
        }
    }

    /**
     * Управление клиентами.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new Client('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('Client') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('Client'));
        }
        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param $id integer идентификатор нужной модели
     *
     * @return Client
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Client::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('ClientModule.client', 'Запрошенная страница не найдена.'));
        }

        return $model;
    }
}
