<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('ClientModule.client', 'Клиенты') => ['/client/clientBackend/index'],
    $model->title => ['/client/clientBackend/view', 'id' => $model->id],
    Yii::t('ClientModule.client', 'Редактирование'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'Клиенты - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ClientModule.client', 'Управление клиентами'), 'url' => ['/client/clientBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ClientModule.client', 'Добавить клиента'), 'url' => ['/client/clientBackend/create']],
    ['label' => Yii::t('ClientModule.client', 'Клиент') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('ClientModule.client', 'Редактирование клиента'), 'url' => [
        '/client/clientBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('ClientModule.client', 'Просмотреть клиента'), 'url' => [
        '/client/clientBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('ClientModule.client', 'Удалить клиента'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/client/clientBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('ClientModule.client', 'Вы уверены, что хотите удалить клиента?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('ClientModule.client', 'Редактирование') . ' ' . Yii::t('ClientModule.client', 'клиента'); ?>        <br/>
        <small>&laquo;<?=  $model->title; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>