<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Client
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('ClientModule.client', 'Клиенты') => ['/client/clientBackend/index'],
    Yii::t('ClientModule.client', 'Управление'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'Клиенты - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ClientModule.client', 'Управление клиентами'), 'url' => ['/client/clientBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ClientModule.client', 'Добавить клиента'), 'url' => ['/client/clientBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('ClientModule.client', 'Клиенты'); ?>
        <small><?= Yii::t('ClientModule.client', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?= Yii::t('ClientModule.client', 'Поиск клиентов'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('client-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<br/>

<p> <?= Yii::t('ClientModule.client', 'В данном разделе представлены средства управления клиентами'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id' => 'client-grid',
        'type' => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'id',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->id, ["update", "id" => $data->id]);
                },
                'htmlOptions' => ['width' => '100px'],
            ],
            [
                'type' => 'raw',
                'name' => 'image',
                'filter' => false,
                'value' => function (Client $data) {
                    return CHtml::image($data->getImageUrl(120, 80), $data->title, ["width" => 120, "height" => 80, "class" => "img-thumbnail"]);
                },
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'title',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'slug',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'slug', ['class' => 'form-control']),
            ],
//            'create_time',
//            'update_time',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    $model::STATUS_PUBLISHED => ['class' => 'label-success'],
                    $model::STATUS_MODERATION => ['class' => 'label-warning'],
                    $model::STATUS_DRAFT => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'link',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'link', ['class' => 'form-control']),
            ],
            [
                'name'  => 'searchPortfolioCount',
                'type'  => 'raw',
                'value' => 'CHtml::link($data->searchPortfolioCount, ["/portfolio/portfolioBackend/index", "Portfolio[client_id]" => $data->id])',
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'frontViewButtonUrl' => function ($data) {
                    return $data->url;
                },
                'buttons' => [
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->status == Client::STATUS_PUBLISHED;
                        },
                    ],
                ],
            ],
        ],
    ]
); ?>
