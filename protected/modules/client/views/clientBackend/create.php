<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('ClientModule.client', 'Клиенты') => ['/client/clientBackend/index'],
    Yii::t('ClientModule.client', 'Добавление'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'Клиенты - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ClientModule.client', 'Управление клиентами'), 'url' => ['/client/clientBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ClientModule.client', 'Добавить клиента'), 'url' => ['/client/clientBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('ClientModule.client', 'Клиенты'); ?>
        <small><?=  Yii::t('ClientModule.client', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>