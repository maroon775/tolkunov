<?php

/**
 * This is the model class for table "{{client_client}}".
 *
 * The followings are the available columns in table '{{client_client}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property integer $status
 * @property string $title
 * @property string $slug
 * @property string $text
 * @property string $image
 * @property string $image2
 * @property string $link
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 *
 * @property integer $portfolioCount
 * @property integer $reviewsCount
 *
 * @method Client published()
 */
class Client extends yupe\models\YModel
{
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;
    /**
     *
     */
    const STATUS_MODERATION = 2;

    public $searchPortfolioCount;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{client_client}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['title, slug, text, link, seo_title, seo_description, seo_keywords', 'filter', 'filter' => 'trim'],
            ['title, slug, text, link, seo_title, seo_description, seo_keywords', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],
            ['title, slug', 'required'],
            ['status', 'numerical', 'integerOnly' => true],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['title, link, seo_title, seo_keywords, seo_description', 'length', 'max' => 255],
            ['slug', 'unique'],
//            ['link', 'yupe\components\validators\YUrlValidator'],
            [
                'slug',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('ClientModule.client', 'Bad characters in {attribute} field'),
            ],
            ['slug', 'length', 'max' => 150],
            ['text', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, status, title, slug, text, image, image2, link, seo_title, seo_keywords, seo_description, searchPortfolioCount', 'safe', 'on' => 'search'],
        ];
    }

    public function defaultScope()
    {
        return [
            'order' => $this->getTableAlias(false, false) . '.create_time DESC',
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => $this->tableAlias . '.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('client');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
            'seo' => [
                'class' => 'vendor.chemezov.yii-seo.behaviors.SeoActiveRecordBehavior',
                'route' => '/client/client/view',
                'params' => [
                    'slug' => function (Client $data) {
                        return $data->slug;
                    },
                ],
            ],
            'imageUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
                'defaultImage' => $module->defaultImage,
            ],
            'imageUpload2' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image2',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
                'defaultImage' => $module->defaultImage,
                'deleteFileKey' => 'delete-file-2',
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'reviewsCount' => [self::STAT, 'Review', 'client_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата обновления',
            'status' => 'Статус',
            'title' => 'Имя клиента',
            'slug' => 'URL',
            'text' => 'Описание',
            'image' => 'Изображение',
            'image2' => 'Изображение (ч/б)',
            'link' => 'Ссылка на сайт клиента',
            'seo_title' => 'SEO Title',
            'seo_keywords' => 'SEO Keywords',
            'seo_description' => 'SEO Description',
            'searchPortfolioCount' => 'Кол-во баннеров',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.status', $this->status);
        $criteria->compare($this->tableAlias . '.title', $this->title, true);
        $criteria->compare($this->tableAlias . '.slug', $this->slug, true);
        $criteria->compare($this->tableAlias . '.text', $this->text, true);
        $criteria->compare($this->tableAlias . '.image', $this->image, true);
        $criteria->compare($this->tableAlias . '.image2', $this->image2, true);
        $criteria->compare($this->tableAlias . '.link', $this->link, true);
        $criteria->compare($this->tableAlias . '.seo_title', $this->seo_title, true);
        $criteria->compare($this->tableAlias . '.seo_keywords', $this->seo_keywords, true);
        $criteria->compare($this->tableAlias . '.seo_description', $this->seo_description, true);

        $portfolio_table = Portfolio::model()->tableName();
        $portfolio_count_sql = "(select count(*) from $portfolio_table pt where pt.client_id = t.id)";

        $criteria->select = [
            '*',
            $portfolio_count_sql . ' as searchPortfolioCount',
        ];

        $criteria->compare($portfolio_count_sql, $this->searchPortfolioCount);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'sort' => [
                'defaultOrder' => 'searchPortfolioCount DESC',
                'attributes' => [
                    // order by
                    'searchPortfolioCount' => [
                        'asc' => 'searchPortfolioCount ASC',
                        'desc' => 'searchPortfolioCount DESC',
                    ],
                    '*',
                ],
            ],
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Client the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('ClientModule.client', 'Draft'),
            self::STATUS_MODERATION => Yii::t('ClientModule.client', 'On moderation'),
            self::STATUS_PUBLISHED => Yii::t('ClientModule.client', 'Published'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('ClientModule.client', '*unknown*');
    }

    public function getLinkText()
    {
        return parse_url($this->link, PHP_URL_HOST) ?: $this->link;
    }

    public function getLinkUrl()
    {
        return parse_url($this->link, PHP_URL_HOST) ? $this->link : 'http://' . $this->link;
    }
}
