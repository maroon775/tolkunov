<?php
use yupe\widgets\YWidget;

/**
 * Class SubscribeFormWidget
 */
class SubscribeFormWidget extends YWidget
{
    /**
     * @var string
     */
    public $view = 'subscribe';

    /**
     * @throws CException
     */
    public function run()
    {
        $model = new SubscribeForm();

        $this->render($this->view, [
            'model' => $model,
        ]);
    }
}
