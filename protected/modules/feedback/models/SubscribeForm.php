<?php

class SubscribeForm extends CFormModel
{
    public $email;

    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],
            ['email', 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Email',
        ];
    }
}
