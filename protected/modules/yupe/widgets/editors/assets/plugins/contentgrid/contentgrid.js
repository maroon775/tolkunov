(function ($) {
    $.Redactor.prototype.contentgrid = function () {
        return {
            getTemplate: function () {
                return String()
                    + '<div class="modal-section" id="redactor-modal-contentgrid">'
                    + '<section>'
                    + '<label>Введите кол-во блоков</label>'
                    + '<input type="number" id="mymodal-number" value="4"/>'
                    + '</section>'
                    + '</div>';
            },
            init: function () {
                var button = this.button.add('advanced', 'Вставить сетку');
                this.button.addCallback(button, this.contentgrid.show);

                this.button.setAwesome('advanced', 'fa-briefcase');
            },
            show: function () {
                this.modal.addTemplate('contentgrid', this.contentgrid.getTemplate());
                this.modal.load('contentgrid', 'Вставить адаптивную сетку', 500);
                this.modal.createCancelButton();

                var button = this.modal.createActionButton('Вставить');
                button.on('click', this.contentgrid.insert);

                this.selection.save();
                this.modal.show();

                $('#mymodal-textarea').focus();
            },
            getBlockHTML: function () {
                return String()
                    + '<div class="b-content-grid__item">'
                    + '<div class="b-content-grid__image">Image</div>'
                    + '<h4 class="b-content-grid__title">Lorem ipsum dolor sit amet</h4>'
                    + '<div class="b-content-grid__text">'
                    + '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed libero turpis, sodales at diam sit amet, ullamcorper viverra quam. Nam placerat, augue a blandit ultrices</p>'
                    + '</div>'
                    + '</div>';
            },
            insert: function () {
                var count = parseInt($('#mymodal-number').val());

                if (count <= 0) return false;

                var html = '<div class="b-content-grid">';

                for (var i = 0; i < count; i++) {
                    html += this.contentgrid.getBlockHTML();
                }

                html += '</div>';

                this.modal.close();
                this.selection.restore();

                this.buffer.set(); // for undo action
                this.insert.htmlWithoutClean(html);
            }
        };
    };
})(jQuery);
