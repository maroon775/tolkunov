<?=  "<?php\n"; ?>
/**
 * <?=  ucfirst($this->moduleID); ?> install migration
 * Класс миграций для модуля <?=  ucfirst($this->moduleID); ?>:
 *
 * @category YupeMigration
 * @package  yupe.modules.<?=  $this->moduleID; ?>.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000000_<?=  $this->moduleID; ?>_base extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{<?=  $this->moduleID; ?>_<?=  $this->moduleID; ?>}}',
            [
                'id'             => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time'    => 'datetime NOT NULL',
                'update_time'    => 'datetime NOT NULL',
                'status' => 'tinyint(1) NOT NULL DEFAULT 0',
                'title' => 'string NOT NULL',
                'text' => 'text',
                'image' => 'string',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{<?=  $this->moduleID; ?>_<?=  $this->moduleID; ?>}}_create_time", '{{<?=  $this->moduleID; ?>_<?=  $this->moduleID; ?>}}', "create_time", false);
        $this->createIndex("ix_{{<?=  $this->moduleID; ?>_<?=  $this->moduleID; ?>}}_update_time", '{{<?=  $this->moduleID; ?>_<?=  $this->moduleID; ?>}}', "update_time", false);
        $this->createIndex("ix_{{<?=  $this->moduleID; ?>_<?=  $this->moduleID; ?>}}_status", '{{<?=  $this->moduleID; ?>_<?=  $this->moduleID; ?>}}', "status", false);

    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{<?=  $this->moduleID; ?>_<?=  $this->moduleID; ?>}}');
    }
}
