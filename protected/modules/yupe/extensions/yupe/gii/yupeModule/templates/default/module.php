<?php
/**
 * @var $this YupeModuleCode
 */
?>
<?=  "<?php\n"; ?>

use yupe\components\WebModule;

/**
 * <?=  $this->moduleClass; ?> основной класс модуля <?=  $this->moduleID."\n"; ?>
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-<?= date('Y'); ?> amyLabs && Yupe! team
 * @package yupe.modules.<?=  $this->moduleID."\n"; ?>
 * @since 0.1
 */

class <?= $this->moduleClass; ?> extends WebModule
{
    const VERSION = '1.0';

    /**
    * @var string
    */
    public $uploadPath = '<?=  $this->moduleID; ?>';
    /**
    * @var string
    */
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    /**
    * @var int
    */
    public $minSize = 0;
    /**
    * @var int
    */
    public $maxSize = 5368709120;
    /**
    * @var int
    */
    public $maxFiles = 1;

    /**
     * Массив с именами модулей, от которых зависит работа данного модуля
     *
     * @return array
     */
    public function getDependencies()
    {
        <?php if (count($this->getModuleDependencies())): ?>
        return ['<?=implode("', '", $this->getModuleDependencies())?>'];
        <?php else:?>
        return [];
        <?php endif;?>
    }

    /**
     * Работоспособность модуля может зависеть от разных факторов: версия php, версия Yii, наличие определенных модулей и т.д.
     * В этом методе необходимо выполнить все проверки.
     *
     * @return array|false
     */
    public function checkSelf()
    {
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    '<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>',
                    'Directory "{dir}" is not writeable! {link}',
                    [
                        '{dir}' => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Change settings'),
                            [
                                '/yupe/backend/modulesettings/',
                                'module' => '<?=  $this->moduleID; ?>',
                            ]
                        ),
                    ]
                ),
            ];
        }

        return isset($messages[WebModule::CHECK_ERROR]) ? $messages : true;
    }

    /**
    * @return bool
    */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755);
        }

        return false;
    }

    /**
     * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
     *
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', '<?=  $this->moduleCategory; ?>');
    }

    /**
     * массив лейблов для параметров (свойств) модуля. Используется на странице настроек модуля в панели управления.
     *
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'editor' => Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Visual Editor'),
            'uploadPath' => Yii::t(
                '<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>',
                'File uploads directory (relative to "{path}")',
                ['{path}' => Yii::getPathOfAlias('webroot').'/'.Yii::app()->getModule("yupe")->uploadPath]
            ),
            'allowedExtensions' => Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Accepted extensions (separated by comma)'),
            'minSize' => Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Minimum size (in bytes)'),
            'maxSize' => Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Maximum size (in bytes)'),
        ];
    }

    /**
     * массив параметров модуля, которые можно редактировать через панель управления (GUI)
     *
     * @return array
     */
    public function getEditableParams()
    {
        return [
            'editor' => Yii::app()->getModule('yupe')->getEditors(),
            'uploadPath',
            'allowedExtensions',
            'minSize',
            'maxSize',
        ];
    }

    /**
     * массив групп параметров модуля, для группировки параметров на странице настроек
     *
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return parent::getEditableParamsGroups();
    }

    /**
     * если модуль должен добавить несколько ссылок в панель управления - укажите массив
     *
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', '<?=  $this->moduleID; ?>')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Index'),
                'url' => ['/<?=  $this->moduleID; ?>/<?=  $this->moduleID; ?>Backend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Add'),
                'url' => ['/<?=  $this->moduleID; ?>/<?=  $this->moduleID; ?>Backend/create']
            ],
        ];
    }

    /**
     * текущая версия модуля
     *
     * @return string
     */
    public function getVersion()
    {
        return Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', self::VERSION);
    }

    /**
     * Возвращает название модуля
     *
     * @return string.
     */
    public function getName()
    {
        return Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', '<?=  $this->moduleID; ?>');
    }

    /**
     * Возвращает описание модуля
     *
     * @return string.
     */
    public function getDescription()
    {
        return Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Описание модуля "<?=  $this->moduleID; ?>"');
    }

    /**
     * Имя автора модуля
     *
     * @return string
     */
    public function getAuthor()
    {
        return Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', '<?= $this->moduleAuthor; ?>');
    }

    /**
     * Контактный email автора модуля
     *
     * @return string
     */
    public function getAuthorEmail()
    {
        return Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', '<?= $this->moduleAuthorEmail; ?>');
    }

    /**
    * веб-сайт разработчика модуля или страничка самого модуля
    *
    * @return string
    */
    public function getUrl()
    {
        return Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', '<?= $this->moduleAuthorSite; ?>');
    }

    /**
     * Ссылка, которая будет отображена в панели управления
     * Как правило, ведет на страничку для администрирования модуля
     *
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/<?=  $this->moduleID; ?>/<?=  $this->moduleID; ?>Backend/index';
    }

    /**
     * Название иконки для меню админки, например 'user'
     *
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-<?=  $this->moduleIcon; ?>";
    }

    /**
      * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
      *
      * @return bool
      **/
    public function getIsInstallDefault()
    {
        return parent::getIsInstallDefault();
    }

    /**
     * Инициализация модуля, считывание настроек из базы данных и их кэширование
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                '<?=  $this->moduleID; ?>.models.*',
                '<?=  $this->moduleID; ?>.components.*',
            ]
        );
    }

    /**
     * Массив правил модуля
     * @return array
     */
    public function getAuthItems()
    {
        return [
            [
                'name' => '<?=  ucfirst($this->moduleID); ?>.<?=  ucfirst($this->moduleID); ?>Manager',
                'description' => Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Manage <?=  $this->moduleID; ?>'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => '<?=  ucfirst($this->moduleID); ?>.<?=  ucfirst($this->moduleID); ?>Backend.Create',
                        'description' => Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Create')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => '<?=  ucfirst($this->moduleID); ?>.<?=  ucfirst($this->moduleID); ?>Backend.Delete',
                        'description' => Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Delete')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => '<?=  ucfirst($this->moduleID); ?>.<?=  ucfirst($this->moduleID); ?>Backend.Index',
                        'description' => Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Index')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => '<?=  ucfirst($this->moduleID); ?>.<?=  ucfirst($this->moduleID); ?>Backend.Update',
                        'description' => Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Update')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => '<?=  ucfirst($this->moduleID); ?>.<?=  ucfirst($this->moduleID); ?>Backend.View',
                        'description' => Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'View')
                    ],
                ]
            ]
        ];
    }
}
