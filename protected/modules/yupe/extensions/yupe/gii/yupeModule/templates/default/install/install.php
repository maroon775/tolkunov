<?=  "<?php\n"; ?>
/**
 * Файл настроек для модуля <?=  $this->moduleID."\n"; ?>
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-<?= date('Y'); ?> amyLabs && Yupe! team
 * @package yupe.modules.<?=  $this->moduleID; ?>.install
 * @since 0.1
 *
 */
return [
    'module'    => [
        'class' => 'application.modules.<?=  $this->moduleID; ?>.<?=  $this->moduleClass; ?>',
    ],
    'import'    => [
        'application.modules.<?=  $this->moduleID; ?>.models.*',
    ],
    'component' => [],
    'rules'     => [
        '/<?=  $this->moduleID; ?>' => '/<?=  $this->moduleID; ?>/<?=  $this->moduleID; ?>/index',
        '/<?=  $this->moduleID; ?>/<slug:[\w-]+>' => [
            '/<?=  $this->moduleID; ?>/<?=  $this->moduleID; ?>/view',
            'type' => 'db',
            'fields' => [
                'slug' => [
                    'table' => '{{<?=  $this->moduleID; ?>_<?=  $this->moduleID; ?>}}',
                    'field' => 'slug',
                ],
            ],
        ],
    ],
];