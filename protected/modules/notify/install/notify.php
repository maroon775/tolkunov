<?php
return [
    'module' => [
        'class' => 'application.modules.notify.NotifyModule',
    ],
    'import' => [
        'application.modules.notify.listeners.*',
    ],
    'component' => [
        'notify' => [
            'class' => 'notify\components\Notify',
            'mail' => [
                'class' => 'yupe\components\Mail',
                'method' => 'sendmail',
            ],
        ],
    ],
    'rules' => [
        '/profile/notify' => 'notify/notify/settings',
    ],
];
