<?php

/**
 * BlogsWidget виджет для вывода блогов
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.blog.widgets
 * @since 0.1
 *
 */

Yii::import('application.modules.blog.models.Blog');

/**
 * Class BlogsWidget
 */
class BlogsWidget extends yupe\widgets\YWidget
{
    /**
     * @var string
     */
    public $view = 'blogswidget';

    /**
     * @var Blog|null
     */
    public $current;

    /**
     * @throws CException
     */
    public function run()
    {
        $models = Blog::model()->public()->published()->roots()->cache($this->cacheTime)->findAll();

        $this->render($this->view, ['models' => $models]);
    }
}
