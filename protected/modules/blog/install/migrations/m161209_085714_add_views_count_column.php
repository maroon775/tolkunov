<?php

class m161209_085714_add_views_count_column extends CDbMigration
{
    public function up()
    {
        $this->addColumn('{{blog_post}}', 'views', 'integer NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('{{blog_post}}', 'views');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}