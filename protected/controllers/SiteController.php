<?php
/**
 * Дефолтный контроллер сайта:
 *
 * @category YupeController
 * @package  yupe.controllers
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  0.5.3 (dev)
 * @link     http://yupe.ru
 *
 **/
namespace application\controllers;

use yupe\components\controllers\FrontController;
use \Yii;
use \Imagick;

class SiteController extends FrontController
{
    /**
     * Отображение главной страницы
     *
     * @return void
     */
    public function actionIndex()
    {
        $this->render('index');
    }

    /**
     * Отображение для ошибок:
     *
     * @return void
     */
    public function actionError()
    {
        $error = \Yii::app()->errorHandler->error;

        if (empty($error) || !isset($error['code']) || !(isset($error['message']) || isset($error['msg']))) {
            $this->redirect(['index']);
        }

        if (!\Yii::app()->getRequest()->getIsAjaxRequest()) {

            $this->render(
                'error',
                [
                    'error' => $error,
                ]
            );
        }
    }

    public function loadSizes($type)
    {
        $command = Yii::app()->db->createCommand();

        $command
            ->select(['CONCAT_WS("x",items.width,items.height) as size_string'])
            ->from('{{portfolio_item}} as items')
            ->join('{{portfolio_portfolio}} p', 'items.portfolio_id = p.id')
            ->where('p.type = :type')
            ->group('size_string')
            ->order('COUNT(*) DESC')
            ->limit(6);

        return $command->queryColumn([':type' => $type]);
    }

    public function actionImage()
    {
        $fontpath_light = Yii::app()->uploadManager->getBasePath() . "/Light.ttf";
        $fontpath_bold = Yii::app()->uploadManager->getBasePath() . "/Bold.ttf";

        if (!file_exists($fontpath_light)) {
            die("Error!  Font file does not exist at '" . $fontpath_light . "'");
        }

        if (!file_exists($fontpath_bold)) {
            die("Error!  Font file does not exist at '" . $fontpath_light . "'");
        }

        $baseImage = Yii::app()->uploadManager->getBasePath() . "/cover.png";
        $cachefile = Yii::app()->uploadManager->getBasePath() . "/cover_cache.png";

        $portfolioCount = \Portfolio::model()->published()->count();
        $reviewsCount = \Review::model()->published()->count();
        $postsCount = \Post::model()->published()->count();
        $offersCount = \OfferItem::model()->getMaxBonus();

//        $portfolioCount = 985;//\Portfolio::model()->published()->count();
//        $reviewsCount = 232;//\Review::model()->published()->count();
//        $postsCount = 15;//\Post::model()->published()->count();
//        $offersCount = 40000;//\OfferItem::model()->getMaxBonus();

        $cacheKey = 'cover.png';

        if (Yii::app()->cache->get($cacheKey) === false) {
            if (file_exists($cachefile)) {
                unlink($cachefile);
            }

            Yii::app()->cache->set($cacheKey, 1, 60 * 10);
        }

        if (!file_exists($cachefile)) {

            try {

                /* Base canvas for result image */
                $canvas = new \Imagick($baseImage);

                /* Draw for count text */
                $countDraw = new \ImagickDraw();
                $countDraw->setFont($fontpath_light);
                $countDraw->setFontSize(50);
                $countDraw->setTextKerning(-2);
                $countDraw->setGravity(\Imagick::GRAVITY_WEST);
                $countDraw->setFillColor('#ffffff');

                /* Draw for text */
                $textDraw = new \ImagickDraw();
                $textDraw->setFont($fontpath_light);
                $textDraw->setTextInterlineSpacing(-8);
                $textDraw->setFontSize(24);
                $textDraw->setGravity(\Imagick::GRAVITY_WEST);
                $textDraw->setFillColor('#ffffff');

                $data = [
                    [
                        'count' => $portfolioCount,
                        'text' => Yii::t('PortfolioModule.portfolio', "баннер\nв портфолио|баннера\nв портфолио|баннеров\nв портфолио", [$portfolioCount]),
                        'backgroundColor' => '#e74c3c',
                        'top' => 281,
                    ],
                    [
                        'count' => $reviewsCount,
                        'text' => Yii::t('ReviewModule.review', "отзыв\nот клиентов|отзыва\nот клиентов|отзывов\nот клиентов", [$reviewsCount]),
                        'backgroundColor' => '#008fcb',
                        'top' => 281,
                    ],
                    [
                        'count' => $postsCount,
                        'text' => Yii::t('BlogModule.blog', "статья в нашем\nсправочнике|статьи в нашем\nсправочнике|статей в нашем\nсправочнике", [$postsCount]),
                        'backgroundColor' => '#2ecc71',
                        'top' => 281 + 79 + 10,
                    ],
                    [
                        'count' => $offersCount,
                        'text' => Yii::t('OfferModule.offer', "рубль\nбонусов|рубля\nбонусов|рублей\nбонусов", [$offersCount]),
                        'backgroundColor' => '#9c1cae',
                        'top' => 281 + 79 + 10,
                    ],
                ];

                foreach ($data as $key => $item) {
                    $metrics = $canvas->queryFontMetrics($countDraw, $item['count']);
                    $textMetrics = $canvas->queryFontMetrics($textDraw, $item['text']);

                    $blockWidth = ceil(20 + $metrics['textWidth'] + 7 + $textMetrics['textWidth'] + 20);
                    $data[$key]['blockWidth'] = $blockWidth;
                }

                foreach ($data as $key => $item) {
                    if ($key % 2) {
                        $otherBlockWidth = $data[$key - 1]['blockWidth'];
                    } else {
                        $otherBlockWidth = $data[$key + 1]['blockWidth'];
                    }

                    $data[$key]['lineWidth'] = $data[$key]['blockWidth'] + $otherBlockWidth + 11;
                }

                foreach ($data as $key => $item) {
                    $metrics = $canvas->queryFontMetrics($countDraw, $item['count']);
                    $textMetrics = $canvas->queryFontMetrics($textDraw, $item['text']);

                    $blockWidth = $item['blockWidth'];
                    $lineWidth = $item['lineWidth'];
                    $top = $item['top'];

                    $draw = new \ImagickDraw();
                    $draw->setFillColor($item['backgroundColor']);
                    $draw->roundRectangle(3, 3, $blockWidth + 3, 78 + 3, 5, 5);

                    $imagick = new \Imagick();
                    $imagick->newImage($blockWidth + 6, 78 + 6, '#fff');

                    $imagick->drawImage($draw);

                    $imagick->annotateImage($countDraw, 20, 1, 0, $item['count']);
                    $imagick->annotateImage($textDraw, ceil(20 + $metrics['textWidth'] + 7), -2, 0, $item['text']);

                    if ($key % 2) {
                        $left = ($canvas->getImageWidth() / 2) + round(($lineWidth + 6) / 2) - ($blockWidth + 6);
                    } else {
                        $left = ($canvas->getImageWidth() / 2) - round(($lineWidth + 6) / 2);
                    }

                    $canvas->compositeImage($imagick, Imagick::COMPOSITE_COPY, $left, $top);
                }

                $models = \Portfolio::model()->published()->findAll(['limit' => 4, 'order' => 'date DESC']);

                foreach ($models as $key => $model) {
//                    var_dump($model->imageUpload->getFilePath());

                    $background = new Imagick();
                    $background->newImage(150, 150, '#fff');

                    $image = new Imagick($model->imageUpload->getImagePath(150,150));
                    $image->setImageFormat("png");
//                    $image->adaptiveResizeImage(150, 150);
//                    $image->roundCorners(1, 1);
                    $image->compositeImage($background, Imagick::COMPOSITE_DSTATOP, 0, 0);

                    $left = round($canvas->getImageWidth() / 2) - round((150 * 4 + (10 * (4 - 1))) / 2) + ((150 + 10) * $key);
                    $top = 531;

                    $canvas->compositeImage($image, Imagick::COMPOSITE_COPY, $left, $top);

                    /* Draw portfolio type */
                    $textDraw = new \ImagickDraw();
                    $textDraw->setFont($fontpath_bold);
                    $textDraw->setFontSize(12);
                    $textDraw->setTextKerning(-0.2);
                    $textDraw->setGravity(\Imagick::GRAVITY_CENTER);
                    $textDraw->setFillColor('#ffffff');

                    $text = mb_strtoupper($model->getTypeString());

                    switch ($model->type) {
                        case \Portfolio::TYPE_HTML5:
                            $backgroundColor = '#9c1cae';
                            break;
                        case \Portfolio::TYPE_GIF:
                            $backgroundColor = '#008fcb';
                            break;
                        case \Portfolio::TYPE_JPG:
                            $backgroundColor = '#2ecc71';
                            break;
                    }

                    $metrics = $canvas->queryFontMetrics($textDraw, $text);
                    $blockWidth = $metrics['textWidth'] + 16;

                    $draw = new \ImagickDraw();
                    $draw->setFillColor($backgroundColor);
                    $draw->roundRectangle(3, 3, $blockWidth + 3, 20 + 3, 10, 10);

                    $imagick = new \Imagick();
                    $imagick->newImage($blockWidth + 6, 20 + 6, 'transparent');

                    $imagick->drawImage($draw);
                    $imagick->setImageFormat('png');

                    $imagick->annotateImage($textDraw, 1, 1, 0, $text);

                    $canvas->compositeImage($imagick, Imagick::COMPOSITE_ATOP, $left + 7, $top + 7);

                    /* Draw portfolio date */
                    $textDraw = new \ImagickDraw();
                    $textDraw->setFont($fontpath_bold);
                    $textDraw->setFontSize(12);
                    $textDraw->setTextKerning(-0.4);
                    $textDraw->setGravity(\Imagick::GRAVITY_CENTER);
                    $textDraw->setFillColor('#ffffff');

                    $text = Yii::app()->dateFormatter->format('d.MM.yyyy', $model->date);

                    $backgroundColor = '#252525';

                    $metrics = $canvas->queryFontMetrics($textDraw, $text);
                    $blockWidth = $metrics['textWidth'] + 16;

                    $draw = new \ImagickDraw();
                    $draw->setFillColor($backgroundColor);
                    $draw->roundRectangle(3, 3, $blockWidth + 3, 20 + 3, 10, 10);

                    $imagick = new \Imagick();
                    $imagick->newImage($blockWidth + 6, 20 + 6, 'transparent');

                    $imagick->drawImage($draw);
                    $imagick->setImageFormat('png');

                    $imagick->annotateImage($textDraw, 1, 1, 0, $text);

//                    header("Content-Type: image/png");
//                    echo $imagick;
//                    die();

                    $canvas->compositeImage($imagick, Imagick::COMPOSITE_ATOP, $left + 150 - $imagick->getImageWidth() + 2 - 10, $top + 150 - $imagick->getImageHeight() + 2 - 10);
                }

                $canvas->setImageFormat('PNG');
//                $canvas->setCompressionQuality(4);
//                $canvas->adaptiveResizeImage(false, 630);
                $canvas->writeImage($cachefile);

                header("Content-Type: image/png");
                echo $canvas;

                $canvas->clear();
                $canvas->destroy();
            } catch (Exception $e) {
                echo 'Error: ', $e->getMessage(), "";
            }

        } else {
            header('Content-Type: image/png');
            header('Content-Length: ' . filesize($cachefile));
            readfile($cachefile);
        }
    }
}
