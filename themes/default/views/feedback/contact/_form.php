<?php
/**
 * @var $this ContactController
 * @var $model FeedBackForm
 * @var $form TbActiveForm
 */
?>

<div id="contact_form_block">
    <?php $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        [
            'id' => 'feedback-form',
            'htmlOptions' => [
                'class' => 'b-form__form',
            ],
        ]
    ); ?>

    <?php if (Yii::app()->user->hasFlash(\yupe\widgets\YFlashMessages::SUCCESS_MESSAGE)): ?>
        <div class="b-order-done">
            <p class="b-order-done__title">Готово, ваша заявка отправлена!</p>

            <p class="b-order-done__text">Мы внимательно изучим её, а затем свяжемся с вами.</p>
        </div>
    <?php else: ?>
        <h2 class="b-form__header">Напишите нам</h2>

        <p class="b-form__lead">Чтобы получить ответ, напишите как вас зовут и как с вами связаться</p>

        <?= $form->errorSummary($model); ?>

        <?= $form->textAreaGroup(
            $model,
            'text',
            ['widgetOptions' => ['htmlOptions' => ['rows' => 10, 'placeholder' => false]], 'labelOptions' => ['class' => 'sr-only'], 'errorOptions' => ['class' => 'sr-only']]
        ); ?>

        <div class="form-group">
            <div class="b-checkbox">
                <?= $form->checkBox($model, 'agree'); ?>
                <?= $form->labelEx($model, 'agree'); ?>
                <?= $form->error($model, 'agree'); ?>
            </div>
        </div>

        <?php if ($module->showCaptcha && !Yii::app()->getUser()->isAuthenticated()): ?>
            <?php if (CCaptcha::checkRequirements()): ?>
                <?php $this->widget(
                    'CCaptcha',
                    [
                        'showRefreshButton' => true,
                        'imageOptions' => [
                            'width' => '150',
                        ],
                        'buttonOptions' => [
                            'class' => 'btn btn-info',
                        ],
                        'buttonLabel' => '<i class="glyphicon glyphicon-repeat"></i>',
                    ]
                ); ?>
                <div class='row'>
                    <div class="col-sm-6">
                        <?= $form->textFieldGroup(
                            $model,
                            'verifyCode',
                            [
                                'widgetOptions' => [
                                    'htmlOptions' => [
                                        'placeholder' => Yii::t(
                                            'FeedbackModule.feedback',
                                            'Insert symbols you see on image'
                                        ),
                                    ],
                                ],
                            ]
                        ); ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>

        <?php echo CHtml::submitButton('Отправить', ['id' => 'submit_button_' . $form->id, 'class' => 'btn btn-info btn-lg b-form__button', 'data-loading-text' => 'Отправить']); ?>
    <?php endif; ?>

    <?php $this->endWidget(); ?>
    <script>
        <?php ob_start();?>
        jQuery('body').on('click', '#submit_button_<?=$form->id?>', function () {
            var fd = new FormData();

            fd.append(yupeTokenName, yupeToken);
            fd.append('<?=get_class($model)?>[text]', $('#<?=get_class($model)?>_text').val());
            fd.append('<?=get_class($model)?>[agree]', $('#<?=get_class($model)?>_agree').is(':checked') ? 1 : 0);

            jQuery.ajax({
                'beforeSend': function () {
                    $("#submit_button_<?=$form->id?>").button("loading");
                },
                'url': '<?=Yii::app()->createUrl('/feedback/contact/index')?>',
                type: 'POST',
                cache: false,
                data: fd,
                datatype: 'json',
                processData: false,
                contentType: false,
                'success': function (html) {
                    jQuery("#contact_form_block").replaceWith(html)
                },
                error: function () {
                    $('#submit_button_<?=$form->id?>').button("reset");
                    alert("Произошла непредвиденная ошибка");
                }
            });

            return false;
        });
        <?php $vacancyFormScript = ob_get_clean(); ?>
        <?php Yii::app()->clientScript->registerScript('contact-form-script', $vacancyFormScript, CClientScript::POS_READY); ?>
    </script>
</div>
