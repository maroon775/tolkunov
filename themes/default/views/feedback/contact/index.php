<?php
$this->title = ['Контактная информация', Yii::app()->getModule('yupe')->siteName];
$this->breadcrumbs = [Yii::t('FeedbackModule.feedback', 'Contacts')];
Yii::import('application.modules.feedback.FeedbackModule');
Yii::import('application.modules.install.InstallModule');
?>

<div class="container">
    <h1 class="b-page__title">Контактная информация</h1>

    <div class="b-contacts">
        <div class="b-contacts-grid">
            <div class="b-contacts-grid__item b-contacts-grid__item_phone">
                <div class="b-contacts__item b-contacts__item_phone">+7 (812) 603-70-97</div>
            </div>
            <div class="b-contacts-grid__item b-contacts-grid__item_skype">
                <div class="b-contacts__item b-contacts__item_skype"><a href="skype:tolkunovdb?chat" class="b-contacts__link">tolkunovdb</a></div>
            </div>
            <div class="b-contacts-grid__item b-contacts-grid__item_vk">
                <div class="b-contacts__item b-contacts__item_vk"><a href="https://vk.com/im?sel=-68302756" target="_blank" class="b-contacts__link">Messenger VK</a></div>
            </div>
            <div class="b-contacts-grid__item b-contacts-grid__item_mail">
                <div class="b-contacts__item b-contacts__item_mail"><a href="mailto:order@tolkunov.com" class="b-contacts__link">order@tolkunov.com</a></div>
            </div>
            <div class="b-contacts-grid__item b-contacts-grid__item_telegram">
                <div class="b-contacts__item b-contacts__item_telegram"><a href="http://telegram.me/tolkunovdb" target="_blank" class="b-contacts__link">tolkunovdb</a></div>
            </div>
            <div class="b-contacts-grid__item b-contacts-grid__item_fb">
                <div class="b-contacts__item b-contacts__item_fb"><a href="https://www.facebook.com/messages/t/tolkunov" target="_blank" class="b-contacts__link">Messenger Facebook</a></div>
            </div>
        </div>
    </div>
    <div class="b-map">
        <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "karta-na-stranice-kontaktov"]); ?>
    </div>
    <div class="b-contact-links">
        <h2 class="b-contact-links__header">Официальные аккаунты</h2>

        <p class="b-contact-links__lead">Новые работы, полезности, акции, истории из жизни студии</p>

        <div class="b-contact-links__list">
            <a target="_blank" href="https://www.facebook.com/tolkunov/" class="b-contact-links__item b-contact-links__item_fb"></a>
            <a target="_blank" href="https://vk.com/tolkunov_com" class="b-contact-links__item b-contact-links__item_vk"></a>
            <a target="_blank" href="https://twitter.com/tolkunov_com" class="b-contact-links__item b-contact-links__item_tw"></a>
            <a target="_blank" href="https://www.instagram.com/tolkunov_com/" class="b-contact-links__item b-contact-links__item_instagram"></a>
            <a target="_blank" href="https://www.youtube.com/channel/UCwm8I4uQjaq5ycfSJZTuxrg" class="b-contact-links__item b-contact-links__item_youtube"></a>
            <a target="_blank" href="https://spark.ru/startup/tolkunov-com" class="b-contact-links__item b-contact-links__item_spark"></a>
        </div>
    </div>
</div>
<div class="b-form">
    <div class="container">
        <?php $this->renderPartial('_form', ['model' => $model, 'module' => $module]); ?>
    </div>
</div>
