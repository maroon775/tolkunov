<?php $this->title = [Yii::t('default', 'Error') . ' ' . $error['code'], Yii::app()->getModule('yupe')->siteName]; ?>

<!--<h2>--><? //= Yii::t('default', 'Error') . ' ' . $error['code']; ?><!--!</h2>-->

<?php
switch ($error['code']) {
    case '404':
        $msg = Yii::t('default', 'Страница не найдена');
        break;
    default:
        $msg = $error['message'];
        break;
}
?>
<div class="container">
    <div class="b-error">
        <p class="b-error__title"><?= $error['code'] ?></p>

        <p class="b-error__text"><?= $msg ?></p>
    </div>
</div>
