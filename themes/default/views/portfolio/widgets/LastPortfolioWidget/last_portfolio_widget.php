<?php
/**
 * @var $this LastPortfolioWidget
 * @var $models Portfolio[]
 */
?>

<div class="b-portfolio">
    <?php $this->widget(
        'zii.widgets.CListView',
        [
            'id' => 'portfolio-list',
            'dataProvider' => new CArrayDataProvider($models, ['pagination' => false]),
            'itemView' => '../../portfolio/_item',
            'template' => '{items}',
            'cssFile' => false,
            'enableHistory' => false,
            'itemsCssClass' => 'b-portfolio__list',
            'htmlOptions' => [
//                'class' => 'b-reviews__container',
                'style' => 'max-height:1584px;overflow:hidden;padding-top:10px;',
            ],
        ]
    ); ?>
    <div class="g-loader">
        <div class="g-loader__content">
            <?= CHtml::link(
                Yii::t('PortfolioModule.portfolio', 'Посмотреть {n} работу|Посмотреть все {n} работы|Посмотреть все {n} работ', [Portfolio::model()->published()->count()]),
                ['/portfolio/portfolio/index'],
                ['class' => 'btn btn-info btn-lg g-loader__link']
            ); ?>
            <p class="g-loader__text">Запаситесь чашечкой чая или кофе — для просмотра всех работ потребуется больше 50 минут.</p>
        </div>
    </div>
</div>
