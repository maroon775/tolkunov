<?php
/**
 * Отображение для portfolio/style
 *
 * @var $this PortfolioController
 * @var $dataProvider CActiveDataProvider
 * @var $baseCriteria CDbCriteria
 * @var $model Portfolio
 * @var $currentStyle PortfolioStyle
 * @var $styles PortfolioStyle[]
 **/

$this->title = $currentStyle->seo_title ?: [$currentStyle->title, 'Портфолио', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = $currentStyle->seo_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = $currentStyle->seo_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;

$this->breadcrumbs = [Yii::t('PortfolioModule.portfolio', 'Портфолио')];

Yii::app()->clientScript->registerScript('search', "
    $(function(){
    if (location.hash && $('a[href=\"' + location.hash + '\"]').length) {
        var link = $('a[href=\"' + location.hash + '\"]');

        var typeId = $(link).data('type-id');
        var href = $(link).attr('href');
        var data = 'Portfolio[type]=' + typeId;

        state = {action: 'popup', modal: data};

        $.fn.yiiListView.update('portfolio-list', {
            data: data
        });

        // Change URL in browser
        history.replaceState(state, document.title, href);
        portfolioBaseState = state;
        portfolioBaseUrl = '" . $currentStyle->url . "' + href;
    } else {
        history.replaceState(portfolioBaseState, document.title);
    }
    });

    $(document).on('click', '.b-portfolio-filter__item', function () {
        var typeId = $(this).data('type-id');
        var href = $(this).attr('href');
        var data = 'Portfolio[type]=' + typeId;

        state = {action: 'popup', modal: data};

        console.log('update list');
        $.fn.yiiListView.update('portfolio-list', {
            data: data
        });

        // Change URL in browser
        history.pushState(state, document.title, href);
        portfolioBaseState = state;
        portfolioBaseUrl = '" . $currentStyle->url . "' + href;
        return false;
    });

    // Listen for history state changes
    window.addEventListener('popstate', function (e) {
        var state = history.state;
        // back button pressed. close popup
        if (state && state.action == 'popup') {
            // Forward button pressed, reopen popup
            console.log('update list');
            $.fn.yiiListView.update('portfolio-list', {
                data: state.modal
            });
        }
    });
");
?>

<?php $this->renderPartial('_menu'); ?>

<div class="container">
    <?php $this->widget('application.components.PortfolioMenu', [
        'itemCssClass' => 'b-style-filter__item',
        'activeCssClass' => 'b-style-filter__item_active',
        'htmlOptions' => ['class' => 'b-style-filter'],
        'items' => array_map(function (PortfolioStyle $style) use ($currentStyle) {
            return [
                'label' => $style->title,
                'url' => $style->url,
                'active' => $style->id == $currentStyle->id,
                'linkOptions' => [
                        'class' => 'b-style-filter__item'
                ]
            ];
        }, $styles),
    ]); ?>
</div>

<div class="b-portfolio">
    <h5 class="b-portfolio__header"><?= $currentStyle->title_h1 ?: $currentStyle->title ?></h5>

    <?php ob_start(); ?>
    <?php $this->renderPartial('_search', ['model' => $model, 'baseCriteria' => $baseCriteria]); ?>
    <?php $filter = ob_get_clean(); ?>

    <?php $this->widget(
        'zii.widgets.CListView',
        [
            'id' => 'portfolio-list',
            'dataProvider' => $dataProvider,
            'itemView' => '_item',
            'template' => $filter . '{items}{pager}',
            'cssFile' => false,
            'ajaxType' => 'GET',
            'enableHistory' => false,
            'afterAjaxUpdate' => 'js:function(){window.retinajs();}',
            'itemsCssClass' => 'b-portfolio__list',
            'htmlOptions' => ['class' => 'g-mb50'],
            'pagerCssClass' => 'g-loader',
            'pager' => [
                'class' => 'application.components.LinkPager',
                'header' => false,
            ],
        ]
    ); ?>
</div>

<script>
    portfolioBaseUrl = '<?=$currentStyle->url?>';
    portfolioBaseState = {action: 'popup', modal: 'Portfolio[type]='};

    // Listen for history state changes
    window.addEventListener('popstate', function () {
        var state = history.state;
        // back button pressed. close popup
        if (!state) {
            location.reload();
        }
    });
</script>
<div class="g-loader hidden">
    <div class="g-loader__content"><a href="#" class="g-loader__button"><i class="g-loader__button-icon"></i><span class="g-loader__button-text">Загрузить ещё</span></a></div>
</div>
