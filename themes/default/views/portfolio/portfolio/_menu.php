<?php
/**
 * @var $this PortfolioController
 */
?>

<h1 class="b-page__title">В портфолио <?= Yii::t('PortfolioModule.portfolio', '{n} баннер|{n} баннера|{n} баннеров', [Portfolio::model()->published()->count()]); ?></h1>

<?php $this->widget('application.components.PortfolioMenu', [
    'itemCssClass' => 'b-filter__item',
    'activeCssClass' => 'b-filter__item_active',
    'htmlOptions' => ['class' => 'b-filter'],
    'linkLabelWrapper' => 'span',
    'linkLabelWrapperHtmlOptions' => ['class' => 'b-filter__title'],
    'items' => [
        [
            'label' => 'Последние',
            'url' => ['/portfolio/portfolio/index'],
            'active' => $this->action->id == 'index',
            'linkOptions' => ['class' => 'b-filter__item b-filter__item_with-arrow'],
        ],
        [
            'label' => 'Любимые',
            'url' => ['/portfolio/portfolio/favorite'],
            'active' => $this->action->id == 'favorite',
            'linkOptions' => ['class' => 'b-filter__item b-filter__item_with-arrow'],
        ],
        [
            'label' => 'По темам',
            'url' => ['/portfolio/portfolio/themes'],
            'active' => $this->action->id == 'theme',
            'linkOptions' => ['class' => 'b-filter__item b-filter__item_with-arrow'],
        ],
        [
            'label' => 'По размерам',
            'url' => ['/portfolio/portfolio/sizes'],
            'active' => $this->action->id == 'size',
            'linkOptions' => ['class' => 'b-filter__item b-filter__item_with-arrow'],
        ],
        [
            'label' => 'По стилям',
            'url' => ['/portfolio/portfolio/styles'],
            'active' => $this->action->id == 'style',
            'linkOptions' => ['class' => 'b-filter__item b-filter__item_with-arrow'],
        ],
    ],
]); ?>
