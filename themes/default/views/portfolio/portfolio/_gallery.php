<? /* @var $model Gallery */ ?>
<div class="b-gallery">
    <?php foreach ($model->images as $image): ?>
        <a href="<?= $image->getImageUrl() ?>" rel="gallery_<?= $model->id ?>" title="<?= $image->name ?>" class="b-gallery__item">
            <?= CHtml::image($image->getImageUrl(240, 160), $image->alt, ['class' => 'b-gallery__image', 'data-rjs' => '2']); ?>
        </a>
    <?php endforeach; ?>
</div>
