<?php
/**
 * @var $this PortfolioController
 * @var $model Portfolio
 */

$this->title = $model->seo_title ?: [$model->getTitle(), 'Портфолио', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = $model->seo_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = $model->seo_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>
<div <?= Yii::app()->request->isAjaxRequest ? 'style="width:1080px;padding:0 40px"' : 'class="container"' ?>>
    <h1 class="b-page__title"><?= CHtml::link($model->getType() . '-баннер', ['/portfolio/portfolio/index', '#' => '!' . $model->getTypeString()], ['class' => 'b-page__link']); ?> для <?= CHtml::link($model->client->title,
            $model->client->url, ['class' => 'b-page__link']); ?><br>«<?= $model->title ?>»</h1>

    <?php if (count($model->getLanguages()) > 1): ?>
        <div class="b-language-tabs">
            <ul role="tablist" class="b-language-tabs__list">
                <?php foreach ($model->getLanguages() as $key => $language): ?>
                    <li role="presentation" class="b-language-tabs__item<?= $language == reset($model->getLanguages()) ? ' active' : '' ?>">
                        <a href="#banner_language_<?= $key ?>" aria-controls="banner_language_<?= $key ?>" role="tab" data-toggle="tab" class="b-language-tabs__link">
                            <span class="b-language-tabs__title"><?= $language ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <div class="b-portfolio-item">
        <div class="tab-content">
            <?php foreach ($model->getLanguages() as $key => $language): ?>
                <div id="banner_language_<?= $key ?>" role="tabpanel" class="tab-pane<?= $language == reset($model->getLanguages()) ? ' active' : '' ?>">
                    <ul class="b-size-tabs list-unstyled">
                        <?php foreach (array_values(array_filter($model->items, function (PortfolioItem $item) use ($language) {
                            return $item->getLanguage() == $language;
                        })) as $itemKey => $item): ?>
                            <li role="presentation" class="b-size-tabs__item<?= $itemKey === 0 ? ' active' : '' ?>">
                                <?= CHtml::link($item->width . '<span class="b-size-tabs__divider">x</span>' . $item->height, '#' . $item->getId(),
                                    ['class' => 'b-size-tabs__link', 'data-toggle' => 'tab', 'role' => 'tab', 'aria-controls' => $item->getId()]); ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <div class="tab-content">
                        <?php foreach (array_values(array_filter($model->items, function (PortfolioItem $item) use ($language) {
                            return $item->getLanguage() == $language;
                        })) as $itemKey => $item): ?>
                            <div id="<?= $item->getId() ?>" role="tabpanel" class="tab-pane<?= $itemKey === 0 ? ' active' : '' ?>">
                                <div class="b-portfolio-item__view"<?= $model->pattern ? ' style="background-image:url(\'' . $model->patternUpload->getImageUrl(1080) . '\');"' : ''; ?> data-rjs="2">
                                    <div class="b-portfolio-item__scroll scrollbar-light">
                                        <?php if ($model->type == Portfolio::TYPE_HTML5): ?>
                                            <div class="b-iframe-scroll" style="width:<?= $item->width ?>px;height: <?= $item->height ?>px">
                                                <?= $item->getIframe() ?>
                                                <div class="b-iframe-scroll__fix"></div>
                                            </div>
                                        <?php else: ?>
                                            <?= CHtml::tag('span', ['class' => 'js-image', 'data-params' => json_encode(['src'=>$item->imageUpload->getImageUrl(), 'alt' => CHtml::encode($model->title),'style'=> "width:{$item->width}px;max-height: {$item->height}px"])]); ?>
                                            <!--                                        --><? //= CHtml::image($item->imageUpload->getImageUrl(), CHtml::encode($model->title), ['class' => 'b-portfolio-item__image img-responsive']); ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="b-portfolio-item__lead">
            <?php if ($model->theme): ?>
                <div class="b-portfolio-item__info b-portfolio-item__info_category">Сфера бизнеса: <?= CHtml::link($model->theme->title, $model->theme->url, ['class' => 'b-portfolio-item__link']); ?></div>
            <?php endif; ?>
            <?php if ($model->style): ?>
                <div class="b-portfolio-item__info b-portfolio-item__info_style">Стиль баннера: <?= CHtml::link($model->style->title, $model->style->url, ['class' => 'b-portfolio-item__link']); ?></div>
            <?php endif; ?>
            <div class="b-portfolio-item__info b-portfolio-item__info_date"><?= Yii::app()->dateFormatter->format('d MMMM yyyy', $model->date); ?></div>
            <div class="b-portfolio-item__info b-portfolio-item__info_share">
                <div class="b-portfolio-item__share">
                    <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                    <script src="//yastatic.net/share2/share.js"></script>
                    <div data-services="vkontakte,facebook,twitter" class="ya-share2"></div>
                </div>
            </div>
        </div>
        <div class="b-portfolio-item__text b-text">
            <?= $model->text; ?>
        </div>
        <div class="text-center">
            <?= CHtml::link('Заказать похожий баннер', ['/order/order/portfolio', 'id' => $model->id], ['class' => 'btn btn-success btn-lg b-portfolio-item__button']); ?>
        </div>
    </div>
</div>

<?php if ($model->theme): ?>
    <?php $dataProvider = Portfolio::model()->published()->theme($model->theme_id)->searchNotFor($model->id); ?>

    <?php if ($dataProvider->getTotalItemCount()): ?>
        <?php $this->widget(
            'zii.widgets.CListView',
            [
                'id' => 'portfolio-list-' . $model->id,
                'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'summaryTagName' => 'h5',
                'summaryCssClass' => 'b-portfolio__header',
                'summaryText' => 'Другие баннеры на тему «' . $model->theme->title . '»:',
                'template' => '{summary}{items}',
                'cssFile' => false,
                'enableHistory' => false,
                'itemsCssClass' => 'b-portfolio__list',
                'htmlOptions' => ['class' => 'b-portfolio g-mb80'],
            ]
        ); ?>
    <?php endif; ?>
<?php endif; ?>

<script>
    function showIframe() {
        $('.b-portfolio-item__view iframe').remove();
        $('.b-portfolio-item__view img').remove();

        $('.tab-content .tab-pane.active .tab-pane.active .js-iframe').each(function () {
            var $e = $(this);

            var $iframe = $('<iframe>')
                .attr('src', $e.data('src'))
                .attr('width', $e.data('width'))
                .attr('height', $e.data('height'))
                .attr('frameborder', '0')
                .attr('scrolling', 'no');

            $e.after($iframe);
        });

        $('.tab-content .tab-pane.active .tab-pane.active .js-image').each(function () {
            var $e = $(this);

            var $image = $('<img>')
                .attr($e.data('params'))
                .addClass('b-portfolio-item__image');
//                .addClass('img-responsive');

            $e.after($image);
        });
    }

    $('.b-language-tabs__link, .b-size-tabs__link').on('click', function () {
        var href = $(this).attr('href');

        history.replaceState(history.state, document.title, href);
        setTimeout(showIframe, 1);
    });

    setTimeout(function () {
        if (location.hash && location.hash.charAt(1) !== '!') {
            var hash = location.hash;
            var id = $('a[href=\"' + hash + '\"]').closest('.tab-pane').attr('id');

            if (id) {
                $('a[href=\"#' + id + '\"]').trigger('click');
            }

            $('a[href=\"' + hash + '\"]').trigger('click');
        }
    }, 1);

    <?php if (!Yii::app()->request->isAjaxRequest): ?>
    setTimeout(showIframe, 1);
    <?php endif; ?>

    <?php if (Yii::app()->request->isAjaxRequest): ?>
    setTimeout(window.retinajs, 1);

    $(".b-portfolio-item__scroll").scrollbar();

    if (!window.previousTitle) {
        window.previousTitle = document.title;
    }

    document.title = <?=CJavaScript::encode($model->seo_title ?: implode([$model->getTitle(), 'Портфолио', Yii::app()->getModule('yupe')->siteName], ' | '))?>;
    <?php endif; ?>

    <?php if (!Yii::app()->request->isAjaxRequest): ?>
    var state = {action: 'portfolioItem', path: "<?=$model->url?>"};
    // Change URL in browser
    history.replaceState(null, document.title, location.href);

    // Listen for history state changes
    window.addEventListener('popstate', function () {
        var state = history.state;
        // back button pressed. close popup
        if (state && state.action == 'popup') {
            // Forward button pressed, reopen popup
            console.log('state', state);

            location.reload();
        }
        if (!state) {
            location.reload();
        }
    });
    <?php endif;?>
</script>
