<?php
/**
 * @var $this OrderController
 * @var $model Order
 * @var $form TbActiveForm
 */
?>

<div id="order_form_block">
    <div class="container">
        <h1 class="b-page__title">Оформление заказа на разработку баннеров</h1>

        <p class="b-page__lead">Представьтесь, пожалуйста</p>
    </div>
    <div class="b-form">
        <div class="container">
            <?php $form = $this->beginWidget(
                'bootstrap.widgets.TbActiveForm',
                [
                    'id' => 'order-form-1',
                    'htmlOptions' => [
                        'class' => 'b-form__form',
                    ],
                ]
            ); ?>

            <?php echo $form->errorSummary($model); ?>

            <?= $form->textFieldGroup($model, 'name', ['widgetOptions' => ['htmlOptions' => ['placeholder' => false]]]); ?>
            <?= $form->emailFieldGroup($model, 'email', ['widgetOptions' => ['htmlOptions' => ['placeholder' => false]]]); ?>
            <?= $form->textFieldGroup($model, 'phone', ['widgetOptions' => ['htmlOptions' => ['placeholder' => false]]]); ?>

            <?php echo CHtml::submitButton('Далее', ['id' => 'submit_button_' . $form->id, 'class' => 'btn btn-info btn-lg b-form__button', 'data-loading-text' => 'Далее']); ?>

            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
