<?php
/**
 * @var $this OrderController
 * @var $model Order
 * @var $fileModels OrderFile[]|null
 * @var $form TbActiveForm
 */

//Yii::app()->clientScript->scriptMap['jquery.js'] = $this->mainAssets . '/assets/vendor/jquery/dist/jquery.min.js';
?>

<div id="order_form_block">
    <div class="container">
        <h1 class="b-page__title">Оформление заказа на разработку баннеров</h1>

        <p class="b-page__lead">Опишите проект. Чем больше информации, тем лучше результат.</p>
    </div>
    <div class="b-form">
        <div class="container">
            <?php $form = $this->beginWidget(
                'application.components.OrderActiveForm',
                [
                    'id' => 'order-form',
                    'hintTag' => 'p',
                    'htmlOptions' => [
                        'class' => 'b-form__form',
                        'enctype' => 'multipart/form-data',
                    ],
                ]
            ); ?>

            <?php $this->widget('\yupe\widgets\YFlashMessages', ['options' => ['closeText' => false]]); ?>

            <?php echo $form->errorSummary(CMap::mergeArray($fileModels, [$model])); ?>

            <?php echo $form->textFieldGroup($model, 'text_1', ['widgetOptions' => ['htmlOptions' => ['placeholder' => false]], 'hint' => 'Например: tolkunov.com, предприниматели, маркетологи. мужчины и женщины от 18 до 45 лет']); ?>
            <?php echo $form->textFieldGroup($model, 'text_2', ['widgetOptions' => ['htmlOptions' => ['placeholder' => false]], 'hint' => 'Например: html5 - 240x400, 300x250, 728x90 или flash 160x600, 240x400 и т.д.']); ?>
            <?php echo $form->textAreaGroup($model, 'text_3', [
                'widgetOptions' => ['htmlOptions' => ['placeholder' => false, 'rows' => 7, 'class' => 'resizable-y']],
                'hint' => 'Какие фразы лучше всего использовать? Пожелания по сценарию? Пожелания по графике? Обязательные элементы в баннере?',
            ]); ?>
            <div class="form-group">
                <label>Файлы</label>

                <p class="help-block">Логотип, брендбук, фотографии - всё, что может пригодиться в разработке баннеров</p>

                <div class="b-form-files"></div>

                <div class="b-form-upload b-form__upload">
                    <?php echo CHtml::fileField('Order[files][]', '', ['multiple' => true, 'id' => 'Order_files_0', 'data-id' => "Order_files"]); ?>
                    <span class="b-form-upload__label">Выберите файлы или перетащите их в эту форму</span>
                </div>
            </div>
            <?php
            $styleString = '';
            $styles = PortfolioStyle::model()->findAll();

            foreach ($styles as $key => $style) {
                $styleString .= CHtml::link($style->title, $style->url, ['target' => '_blank']) . ($key == count($styles) - 2 ? ' или ' : ($key !== count($styles) - 1 ? ', ' : ''));
            }

            ?>
            <?php echo $form->textFieldGroup($model, 'text_4', ['widgetOptions' => ['htmlOptions' => ['placeholder' => false]], 'hint' => $styleString . '?']); ?>
            <?php echo $form->textFieldGroup($model, 'text_5', ['widgetOptions' => ['htmlOptions' => ['placeholder' => false]], 'hint' => 'Рекламная площадка и конкретное место размещения? Ссылка на технические требования к баннерам?']); ?>
            <?php echo $form->textFieldGroup($model, 'text_6',
                ['widgetOptions' => ['htmlOptions' => ['placeholder' => false]], 'hint' => 'Счет Альфа-банка, карта Сбербанка, Яндекс.Деньги, QIWI, WebMoney, расчетный счет ИП или PayPal?']); ?>

            <div class="form-group">
                <div class="b-checkbox">
                    <?= $form->checkBox($model, 'agree'); ?>
                    <?= $form->labelEx($model, 'agree'); ?>
                    <?= $form->error($model, 'agree'); ?>
                </div>
            </div>

            <?php echo CHtml::submitButton('Отправить заявку', ['id' => 'submit_button_' . $form->id, 'class' => 'btn btn-info btn-lg b-form__button', 'data-loading-text' => 'Отправить заявку']); ?>

            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
