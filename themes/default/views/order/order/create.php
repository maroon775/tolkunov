<?php
/**
 * @var $this OrderController
 * @var $model Order
 */

$this->title = ['Оформление заказа на разработку баннеров', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = Yii::app()->getModule('yupe')->siteKeyWords;

Yii::app()->clientScript->scriptMap['jquery.js'] = $this->mainAssets . '/assets/vendor/jquery/dist/jquery.min.js';
Yii::app()->clientScript->scriptMap['jquery.min.js'] = $this->mainAssets . '/assets/vendor/jquery/dist/jquery.min.js';
Yii::app()->clientScript->registerPackage('jquery');
?>

<?php $this->renderPartial($model->scenario == 'step_1' ? '_form_1' : '_form_2', ['model' => $model, 'fileModels' => !empty($fileModels) ? $fileModels : []]); ?>

<script>
    <?php ob_start();?>
    jQuery('body').on('click', '#submit_button_order-form-1', function () {
        var fd = new FormData();

        fd.append(yupeTokenName, yupeToken);
        fd.append('Order[name]', $('#Order_name').val());
        fd.append('Order[email]', $('#Order_email').val());
        fd.append('Order[phone]', $('#Order_phone').val());

        jQuery.ajax({
            'beforeSend': function () {
                $("#submit_button_order-form-1").button("loading");
            },
            'url': '<?=Yii::app()->createUrl('/order/order/create')?>',
            type: 'POST',
            cache: false,
            data: fd,
            datatype: 'json',
            processData: false,
            contentType: false,
            'success': function (html) {
                jQuery("#order_form_block").replaceWith(html)
            },
            error: function () {
                $("#submit_button_order-form-1").button("reset");
                alert("Произошла непредвиденная ошибка");
            }
        });

        return false;
    });

    jQuery('body').on('click', '#submit_button_order-form', function () {
        var fd = new FormData();

        $('.b-form-upload').each(function () {
            var j = 0;

            $(this).find('input[type=file]').each(function () {
                var files = this.files;

                if (files.length) {
                    for (var i = 0; i < files.length; i++) {
                        if (!$(this).closest('.b-form-upload').prev().find('.b-form-files__item').eq(j).hasClass('hidden')) {
                            fd.append("Order[files][]", files[i]);
                        }

                        j++;
                    }
                }
            });
        });

        fd.append(yupeTokenName, yupeToken);
        fd.append('Order[text_1]', $('#Order_text_1').val());
        fd.append('Order[text_2]', $('#Order_text_2').val());
        fd.append('Order[text_3]', $('#Order_text_3').val());
        fd.append('Order[text_4]', $('#Order_text_4').val());
        fd.append('Order[text_5]', $('#Order_text_5').val());
        fd.append('Order[text_6]', $('#Order_text_6').val());
        fd.append('Order[agree]', $('#Order_agree').is(':checked') ? 1 : 0);

        jQuery.ajax({
            'beforeSend': function () {
                $("#submit_button_order-form").button("loading");
            },
            'url': '<?=Yii::app()->createUrl('/order/order/create')?>',
            type: 'POST',
            cache: false,
            data: fd,
            datatype: 'json',
            processData: false,
            contentType: false,
            'success': function (html) {
                jQuery("#order_form_block").replaceWith(html)
            },
            error: function () {
                $("#submit_button_order-form").button("reset");
                alert("Произошла непредвиденная ошибка");
            }
        });

        return false;
    });
    <?php $vacancyFormScript = ob_get_clean(); ?>
    <?php Yii::app()->clientScript->registerScript('order-form-script', $vacancyFormScript, CClientScript::POS_READY); ?>
</script>
