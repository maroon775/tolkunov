<?php
/**
 * @var $this ClientController
 * @var $model Client
 * @var $portfolioDataProvider CActiveDataProvider
 * @var $reviewsDataProvider CActiveDataProvider
 */

$this->title = $model->seo_title ?: [$model->title, 'Клиенты', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = $model->seo_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = $model->seo_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>
<div class="container">
    <div class="text-center">
        <?= CHtml::link('Посмотреть всех клиентов', ['/client/client/index'], ['class' => 'b-page__back']); ?>
    </div>

    <h1 class="b-page__title"><?= $model->title ?></h1>

    <div class="b-client">
        <div class="b-client__head">
            <?php if ($model->image): ?>
                <?= CHtml::image($model->imageUpload->getImageUrl(null, 40, false), CHtml::encode($model->title), ['class' => 'b-client__image', 'data-rjs' => '2']); ?>
            <?php endif; ?>
            <?php if (!empty($model->link)): ?>
                <?= CHtml::link($model->getLinkText(), $model->getLinkUrl(), ['class' => 'b-client__link', 'target' => '_blank', 'rel' => 'nofollow']); ?>
            <?php endif; ?>
        </div>

        <div class="b-client__text b-text">
            <?= $model->text; ?>
        </div>
    </div>
</div>

<div class="b-language-tabs<?= $model->reviewsCount == 0 ? ' hidden' : '' ?>">
    <ul role="tablist" class="b-language-tabs__list">
        <li role="presentation" class="b-language-tabs__item active"><a href="#portfolio" aria-controls="portfolio" role="tab" data-toggle="tab" class="b-language-tabs__link"><span class="b-language-tabs__title">Работы</span></a></li>
        <li role="presentation" class="b-language-tabs__item"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab" class="b-language-tabs__link"><span class="b-language-tabs__title">Отзывы</span></a></li>
    </ul>
</div>

<div class="tab-content">
    <div id="portfolio" class="tab-pane active" role="tabpanel">
        <?php $this->widget(
            'zii.widgets.CListView',
            [
                'id' => 'portfolio-list',
                'dataProvider' => $portfolioDataProvider,
                'itemView' => '../../portfolio/portfolio/_item',
                'template' => '{items}',
                'cssFile' => false,
                'enableHistory' => false,
                'itemsCssClass' => 'b-portfolio__list',
                'htmlOptions' => ['class' => 'b-portfolio g-mb60 g-mt30'],
            ]
        ); ?>
    </div>
    <div id="reviews" class="tab-pane" role="tabpanel">
        <?php $this->widget(
            'zii.widgets.CListView',
            [
                'id' => 'review-list',
                'dataProvider' => $reviewsDataProvider,
                'itemView' => '../../review/review/_item',
                'template' => '{items}',
                'cssFile' => false,
                'enableHistory' => false,
                'itemsCssClass' => 'b-reviews__list',
                'htmlOptions' => ['class' => 'b-reviews__container g-pb60'],
            ]
        ); ?>
    </div>
</div>
<script>
    $('.b-portfolio__item').each(function () {
        $(this).data('hold', Math.floor(Math.random() * 400) + 50);
    });
    $('.b-reviews__container').css('visibility', 'hidden');
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('.b-reviews__container').css('visibility', 'hidden');
        loadReviews(function () {
            $('.b-reviews__container').css('visibility', 'inherit');
        });
    });
</script>
