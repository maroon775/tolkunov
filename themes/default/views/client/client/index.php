<?php
/**
 * Отображение для client/index
 *
 * @var $this ClientController
 * @var $dataProvider CActiveDataProvider
 **/

$this->title = ['Клиенты', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = Yii::app()->getModule('yupe')->siteKeyWords;

$this->breadcrumbs = [Yii::t('ClientModule.client', 'Клиенты')];
?>

<div class="container">
    <h1 class="b-page__title">Клиенты</h1>
</div>
<div class="b-clients">
    <?php $this->widget(
        'zii.widgets.CListView',
        [
            'dataProvider' => $dataProvider,
            'itemView' => '_item',
            'template' => '{items}{pager}',
            'cssFile' => false,
            'itemsCssClass' => 'b-clients__list',
            'htmlOptions' => ['class' => 'container-fluid'],
            'enableHistory' => false,
            'afterAjaxUpdate' => 'js:function(){window.retinajs();}',
            'pagerCssClass' => 'g-loader g-loader_theme_dark',
            'pager' => [
                'class' => 'application.components.LinkPager',
                'header' => false,
            ],
        ]
    ); ?>
</div>
