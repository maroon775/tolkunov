<?php
/**
 * @var $this LastReviewWidget
 * @var $models Review[]
 */
?>
<div class="b-reviews__container">
    <h2 class="b-reviews__head">Отзывы клиентов</h2>

    <p class="b-reviews__lead">Возможно они расскажут о нашей работе что-то более важное и близкое вам</p>

    <?php $this->widget(
        'zii.widgets.CListView',
        [
            'dataProvider' => new CArrayDataProvider($models, ['pagination' => false]),
            'itemView' => '../../review/_item',
            'template' => '{items}',
            'cssFile' => false,
            'afterAjaxUpdate' => 'js:function(){loadReviews();}',
            'itemsCssClass' => 'b-reviews__list',
            'htmlOptions' => [
//                'class' => 'b-reviews__container',
                'style' => 'max-height:1000px;overflow:hidden;',
            ],
//        'pager' => [
//            'class' => 'application.components.LinkPager',
//            'header' => false,
//            'htmlOptions' => ['class' => 'b-filter list-unstyled'],
//            'firstPageLabel' => false,
//            'lastPageLabel' => false,
//            'prevPageLabel' => false,
//            'nextPageLabel' => false,
//        ],
        ]
    ); ?>


    <div class="g-loader g-loader_theme_dark">
        <div class="g-loader__content">
            <?= CHtml::link(
                Yii::t('ReviewModule.review', 'Посмотреть {n} отзыв|Посмотреть все {n} отзыва|Посмотреть все {n} отзывов', [Review::model()->published()->count()]),
                ['/review/review/index'],
                ['class' => 'btn btn-info btn-lg g-loader__link']
            ); ?>
        </div>
    </div>
</div>

<div class="hidden">
    <?php echo $this->controller->clips['modals']; ?>
</div>
