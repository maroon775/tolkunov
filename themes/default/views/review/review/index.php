<?php
/**
 * Отображение для review/index
 *
 * @var $this ReviewController
 * @var $dataProvider CActiveDataProvider
 * @var $model Review
 **/

$this->title = ['Отзывы клиентов', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = Yii::app()->getModule('yupe')->siteKeyWords;

$this->breadcrumbs = [Yii::t('ReviewModule.review', 'Отзывы клиентов')];

Yii::app()->clientScript->registerScript('search', "
    $(function(){
    if (location.hash) {
        var link = $('a[href=\"' + location.hash + '\"]');

        var typeId = $(link).data('type-id');
        var href = $(link).attr('href');
        var data = 'Review[type]=' + typeId;

        state = {action: 'popup', modal: data};

        $.fn.yiiListView.update('review-list', {
            data: data
        });

        // Change URL in browser
        history.replaceState(state, document.title, href);
    } else {
        history.replaceState({action: 'popup', modal: 'Review[type]='}, document.title);
    }
    });

    $(document).on('click', '.b-reviews-filter__item', function () {
        var typeId = $(this).data('type-id');
        var href = $(this).attr('href');
        var data = 'Review[type]=' + typeId;

        state = {action: 'popup', modal: data};

        console.log('update list');
        $.fn.yiiListView.update('review-list', {
            data: data
        });

        // Change URL in browser
        history.pushState(state, document.title, href);
        return false;
    });

    // Listen for history state changes
    window.addEventListener('popstate', function (e) {
        var state = history.state;
        // back button pressed. close popup
        if (state) {
            // Forward button pressed, reopen popup
            console.log('update list');
            $.fn.yiiListView.update('review-list', {
                data: state.modal
            });
        }
    });
");
?>
<?php ob_start(); ?>
<div class="container">
    <h1 class="b-page__title">Отзывы клиентов</h1>

    <div class="b-reviews">
        <?php $this->renderPartial('_search', ['model' => $model,]); ?>
    </div>
</div>
<?php $filter = ob_get_clean(); ?>

<?php $this->widget(
    'zii.widgets.CListView',
    [
        'id' => 'review-list',
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'template' => $filter . '<div class="b-reviews__container g-pb40">{items}{pager}</div>',
        'cssFile' => false,
        'ajaxType' => 'GET',
        'enableHistory' => false,
        'beforeAjaxUpdate' => 'js:function(){scrollTop = $(window).scrollTop()}',
        'afterAjaxUpdate' => 'js:function(){loadReviews(fixReviewsHeight);window.retinajs();}',
        'itemsCssClass' => 'b-reviews__list',
        'htmlOptions' => ['class' => ''],
        'pagerCssClass' => 'g-loader g-loader_theme_dark',
        'pager' => [
            'class' => 'application.components.LinkPager',
            'header' => false,
        ],
    ]
); ?>

<script>
    function fixReviewsHeight() {
        setTimeout(function () {
            var $loader = $('.g-loader');
            var $button = $('.g-loader__content');
            var $content = $('.b-reviews__list');

            if ($loader.length && !$button.hasClass('hidden')) {
                console.log($content.height());
                $content.height($content.height() - 500);
                $content.css('overflow', 'hidden');
            }

            if (scrollTop) {
                $(window).scrollTop(scrollTop);
            }
        }, 1);
    }

    $(function () {
        setTimeout(fixReviewsHeight, 300);
    });
</script>

<!--<div class="hidden">-->
<!--    --><?php //echo $this->clips['modals']; ?>
<!--</div>-->
