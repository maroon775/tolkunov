<?php
/**
 * @var $data Review
 * @var $this CListView
 */
?>
<div class="b-reviews__header">
    <?= CHtml::link($data->getType(), (get_class($this) === 'application\controllers\SiteController') ? ['/review/review/index', '#' => '!' . $data->getTypeString()] : null, ['class' => 'b-reviews__type']); ?>
</div>
<div class="text-center"><?= CHtml::link($data->client->title, $data->client->url, ['class' => 'b-reviews__title']); ?></div>
<div class="b-reviews__content b-reviews__content_type_image"<?= $data->pattern ? ' style="background-image:url(\'' . $data->patternUpload->getImageUrl(320, 330) . '\');"' : ''; ?> data-rjs="2">
    <div class="b-review b-review_image">
        <?= CHtml::link(CHtml::image($data->getImageUrl(196, 264), CHtml::encode($data->client->title), ['class' => 'img-responsive', 'data-rjs' => '2']), '#review_' . $data->id,
            ['class' => 'b-review__link', 'data-url' => $data->getImageUrl()]); ?>
    </div>
</div>
<div class="b-reviews__footer">
    <span class="b-reviews__date"><?= Yii::app()->dateFormatter->format('d MMMM yyyy', $data->date); ?></span>
    <?php if ($data->link): ?>
        <?= CHtml::link('Оригинал отзыва', $data->link, ['class' => 'b-reviews__link', 'target' => '_blank', 'rel' => 'nofollow']); ?>
    <?php endif; ?>
</div>

<?php //$this->beginClip('review_' . $data->id); ?>
<div class="hidden">
    <div id="review_<?= $data->id ?>">
        <div class="b-review-image"<?= $data->pattern ? ' style="background-image:url(\'' . $data->patternUpload->getImageUrl(900, 1040) . '\');"' : ''; ?> data-rjs="2">
            <div class="b-review-image__container"><?= CHtml::image($data->getImageUrl(592, 793), CHtml::encode($data->client->title), ['data-rjs' => '2']); ?></div>
        </div>
    </div>
</div>
<?php //$this->endClip(); ?>

<?php //$this->clips['modals'] .= $this->clips['review_' . $data->id]; ?>

