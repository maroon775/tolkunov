<?php
/**
 * @var $this LastPostsWidget
 * @var $models Post[]
 */
?>

<?php $this->widget(
    'zii.widgets.CListView',
    [
        'id' => 'posts-list',
        'dataProvider' => new CArrayDataProvider($models, ['pagination' => false]),
        'itemView' => '../../post/_item',
        'template' => '{summary}{items}',
        'cssFile' => false,
        'itemsCssClass' => 'b-blog__list',
        'htmlOptions' => [
            'class' => 'b-blog g-mb50',
            'style' => 'max-height:1000px;overflow:hidden;',
        ],
        'summaryTagName' => 'h2',
        'summaryCssClass' => 'b-blog__header',
        'summaryText' => 'Справочник клиента',
    ]
); ?>
<script type="text/javascript">
    _hcwp = window._hcwp || [];
    _hcwp.push({widget: "Bloggerstream", widget_id: "85763", selector: '.b-blog__comments', label: 'Комментарии: {%COUNT%}'});
    (function () {
        if ("HC_LOAD_INIT" in window)return;
        HC_LOAD_INIT = true;
        var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
        var hcc = document.createElement("script");
        hcc.type = "text/javascript";
        hcc.async = true;
        hcc.src = ("https:" == document.location.protocol ? "https" : "http") + "://w.hypercomments.com/widget/hc/85763/" + lang + "/widget.js";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hcc, s.nextSibling);
    })();
</script>
<div class="g-loader">
    <div class="g-loader__content">
        <?= CHtml::link(
            Yii::t('BlogModule.blog', 'Почитать {n} статью|Почитать все {n} статьи|Почитать все {n} статей', [Post::model()->published()->public()->count()]),
            ['/blog/post/index'],
            ['class' => 'btn btn-success btn-lg g-loader__link']
        ); ?>
    </div>
</div>
