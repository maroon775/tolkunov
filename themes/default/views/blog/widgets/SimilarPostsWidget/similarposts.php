<?php
/**
 * @var $posts Post[]
 * @var $this SimilarPostsWidget
 */
?>
<?php Yii::import('application.modules.blog.BlogModule'); ?>

<?php if (count($posts)): ?>
    <div class="b-blog">
        <div class="b-blog__list">
            <?php foreach ($posts as $post): ?>
                <?php $this->controller->renderPartial('//blog/post/_item', ['data' => $post]); ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php else: ?>
    <?php $this->controller->widget(
        'blog.widgets.LastPostsOfBlogWidget',
        ['blogId' => $this->post->blog_id, 'postId' => $this->post->id, 'view' => 'in-post']
    ); ?>
<?php endif; ?>
