<?php
/**
 * @var $models Blog[]
 */
?>
<div class="b-blog-menu">
    <div class="b-blog-menu__list">
        <div class="b-blog-menu__item b-blog-menu__item_home<?= empty($this->current) ? ' b-blog-menu__item_active' : '' ?>">
            <?= CHtml::link('<i class="b-blog-menu__icon b-blog-menu__icon_home"></i>', ['/blog/post/index'], ['class' => 'b-blog-menu__link']); ?>
        </div>
        <?php foreach ($models as $model): ?>
            <div class="b-blog-menu__item<?= (isset($this->current) && $model->id == $this->current->id) || (isset($this->current) && $model->hasDescendant($this->current)) ? ' b-blog-menu__item_active' : '' ?>">
                <?= CHtml::link($model->name, $model->url, ['class' => 'b-blog-menu__link']); ?>

                <?php if ($model->children): ?>
                    <div class="b-blog-menu__list">
                        <?php foreach ($model->children as $child): ?>
                            <div class="b-blog-menu__item<?= $child->id == $this->current->id ? ' b-blog-menu__item_active' : '' ?>">
                                <?= CHtml::link($child->name, $child->url, ['class' => 'b-blog-menu__link']); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
        <div class="b-blog-menu__item b-blog-menu__item_search"><a href="#" class="b-blog-menu__link"><i class="b-blog-menu__icon b-blog-menu__icon_search"></i></a>

            <form class="b-search-form hidden" action="<?= Yii::app()->createUrl('/blog/post/search'); ?>">
                <input type="text" name="q" placeholder="Введите что-нибудь и нажмите “Enter”" class="b-search-form__input"><a href="#" class="b-search-form__close"></a>
            </form>
        </div>
    </div>
</div>
