<?php Yii::import('application.modules.blog.BlogModule'); ?>

<div class="b-blog">
    <div class="b-blog__list">
        <?php foreach ($posts as $post): ?>
            <?php $this->controller->renderPartial('//blog/post/_item', ['data' => $post]); ?>
        <?php endforeach; ?>
    </div>
</div>
