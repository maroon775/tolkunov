<?php
/**
 * @var $data Post
 */
?>
<a href="<?= $data->url ?>" data-effect="relax" data-origin="bottom" data-expose="true" class="b-blog__item">
    <p class="b-blog__title"><?= CHtml::encode($data->getTitle()) ?></p>

    <div class="b-blog__content">
        <?= CHtml::image(
            $data->getImageUrl(288, 160),
            CHtml::encode($data->getTitle()),
            ['class' => 'img-responsive b-blog__image', 'data-rjs' => '2']
        ); ?>
    </div>
    <div class="b-blog__text">
        <?= $data->getQuote(); ?>
    </div>
    <div class="row text-muted">
        <div class="col-xs-6"><span><?= Yii::app()->dateFormatter->format('d MMMM yyyy', $data->publish_time); ?></span></div>
        <div class="col-xs-6 text-right"><span class="b-blog__comments" data-xid="<?= $data->id ?>"></span></div>
    </div>
</a>
