<?php
/**
 * @var $this PostController
 * @var $post Post
 */

$this->title = [$post->title, 'Справочник клиента', Yii::app()->getModule('yupe')->siteName];
$this->description = !empty($post->description) ? $post->description : strip_tags($post->getQuote());
$this->keywords = !empty($post->keywords) ? $post->keywords : implode(', ', $post->getTags());

$this->breadcrumbs = [
    Yii::t('BlogModule.blog', 'Blogs') => ['/blog/blog/index/'],
    CHtml::encode($post->blog->name) => ['/blog/blog/view', 'slug' => $post->blog->slug],
    $post->title,
];
?>

<div class="container">
    <div class="text-center hidden-lg hidden-xl">
        <?= CHtml::link('Посмотреть все статьи', ['/blog/blog/index'], ['class' => 'b-page__back']); ?>
    </div>
    <div class="visible-lg visible-xl">
        <h2 class="b-page__title">Справочник клиента</h2>
    </div>

    <?php $this->widget('application.modules.blog.widgets.BlogsWidget', ['current' => $post->blog]); ?>

    <div class="b-blog-item">
        <h1 class="b-blog-item__title"><?= CHtml::encode($post->title); ?></h1>

        <div class="b-blog-item__lead">
            <div class="b-blog-item__info b-blog-item__info_date"><?= Yii::app()->dateFormatter->format('d MMMM yyyy', $post->publish_time); ?></div>
            <div class="b-blog-item__info b-blog-item__info_comments b-blog__comments" data-xid="<?= $post->id ?>"></div>
            <div class="b-blog-item__info b-blog-item__info_views"><?= Yii::t('BlogModule.blog', '{n} просмотр|{n} просмотра|{n} просмотров', [$post->views]); ?></div>
            <div class="b-blog-item__info b-blog-item__info_category"><span class="hidden-xs">Категория: </span><?= CHtml::link($post->blog->name, $post->blog->url, ['class' => 'b-blog-item__link']); ?></div>
            <div class="b-blog-item__info b-blog-item__info_share">
                <div class="b-blog-item__share">
                    <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                    <script src="//yastatic.net/share2/share.js"></script>
                    <div data-services="vkontakte,facebook,twitter" class="ya-share2"></div>
                </div>
            </div>
        </div>

        <div class="b-blog-item__text b-text">
            <?= $post->content; ?>
        </div>

        <?php $this->widget('blog.widgets.PostMetaWidget', ['post' => $post]); ?>
    </div>
</div>

<?php $this->widget('blog.widgets.SimilarPostsWidget', ['post' => $post]); ?>

<div class="container">
    <div class="b-blog-item__comments">
        <div id="hypercomments_widget"></div>
        <script type="text/javascript">
            _hcwp = window._hcwp || [];
            _hcwp.push({widget: "Stream", widget_id: 85763, xid: "<?=$post->id?>"});
            (function () {
                if ("HC_LOAD_INIT" in window)return;
                HC_LOAD_INIT = true;
                var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
                var hcc = document.createElement("script");
                hcc.type = "text/javascript";
                hcc.async = true;
                hcc.src = ("https:" == document.location.protocol ? "https" : "http") + "://w.hypercomments.com/widget/hc/85763/" + lang + "/widget.js";
                var s = document.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(hcc, s.nextSibling);
            })();
        </script>
        <a href="http://hypercomments.com" class="hc-link" title="comments widget">comments powered by HyperComments</a>
    </div>
    <?php $this->widget('application.modules.feedback.widgets.SubscribeFormWidget', ['view' => 'subscribe_blog']); ?>
</div>

<script type="text/javascript">
    _hcwp = window._hcwp || [];
    _hcwp.push({widget: "Bloggerstream", widget_id: "85763", selector: '.b-blog__comments', label: 'Комментарии: {%COUNT%}'});
    (function () {
        if ("HC_LOAD_INIT" in window)return;
        HC_LOAD_INIT = true;
        var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
        var hcc = document.createElement("script");
        hcc.type = "text/javascript";
        hcc.async = true;
        hcc.src = ("https:" == document.location.protocol ? "https" : "http") + "://w.hypercomments.com/widget/hc/85763/" + lang + "/widget.js";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hcc, s.nextSibling);
    })();
</script>

<!--    --><?php //$this->widget('application.modules.comment.widgets.CommentsWidget', [
//        'redirectTo' => Yii::app()->createUrl('/blog/post/view', ['slug' => $post->slug]),
//        'model' => $post,
//    ]); ?>
<!--<script type="text/javascript">-->
<!--    $(document).ready(function () {-->
<!--        $('#post img').addClass('img-responsive');-->
<!--        $('pre').each(function (i, block) {-->
<!--            hljs.highlightBlock(block);-->
<!--        });-->
<!--    });-->
<!--</script>-->
<!---->
<?php //$this->widget('application.modules.image.widgets.colorbox.ColorBoxWidget', [
//    'targets' => [
//        '#post img' => [
//            'maxWidth' => 1200,
//            'maxHeight' => 800,
//            'href' => new CJavaScriptExpression("js:function(){
//                    return $(this).prop('src');
//                }"),
//        ],
//    ],
//]); ?>
