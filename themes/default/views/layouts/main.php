<?php $this->beginContent('//layouts/common'); ?>

<!-- flashMessages -->
<?php //$this->widget('yupe\widgets\YFlashMessages'); ?>
<!--<!-- breadcrumbs -->
<?php //$this->widget(
//    'bootstrap.widgets.TbBreadcrumbs',
//    [
//        'links' => $this->breadcrumbs,
//    ]
//);?>


<body>
<!--[if lte IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser.
    Please <a target="_blank" href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</p>
<!--<![endif]-->

<nav class="navbar navbar-default b-navbar">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display-->
        <div class="navbar-header">
            <button type="button" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" class="navbar-toggle collapsed">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?= CHtml::link('<div class="b-header__logo"></div>', Yii::app()->hasModule('homepage') ? ['/homepage/hp/index'] : ['/site/index'], ['class' => 'navbar-brand']); ?>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling-->
        <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?= CHtml::link('<div class="b-header__logo"></div>', Yii::app()->hasModule('homepage') ? ['/homepage/hp/index'] : ['/site/index'], ['class' => 'navbar-brand']); ?>
                </div>
                <?php if (Yii::app()->hasModule('menu')): ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu']); ?>
                <?php endif; ?>
                <?= CHtml::link('Заказать баннер', ['/order/order/create'], ['class' => 'btn btn-success btn-lg navbar-btn navbar-right']); ?>
            </div>
        </div>
    </div>
</nav>

<div class="b-page">
    <?php echo $content; ?>
</div>
<div class="b-footer">
    <div class="container">
        <div class="row b-footer__row">
            <div class="b-footer__column b-footer__column_1">
                <div class="row">
                    <div class="col-xs-12 col-md-3 col-lg-12">
                        <?= CHtml::link('', Yii::app()->hasModule('homepage') ? ['/homepage/hp/index'] : ['/site/index'], ['class' => 'b-footer__logo']); ?>
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-12">
                        <div class="b-social-links">
                            <div class="b-social-links__item"><a target="_blank" href="https://vk.com/tolkunov_com" class="b-social-links__link b-social-links__link_vk"></a></div>
                            <div class="b-social-links__item"><a target="_blank" href="https://www.facebook.com/tolkunov/" class="b-social-links__link b-social-links__link_fb"></a></div>
                            <div class="b-social-links__item"><a target="_blank" href="https://twitter.com/tolkunov_com" class="b-social-links__link b-social-links__link_tw"></a></div>
                            <div class="b-social-links__item"><a target="_blank" href="https://www.instagram.com/tolkunov_com/" class="b-social-links__link b-social-links__link_instagram"></a></div>
                            <div class="b-social-links__item"><a target="_blank" href="https://www.youtube.com/channel/UCwm8I4uQjaq5ycfSJZTuxrg" class="b-social-links__link b-social-links__link_youtube"></a></div>
                            <div class="b-social-links__item"><a target="_blank" href="https://spark.ru/startup/tolkunov-com" class="b-social-links__link b-social-links__link_spark"></a></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-5 col-lg-12">
                        <div class="b-footer__copyright clearfix">© Студия Толкунова Дмитрия,<br>2010‐2017</div>
                    </div>
                </div>
            </div>
            <div class="b-footer__column b-footer__column_2">
                <div class="b-footer-menu">
                    <p class="b-footer-menu__header"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "menyu-v-futere-1-zagolovok"]); ?></p>

                    <?php if (Yii::app()->hasModule('menu')): ?>
                        <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'footer-menu-1', 'layout' => 'footer_menu_1']); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="b-footer__column b-footer__column_3">
                <div class="b-footer-menu">
                    <p class="b-footer-menu__header"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "menyu-v-futere-2-zagolovok"]); ?></p>

                    <?php if (Yii::app()->hasModule('menu')): ?>
                        <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'footer-menu-2', 'layout' => 'footer_menu_2']); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (Yii::app()->hasModule('contentblock')): ?>
    <?php $this->widget(
        "application.modules.contentblock.widgets.ContentBlockWidget",
        ["code" => "STAT", "silent" => true]
    ); ?>
<?php endif; ?>
<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END); ?>
</body>
<?php $this->endContent(); ?>
