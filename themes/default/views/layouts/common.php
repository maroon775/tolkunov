<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START); ?>
    <?php Yii::app()->getController()->widget(
        'vendor.chemezov.yii-seo.widgets.SeoHead',
        [
            'httpEquivs' => [
                'Content-Type' => 'text/html; charset=utf-8',
                'X-UA-Compatible' => 'IE=edge,chrome=1',
                'Content-Language' => 'ru-RU',
            ],
            'defaultTitle' => $this->yupe->siteName,
            'defaultDescription' => $this->yupe->siteDescription,
            'defaultKeywords' => $this->yupe->siteKeyWords,
        ]
    ); ?>
    <?php Yii::app()->getClientScript()->registerMetaTag('initial-scale=1.0, width=device-width', 'viewport'); ?>

    <?php
    Yii::app()->getClientScript()->registerCssFile('https://fonts.googleapis.com/css?family=PT+Sans+Narrow');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/assets/fonts/stylesheet.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/assets/css/bootstrap.min.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/assets/vendor/slick-carousel/slick/slick.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/assets/vendor/slick-carousel/slick/slick-theme.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/assets/vendor/fancybox/source/jquery.fancybox.css?v=2.1.5', 'screen');
    //    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/assets/vendor/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=2.1.5', 'screen');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/assets/vendor/odometer/themes/odometer-theme-minimal.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/assets/vendor/jquery.scrollbar/jquery.scrollbar.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/assets/css/common.min.css');
    //    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/assets/vendor/jquery/dist/jquery.min.js');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/assets/vendor/bootstrap/dist/js/bootstrap.min.js');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/assets/vendor/masonry/dist/masonry.pkgd.min.js');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/assets/vendor/slick-carousel/slick/slick.min.js');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/assets/vendor/retina.js/dist/retina.min.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/assets/vendor/fancybox/source/jquery.fancybox.pack.js?v=2.1.5');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/assets/vendor/fancybox/source/helpers/jquery.fancybox-media.js');
    //    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/assets/vendor/fancybox/source/helpers/jquery.fancybox-thumbs.js');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js');
    //    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/assets/vendor/odometer/odometer.min.js');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/assets/js/script.js');
    ?>
    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';

        $([
            '<?=$this->mainAssets?>/assets/img/logo-blue.svg',
            '<?=$this->mainAssets?>/assets/img/social-links/hover/Vk.svg',
            '<?=$this->mainAssets?>/assets/img/social-links/hover/Facebook.svg',
            '<?=$this->mainAssets?>/assets/img/social-links/hover/Instagram.svg',
            '<?=$this->mainAssets?>/assets/img/social-links/hover/Spark.svg',
            '<?=$this->mainAssets?>/assets/img/social-links/hover/Twitter.svg',
            '<?=$this->mainAssets?>/assets/img/social-links/hover/YouTube.svg',
        ]).preload();
    </script>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END); ?>
</head>
<?php echo $content; ?>
</html>
