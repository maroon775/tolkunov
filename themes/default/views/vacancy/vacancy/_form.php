<?php
/**
 * @var $this VacancyController
 * @var $model VacancyResume
 * @var $fileModels VacancyResumeFile[]|null
 * @var $form TbActiveForm
 */

Yii::app()->clientScript->scriptMap['jquery.js'] = $this->mainAssets . '/assets/vendor/jquery/dist/jquery.min.js';
?>

<div class="b-form" id="vacancy_form_block">
    <div class="container">
        <?php $form = $this->beginWidget(
            'bootstrap.widgets.TbActiveForm',
            [
                'id' => 'resume-form',
                'action' => ['/vacancy/vacancy/create'],
                'htmlOptions' => [
                    'class' => 'b-form__form',
                    'enctype' => 'multipart/form-data',
                ],
            ]
        ); ?>

        <?php if (Yii::app()->user->hasFlash(\yupe\widgets\YFlashMessages::SUCCESS_MESSAGE)): ?>
            <div class="b-order-done text-left">
                <p class="b-order-done__title">Готово, ваша заявка отправлена!</p>

                <p class="b-order-done__text">Мы внимательно изучим её, а затем свяжемся с вами.</p>
            </div>
        <?php else: ?>

            <?php $this->widget('\yupe\widgets\YFlashMessages', ['options' => ['closeText' => false]]); ?>

            <?php echo $form->errorSummary([$model]); ?>

            <?php echo $form->hiddenField($model, 'vacancy_id'); ?>
            <?php echo $form->textFieldGroup($model, 'name', ['widgetOptions' => ['htmlOptions' => ['placeholder' => false]]]); ?>
            <?php echo $form->textFieldGroup($model, 'phone', ['widgetOptions' => ['htmlOptions' => ['placeholder' => false]]]); ?>
            <?php echo $form->emailFieldGroup($model, 'email', ['widgetOptions' => ['htmlOptions' => ['placeholder' => false]]]); ?>
            <?php echo $form->textAreaGroup($model, 'text', ['widgetOptions' => ['htmlOptions' => ['placeholder' => false, 'rows' => 7]]]); ?>

            <div class="form-group">
                <label>Файлы</label>

                <p class="help-block">Резюме, портфолио, сертификаты</p>

                <div class="b-form-files"></div>

                <div class="b-form-upload b-form__upload">
                    <?php echo CHtml::fileField('VacancyResume[files][]', '', ['multiple' => true, 'id' => 'VacancyResume_files_0', 'data-id' => "VacancyResume_files"]); ?>
                    <span class="b-form-upload__label">Выберите файлы или перетащите их в эту форму</span>
                </div>
            </div>

            <div class="form-group">
                <div class="b-checkbox">
                    <?= $form->checkBox($model, 'agree'); ?>
                    <?= $form->labelEx($model, 'agree'); ?>
                    <?= $form->error($model, 'agree'); ?>
                </div>
            </div>

            <?php echo CHtml::submitButton('Отправить', ['id' => 'submit_button_' . $form->id, 'class' => 'btn btn-success btn-lg b-form__button', 'data-loading-text' => 'Отправить']); ?>
            <!--        --><?php //echo CHtml::ajaxSubmitButton(
//            'Отправить',
//            ['/vacancy/vacancy/create'],
//            [
//                'beforeSend' => 'js:function(){ $("#submit_button_' . $form->id . '").button("loading"); }',
//                'replace' => '#vacancy_form_block',
//            ],
//            [
//                'id' => 'submit_button_' . $form->id,
//                'class' => 'btn btn-success btn-lg b-form__button',
//                'data-loading-text' => 'Отправить',//                            'onclick' => "yaCounter38387270.reachGoal('prosmotr_doma'); return true;",
//            ]
//        ); ?>
<!--            <br><a href="#" class="b-form__link">Соглашение о конфиденциальности</a>-->
        <?php endif; ?>
        <?php $this->endWidget(); ?>

        <script>
            <?php ob_start();?>
            jQuery('body').on('click', '#submit_button_resume-form', function () {
                var fd = new FormData();

                $('.b-form-upload').each(function () {
                    var j = 0;

                    $(this).find('input[type=file]').each(function () {
                        var files = this.files;

                        if (files.length) {
                            for (var i = 0; i < files.length; i++) {
                                if (!$(this).closest('.b-form-upload').prev().find('.b-form-files__item').eq(j).hasClass('hidden')) {
                                    fd.append("VacancyResume[files][]", files[i]);
                                }

                                j++;
                            }
                        }
                    });
                });

                fd.append(yupeTokenName, yupeToken);
                fd.append('VacancyResume[vacancy_id]', $('#VacancyResume_vacancy_id').val());
                fd.append('VacancyResume[name]', $('#VacancyResume_name').val());
                fd.append('VacancyResume[phone]', $('#VacancyResume_phone').val());
                fd.append('VacancyResume[email]', $('#VacancyResume_email').val());
                fd.append('VacancyResume[text]', $('#VacancyResume_text').val());
                fd.append('VacancyResume[agree]', $('#VacancyResume_agree').is(':checked') ? 1 : 0);

//            console.log(fd, jQuery(this).parents("form").serialize());

                jQuery.ajax({
                    'beforeSend': function () {
                        $("#submit_button_resume-form").button("loading");
                    },
                    'url': '<?=Yii::app()->createUrl('/vacancy/vacancy/create')?>',
                    type: 'POST',
                    cache: false,
                    data: fd,
                    datatype: 'json',
                    processData: false,
                    contentType: false,
                    'success': function (html) {
                        jQuery("#vacancy_form_block").replaceWith(html)
                    },
                    error: function () {
                        $("#submit_button_resume-form").button("reset");
                        alert("Произошла непредвиденная ошибка");
                    }
                });

                return false;
            });
            <?php $vacancyFormScript = ob_get_clean(); ?>
            <?php Yii::app()->clientScript->registerScript('vacancy-form-script', $vacancyFormScript, CClientScript::POS_READY); ?>
        </script>
    </div>
</div>
