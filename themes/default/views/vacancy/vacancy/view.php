<?php
/**
 * @var $this VacancyController
 * @var $model Vacancy
 */

$this->title = $model->seo_title ?: [$model->title, 'Работа у нас', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = $model->seo_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = $model->seo_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="container">
    <div class="text-center">
        <?= CHtml::link('Посмотреть все вакансии', ['/vacancy/vacancy/index'], ['class' => 'b-page__back']); ?>
    </div>
    <h1 class="b-page__title"><?= $model->title ?></h1>

    <div class="b-text g-mb60">
        <?= $model->text_1 ?>
    </div>
</div>
<div class="container">
    <p class="b-page__title">Отправка резюме</p>
</div>

<?php
$form = new VacancyResume();
$form->vacancy_id = $model->id;
?>

<?php $this->renderPartial('_form', ['model' => $form]); ?>
