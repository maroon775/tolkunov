<?php
/**
 * @var $data Vacancy
 * @var $index integer
 */
?>
<a class="b-vacancies__item" href="<?= $data->url ?>">
    <p class="b-vacancies__title"><?= $data->short_title ?: $data->title ?></p>
    <?= CHtml::image($data->getImageUrl(900, 360), CHtml::encode($data->title), ['class' => 'img-responsive b-vacancies__image', 'data-rjs' => '2']); ?>
</a>
