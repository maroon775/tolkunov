<?php
/**
 * Отображение для vacancy/index
 *
 * @var $this VacancyController
 * @var $dataProvider CActiveDataProvider
 **/

$this->title = ['Работа у нас', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = Yii::app()->getModule('yupe')->siteKeyWords;

$this->breadcrumbs = [Yii::t('VacancyModule.vacancy', 'Работа у нас')];
?>

<div class="container">
    <h1 class="b-page__title">Работа у нас</h1>

    <div class="b-vacancies-features">
        <div class="b-vacancies-features__item">
            <img src="<?= $this->mainAssets ?>/assets/img/vacancy/1.png" class="img-responsive b-vacancies-features__image">

            <p class="b-vacancies-features__title">Дружная<br>команда</p>

            <p class="b-vacancies-features__text">Общаемся и работаем с улыбкой. Поддерживаем друг друга в сложных ситуациях.</p>
        </div>
        <div class="b-vacancies-features__item">
            <img src="<?= $this->mainAssets ?>/assets/img/vacancy/2.png" class="img-responsive b-vacancies-features__image">

            <p class="b-vacancies-features__title">Достойное вознаграждение</p>

            <p class="b-vacancies-features__text">Зарабатываем столько, сколько приносим пользы. Нет потолка зарплаты.</p>
        </div>
        <div class="b-vacancies-features__item">
            <img src="<?= $this->mainAssets ?>/assets/img/vacancy/3.png" class="img-responsive b-vacancies-features__image">

            <p class="b-vacancies-features__title">Интересные задачи</p>

            <p class="b-vacancies-features__text">Делаем то, что нравится. То, чем можно гордиться.</p>
        </div>
        <div class="b-vacancies-features__item">
            <img src="<?= $this->mainAssets ?>/assets/img/vacancy/4.png" class="img-responsive b-vacancies-features__image">

            <p class="b-vacancies-features__title">Обучение и развитие</p>

            <p class="b-vacancies-features__text">Учимся новому, делимся опытом между собой.</p>
        </div>
    </div>
    <?php if (Yii::app()->hasModule('gallery')): ?>
        <?php Yii::import('application.modules.gallery.models.*'); ?>
        <?php $models = Gallery::model()->published()->findAll('t.id = 1'); ?>

        <?php foreach ($models as $model): ?>
            <div class="b-slider">
                <?php foreach ($model->images as $image): ?>
                    <div class="b-slider__item">
                        <?= CHtml::image($image->getImageUrl(1000), $image->alt, ['class' => 'img-responsive', 'data-rjs'=>'2']); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

    <div class="b-text g-mb60">
        <?php $this->widget(
            "application.modules.contentblock.widgets.ContentBlockWidget",
            ["code" => "tekst-na-stranice-vakansiy"]);
        ?>
    </div>

    <h2 class="b-page__title">Актуальные вакансии</h2>
</div>
<div class="b-vacancies">
    <?php $this->widget(
        'zii.widgets.CListView',
        [
            'dataProvider' => $dataProvider,
            'itemView' => '_item',
            'template' => '{items}',
            'cssFile' => false,
            'itemsCssClass' => 'b-vacancies__list',
            'htmlOptions' => ['class' => 'container'],
        ]
    ); ?>
</div>
<div class="hidden">
    <div id="vacancy_form" class="b-modal-form">
        <p class="b-modal-form__header">Отправка резюме</p>

        <?php $this->renderPartial('_form', ['model' => new VacancyResume()]); ?>
    </div>
</div>

<script>
    $('.b-vacancies__button').on('click', function () {
        $('#VacancyResume_vacancy_id').val($(this).data('id'));
    });
</script>
