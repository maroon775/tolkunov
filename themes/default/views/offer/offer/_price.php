<?php
/**
 * @var $data Tariff
 */
?>

<div class="b-prices__item <?= $data->is_wide ? 'b-prices__item_wide' : '' ?> <?= $data->is_recommended ? 'b-prices__item_recommended' : '' ?> b-prices__item_color_<?= $data->getColorString() ?>">
    <p class="b-prices__title"><?= $data->title ?></p>

    <div class="b-prices__text">
        <?php foreach ($data->properties as $property): ?>
            <div class="b-prices__property">
                <?php if ($property->description): ?>
                    <p class="b-prices__property-text" data-toggle="popover" data-content="<?= $property->description ?>"><?= $property->getTitle() ?></p>
                <?php else: ?>
                    <p class="b-prices__property-text"><?= $property->getTitle() ?></p>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>

        <?php $this->renderPartial('_price_bonus', ['tariff' => $data]); ?>
    </div>
    <div class="b-prices__price"><?= $data->price ?><span class="b-prices__rub">₽</span></div>
    <div class="text-center"><a href="#prices_example_3" class="b-prices__link">Посмотреть примеры</a></div>
    <?= CHtml::link('Выбрать', ['/order/order/tariff', 'tariff' => $data->title], ['class' => 'btn b-prices__button']); ?>
</div>
