<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 11.01.2017
 * Time: 18:30
 *
 * @var $this OfferController
 * @var $current Tariff|null
 */
?>
<div class="container">
    <div class="b-offers__filter b-offers-filter">
        <div class="b-offers-filter__block b-offers-filter__block_color_yellow">
            <?= CHtml::link('Все', ['/offer/offer/index'],
                ['class' => 'b-offers-filter__item' . (!isset($current) ? ' b-offers-filter__item_active' : '')]); ?>
        </div>
        <div class="b-offers-filter__block b-offers-filter__block_color_red">
            <?php foreach (Tariff::model()->type(Tariff::TYPE_HTML5)->findAll() as $tariff): ?>
                <?= CHtml::link($tariff->title, $tariff->url, ['class' => 'b-offers-filter__item' . (isset($current) && $current->id == $tariff->id ? ' b-offers-filter__item_active' : '')]); ?>
            <?php endforeach; ?>

            <div class="b-offers-filter__title">html5 и flash-баннеры</div>
        </div>
        <div class="b-offers-filter__block b-offers-filter__block_color_blue">
            <?php foreach (Tariff::model()->type(Tariff::TYPE_GIF)->findAll() as $tariff): ?>
                <?= CHtml::link($tariff->title, $tariff->url, ['class' => 'b-offers-filter__item' . (isset($current) && $current->id == $tariff->id ? ' b-offers-filter__item_active' : '')]); ?>
            <?php endforeach; ?>

            <div class="b-offers-filter__title">gif-баннеры</div>
        </div>
        <div class="b-offers-filter__block b-offers-filter__block_color_green">
            <?php foreach (Tariff::model()->type(Tariff::TYPE_JPG)->findAll() as $tariff): ?>
                <?= CHtml::link($tariff->title, $tariff->url, ['class' => 'b-offers-filter__item' . (isset($current) && $current->id == $tariff->id ? ' b-offers-filter__item_active' : '')]); ?>
            <?php endforeach; ?>

            <div class="b-offers-filter__title">jpg-баннеры</div>
        </div>
    </div>
</div>
