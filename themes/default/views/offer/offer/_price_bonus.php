<?php
/**
 * @var $tariff Tariff
 */

$bonus = OfferItem::model()->getMaxBonusForTarif($tariff->id);
?>
<?php if ($bonus): ?>
    <div class="b-prices__property">
        <?= CHtml::link('<b>Бонусов</b> до ' . $bonus . '<span class="b-prices__rub">₽</span>', $tariff->url, ['class' => 'b-prices__bonus']); ?>
    </div>
<?php endif; ?>
