<?php
/**
 * @var $params array
 * @var $layoutParams array
 */

$params['items'] = array_map(
    function ($item) {
        $item['linkOptions'] = array_merge(!empty($item['linkOptions']) ? $item['linkOptions'] : [], ['class' => 'b-footer-menu__link']);
        $item['template'] = null;

        return $item;
    },
    $params['items']
);

$this->widget('application.components.MyMenu', [
    'items' => $params['items'],
    'activateItems' => false,
    'itemCssClass' => 'col-xs-6 col-sm-4 col-md-5',
    'itemTemplate' => '<div class="b-footer-menu__item">{menu}</div>',
    'htmlOptions' => ['class' => 'row'],
]);
