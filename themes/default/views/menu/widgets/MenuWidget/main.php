<?php
/**
 * @var $params array
 * @var $layoutParams array
 */

$params['items'] = array_map(
    function ($item) {
        $item['linkOptions'] = array_merge(!empty($item['linkOptions']) ? $item['linkOptions'] : [], ['class' => 'b-header-menu__link']);

        return $item;
    },
    $params['items']
);

$this->widget('zii.widgets.CMenu', [
    'items' => $params['items'],
//    'itemCssClass' => 'b-header-menu__item',
    'activeCssClass' => 'b-header-menu__item_active',
    'htmlOptions' => ['class' => 'nav navbar-nav'],
]);
